package com.likefirst.protectskin.objects;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class Notification
{

	private long id;
	private String title;
	private String msg;
	private String date;

	public Notification(String title, String msg, String date)
	{
		this.title = title;
		this.msg = msg;
		this.date = date;
	}

	public long getId()
	{
		return id;
	}

	public String getTitle()
	{
		return title;
	}

	public String getMsg()
	{
		return msg;
	}

	public String getDate()
	{
		return date;
	}
}
