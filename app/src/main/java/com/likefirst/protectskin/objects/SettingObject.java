package com.likefirst.protectskin.objects;

/**
 * Created by jinwoo on 2016-07-19.
 */
public class SettingObject
{
	/**
	 * Available types of setting
	 */
	public static final int TYPE_VAL_BOOL = 1; // setting has boolean value
	public static final int TYPE_VAL_INT = 2; // setting has int value
	public static final int TYPE_VAL_STRING = 3; // setting has string value
	public static final int TYPE_NONE = 0; // setting has no value or separate source of value

	public static int INVALID_INT = -1;
	public static boolean INVALID_BOOL = false;
	public static String INVALID_STRING = null;

	private String settingKey;
	private String settingName;
	private int settingType;

	private String valStr;
	private int valInt = 0;
	private boolean valBool = false;

	public SettingObject(String name)
	{
		init(name, TYPE_NONE);
	}
	public SettingObject(String name, int val){init(name, TYPE_VAL_INT); valInt = val;}
	public SettingObject(String name, boolean val){init(name, TYPE_VAL_BOOL); valBool = val;}
	public SettingObject(String name, String val){init(name, TYPE_VAL_STRING); valStr = val;}



	public void init(String name, int type)
	{
		settingName = name;
		if(type == TYPE_VAL_BOOL ||
				type == TYPE_VAL_INT ||
				type == TYPE_VAL_STRING ||
				type == TYPE_NONE)
		{
			settingType = type;
		}
		else
		{
			throw new IllegalArgumentException("Illegal setting type");
		}
	}


	public String getSettingName(){return settingName;}
	public int getSettingType(){return settingType;}

	public int getIntValue(){if(settingType == TYPE_VAL_INT) {return valInt;}else{try{throw new Exception("Type not matched to this method");}catch (Exception e){return INVALID_INT;}}}
	public boolean getBoolValue(){if(settingType == TYPE_VAL_BOOL){return valBool;}else{try{throw new Exception("Type not matched to this method");}catch (Exception e){return INVALID_BOOL;}}}
	public String getStringValue(){if(settingType == TYPE_VAL_STRING){return valStr;}else{try{throw new Exception("Type not matched to this method");}catch (Exception e){return INVALID_STRING;}}}

	public void setType(int type)
	{
		if (type == TYPE_VAL_BOOL ||
				type == TYPE_VAL_INT ||
				type == TYPE_NONE)
		{
			settingType = type;
		} else
		{
			throw new IllegalArgumentException("Illegal setting type");
		}
	}
	public void setKey(String key){settingKey = key;}
	public void setIntValue(int value){if(settingType == TYPE_VAL_INT){valInt = value;}else{try{throw new Exception("Type not matched to this method");}catch (Exception e){}}}
	public void setBoolValue(boolean value){if(settingType == TYPE_VAL_BOOL) {valBool = value;}else{try{throw new Exception("Type not matched to this method");}catch (Exception e){}}}
	public void setStrValue(String value){if(settingType == TYPE_VAL_STRING) {valStr = value;}else{try{throw new Exception("Type not matched to this method");}catch (Exception e){}}}
}
