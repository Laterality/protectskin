package com.likefirst.protectskin.objects;

import com.likefirst.protectskin.utilities.PW486;

/**
 * Created by jinwoo on 2016-07-11.
 */
public class SkinType
{
	public static final int SKIN_TYPE_DRY = 1;
	public static final int SKIN_TYPE_GENERAL = 2;
	public static final int SKIN_TYPE_OILY = 3;
	public static final int SKIN_TYPE_COMPLEX = 4;
	public static final int SKIN_TYPE_SENSITIVE = 5;

	private int skinType = 0;
	private String typeName;

	public SkinType(int type)
	{
		skinType = type;
		switch (type)
		{
			case SKIN_TYPE_DRY:
				typeName = "건성 피부";
				break;
			case SKIN_TYPE_GENERAL:
				typeName = "일반 피부";
				break;
			case SKIN_TYPE_OILY:
				typeName = "지성 피부";
				break;
			case SKIN_TYPE_COMPLEX:
				typeName = "복합성 피부";
				break;
			case SKIN_TYPE_SENSITIVE:
				typeName = "민감성 피부";
				break;
		}
	}


	public int getSkinType(){return skinType;}
	public String getSkinTypeString(){return typeName;}

	public static String codeToId(int type)
	{
		switch (type)
		{
			case SKIN_TYPE_DRY:
				return PW486.SKINTYPE_DRY;
			case SKIN_TYPE_GENERAL:
				return PW486.SKINTYPE_GENERAL;
			case SKIN_TYPE_OILY:
				return PW486.SKINTYPE_OILY;
			case SKIN_TYPE_COMPLEX:
				return PW486.SKINTYPE_COMPLEX;
			case SKIN_TYPE_SENSITIVE:
				return PW486.SKINTYPE_SENSITIVE;
			default:
				return null;
		}
	}

	public static String codeToName(int type)
	{
		switch (type)
		{
			case SKIN_TYPE_DRY:
				return "건성 피부";
			case SKIN_TYPE_GENERAL:
				return "일반 피부";
			case SKIN_TYPE_OILY:
				return "지성 피부";
			case SKIN_TYPE_COMPLEX:
				return "복합성 피부";
			case SKIN_TYPE_SENSITIVE:
				return "민감성 피부";
			default:
				return null;
		}
	}

}
