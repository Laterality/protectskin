package com.likefirst.protectskin.objects;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class ScheduleDetailListItem
{

	private String msg;
	private boolean fulfilled;

	public String getMsg(){return msg;}
	public boolean isFulfilled(){return fulfilled;}
}
