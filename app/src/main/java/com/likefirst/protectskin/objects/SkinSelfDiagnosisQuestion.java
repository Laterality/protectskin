package com.likefirst.protectskin.objects;

import android.support.annotation.Nullable;

/**
 * Created by jinwoo on 2016-07-10.
 */
public class SkinSelfDiagnosisQuestion
{
	private String question;
	private SkinSelfDiagnosisQuestion yNext;
	private SkinSelfDiagnosisQuestion nNext;
	private SkinSelfDiagnosisQuestion uNext;
	private SkinSelfDiagnosisQuestion prev;
	private boolean isLast = false;
	private boolean isFirst = false;
	private SkinType yResult;
	private SkinType nResult;
	private SkinType uResult;

	public SkinSelfDiagnosisQuestion(String question)
	{
		this.question = question;
	}

	public void setNode(SkinSelfDiagnosisQuestion yNext, SkinSelfDiagnosisQuestion nNext, SkinSelfDiagnosisQuestion uNext)
	{
		this.yNext = yNext;
		this.nNext = nNext;
		this.uNext = uNext;
	}

	public void setPrev(SkinSelfDiagnosisQuestion prev) {this.prev = prev;}
	public void setIsLast(boolean last){isLast = last;}
	public void setIsFirst(boolean first){isFirst = first;}
	public void setResult(SkinType y, SkinType n, SkinType u){yResult = y; nResult = n; uResult = u;}

	public String getQuestion() {return question;}
	public @Nullable SkinSelfDiagnosisQuestion getYNext() {return yNext;}
	public @Nullable SkinSelfDiagnosisQuestion getNNext() {return nNext;}
	public @Nullable SkinSelfDiagnosisQuestion getUNext() {return uNext;}
	public @Nullable SkinSelfDiagnosisQuestion getPrev() {return prev;}
	public SkinType getYResult(){return yResult;}
	public SkinType getnResult(){return nResult;}
	public SkinType getUResult(){return uResult;}
	public boolean isFirst(){return isFirst;}
	public boolean isLast(){return isLast;}
	public boolean isYPosiible(){return yNext != null || yResult != null;}
	public boolean isNPosiible(){return nNext != null || nResult != null;}
	public boolean isUPosiible(){return uNext != null || uResult != null;}
}
