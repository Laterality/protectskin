package com.likefirst.protectskin;

import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.PostDto2;
import com.likefirst.protectskin.api_dto.ReplyDto;
import com.likefirst.protectskin.api_dto.UploadDto;
import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.UserLoginDto;
import com.likefirst.protectskin.api_dto.UserPostDto;
import com.likefirst.protectskin.api_dto.results.ImageUploadResult;
import com.likefirst.protectskin.api_dto.results.MonthlyScheduleSetResult;
import com.likefirst.protectskin.api_dto.results.PostResult;
import com.likefirst.protectskin.api_dto.results.PostResult2;
import com.likefirst.protectskin.api_dto.results.RecommendResult;
import com.likefirst.protectskin.api_dto.results.ReplyResult;
import com.likefirst.protectskin.api_dto.results.UserResult;
import com.likefirst.protectskin.api_dto.results.PostsResult;
import com.likefirst.protectskin.api_dto.results.UserResult2;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jinwoo on 2016-07-23.
 *
 * 내부 REST API 관련 상수, 인터페이스 정의
 */
public class API_Internal
{
	public static final String BASE_URL = "http://www.latera.kr/pboo/";

	public static final String PATH_ID = "id";

	public static final String GET_USER = "api/user/{" + PATH_ID + "}";
	public static final String PUT_USER = "api/user/{" + PATH_ID + "}";
	public static final String POST_REGISTER = "api/user/register";
	public static final String POST_LOGIN = "api/user/login";
	public static final String GET_POSTS = "api/post";
	public static final String GET_POST = "api/post/{" + PATH_ID + "}";
	public static final String GET_POST_RECOMMEND = "api/post/recommend/{" + PATH_ID + "}";
	public static final String GET_POST_BOOKMARK = "api/post/bookmark/{" + PATH_ID + "}";
	public static final String GET_POST_REPLY = "api/post/reply/{" + PATH_ID + "}";
	public static final String GET_POST_SEARCH = "api/post/search";
	public static final String POST_POST_REPLY = "api/post/reply/{" + PATH_ID + "}";
	public static final String POST_UPLOAD_IMAGE_BOARD = "api/upload/board";
	public static final String POST_UPLOAD_IMAGE_PROFILE = "api/upload/profile";
	public static final String POST_POST = "api/post";
	public static final String GET_MONTHLY_SCHEDULE_SET  = "api/schedule";


	public static class Services
	{


		public interface UserService
		{

			@GET(GET_USER)
			Call<UserResult> getUser(@Path(PATH_ID)String id);

			@POST(POST_REGISTER)
			Call<UserResult> registerUser(@Body UserPostDto user);

			@POST(POST_LOGIN)
			Call<UserResult> loginUser(@Body UserLoginDto user);

			@PUT(PUT_USER)
			Call<UserResult2> updateUser(@Path(PATH_ID)String id, @Body UserDto user);
		}


		public interface PostService
		{
			/**
			 * 게시물 생성 api
			 *
			 * Path : /api/post
			 * Medthod : POST
			 *
			 * Request
			 * @body.author : String, 작성자 id
			 * @body.board_id : String, 게시판 id
			 * @body.skinType_id : String, 피부타입 id, 전체인경우 null
			 * @body.title : 게시물 제목
			 * @body.content : 게시물 내용
			 *
			 * Response
			 * @body.result : 결과 문자열
			 * @body.post : 작성된 게시물
			 *
			 */
			@POST(POST_POST)
			Call<PostResult2> createPost(@Body PostDto2 post);

			/**
			 * 게시물 목록을 반환
			 *
			 * Path : /api/post
			 * Method : GET
			 *
			 * Request
			 * @query.user : 사용자 id
			 * @query.before : 기준 시간(unix epoch, before값 이전의 게시물 반환), 기본값 : now
			 * @query.limit : 반환할 리스트의 최대 사이즈, 기본값 3
			 * @query.sort : 리스트 정렬 기준["date", "recommend"], 기본값 : date
			 * @Query.content : 게시물 내용 포함 여부["true", "false"]. 기본값 : false
			 *
			 * Response
			 * @result.result : 수행 결과 [success, error]
			 * @result.posts : 게시물 리스트
			 */
			@GET(GET_POSTS)
			Call<PostsResult> getPosts(@Query("user") String user,
			                           @Query("board")String board, @Query("skin")String skin,
			                           @Query("before") long before, @Query("limit") int limit,
			                           @Query("sort") String sort, @Query("content")String content);


			/**
			 * 게시물 조회 API
			 *
			 * Path : /api/post/{id}
			 * Method : GET
			 *
			 * Request
			 * @path.id : 조회할 게시물 id
			 * @query.user : 사용자 id, 해당 사용자에 대한 footstep 남김
			 *
			 * Response
			 * @body.result : 결과 문자열
			 * @body.post : 조회환 게시물
			 * @body.footstep : 게시물에 대한 사용자의 읽기/추천/북마크 여부
			 *
			 */
			@GET(GET_POST)
			Call<PostResult> getPost(@Path(PATH_ID)String id, @Query("user")String user_id);

			/**
			 * 게시물 추천 API
			 *
			 * Path : /api/post/recommend/{id}
			 * Method : GET
			 *
			 * Request
			 * @path.id : 게시물 id
			 * @query.by : 유저 id
			 *
			 * Response
			 * @body.result : 결과 문자열
			 * @body.footstep : 게시물에 대한 수정된 footstep
			 */
			@GET(GET_POST_RECOMMEND)
			Call<RecommendResult> recommendPost(@Path(PATH_ID)String id, @Query("by")String user_id);


			/**
			 * 게시물 북마크 API
			 *
			 * Path : /api/post/bookmark/{id}
			 * Method : GET
			 *
			 * Request
			 * @path.id : 게시물 id
			 * @query.by : 유저 id
			 *
			 * Response
			 * @body.result : 결과 문자열
			 * @body.footstep : 게시물에 대한 수정된 footstep
			 */
			@GET(GET_POST_BOOKMARK)
			Call<RecommendResult> bookmarkPost(@Path(PATH_ID)String id, @Query("by")String user_id);


			/**
			 * 게시물 덧글 추가 API
			 *
			 * Path : /api/post/reply/{id}
			 * Method : POST
			 *
			 * Request
			 * @path.id : 게시물 id
			 * @body.user : 덧글 작성자 id
			 * @body.content : 덧글 내용
			 * @body.rereply : 답글 여부 [true, false]
			 * @body.to : 답글인 경우, 해당 덧글 id
			 *
			 * Response
			 * @code.500 : 서버 에러
			 * @code.404 : 해당 댓글을 찾을 수 없는 경우(삭제된 경우)
			 * @code.200 : 정상 수행
			 * @body.result : 결과 문자열
			 * @body.replies : 해당 게시물에 대한 덧글 리스트
			 */
			@POST(POST_POST_REPLY)
			Call<ReplyResult> addReply(@Path(PATH_ID)String post_id, @Body ReplyDto reply);


			/**
			 * 덧글 조회 API
			 *
			 * 게시물에 대한 덧글 목록을 반환
			 *
			 * Path : /api/post/reply/{id}
			 * Method : GET
			 *
			 * Request
			 * @path.id : 게시물 id
			 *
			 * Response
			 * @body.result : 결과 문자열
			 * @body.replies : 덧글 목록(ASCENDING BY regDate)
			 */
			@GET(GET_POST_REPLY)
			Call<ReplyResult> getReplies(@Path(PATH_ID)String post_id);


			/**
			 * 게시물 검색 API
			 *
			 * Path : /api/post/search
			 * Method : GET
			 *
			 * Request
			 * @query.q : 검색 키워드
			 * @query.board : 검색할 게시판, 기본값 : 전체
			 * @query.skin : 검색할 피부 타입, 기본값 : 전체
			 * @query.sort : 정렬 방식, ["date", "recommend"], 기본값 "date"
			 *
			 * Response
			 * @body.result : 결과 문자열
			 * @body.posts : 검색된 게시물 리스트
			 *
			 */
			@GET(GET_POST_SEARCH)
			Call<PostsResult> searchPost(@Query("q")String keyword, @Query("board")String board, @Query("skin")String skin, @Query("sort")String sort);
		}

		public interface ScheduleService
		{
			@GET(GET_MONTHLY_SCHEDULE_SET)
			Call<MonthlyScheduleSetResult> getMonthlySchedule(@Query("user")String user_id);
		}

		public interface ImageUploadService
		{
			@Multipart
			@POST(POST_UPLOAD_IMAGE_BOARD)
			Call<ImageUploadResult> uploadBoardImage(@Part MultipartBody.Part img, @Part("description") RequestBody description);
		}
	}
}
