package com.likefirst.protectskin.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetMinuteDustApiDto;
import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetUVApiDto;
import com.likefirst.protectskin.utilities.InteractionFragment;

/**
 * Created by jinwoo on 2016-07-10.
 */
public class MainActivityPresenterImpl implements MainActivityModel.MainActivityPresenter
{
	private AppCompatActivity activity;
	private View view;
	private MainActivityModel model;
	private int fl;

	public MainActivityPresenterImpl(AppCompatActivity activity, int flContent)
	{
		this.activity = activity;
		model = new MainActivityModel(activity);
		fl = flContent;
	}

	@Override
	public void setView(View view)
	{
		this.view = view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		model.init(new MainActivityModel.AsyncCallback()
		{
			@Override
			public void onPreExecute()
			{

			}

			@Override
			public void onGetDustInfo(WeatherPlanetMinuteDustApiDto dust)
			{
				if (dust != null)
				{
					view.setDustIndicator(dust.getWeather().getDusts().get(0).getPm10().getValue());
					view.setDustGrade(dust.getWeather().getDusts().get(0).getPm10().getGrade());
				} else
				{
					view.setDustIndicator("알수없음");
					view.setDustGrade(" ");
				}
			}

			@Override
			public void onGetUVInfo(WeatherPlanetUVApiDto uv)
			{
				if (uv != null)
				{
					int day = uv.getWeather().getwIndex().getUvindex().get(0).getDay(0).getIndex().isEmpty() ? 1 : 0;
					view.setUVIndicator(uv.getWeather().getwIndex().getUvindex().get(0).getDay(day).getIndex());
					view.setRecommendation(uv.getWeather().getwIndex().getUvindex().get(0).getDay(day).getComment());
				} else
				{
					view.setUVIndicator("알수없음");
					view.setUVGrade(" ");
				}
			}

		}, fl);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		model.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onResume(AppCompatActivity activity)
	{
		model.setActvity(activity);
	}

	@Override
	public void onBackPressed()
	{
		model.onBackPressed();
	}

	@Override
	public void showGPSRequireDialog(Activity activity)
	{
		model.showSettingDialog(activity);
	}

	@Override
	public void setAsyncCallback(MainActivityModel.AsyncCallback callback)
	{
		model.setAsyncCallback(callback);
	}

	@Override
	public FragmentPagerAdapter getMenuPagerAdapter(AppCompatActivity activity)
	{
		return model.getMenuPagerAdapter(activity);
	}

	@Override
	public void onTabSelected(TabLayout.Tab tab)
	{
		model.showFragment(((InteractionFragment)model.getSubmenu(tab.getPosition())).getChild(0));
	}

	@Override
	public void checkPermission()
	{
		model.permissionProcess();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
	{
		model.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}

	@Override
	public void refreshHome()
	{
		model.refreshHome();
	}
}
