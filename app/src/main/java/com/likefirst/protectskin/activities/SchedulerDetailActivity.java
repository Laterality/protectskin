package com.likefirst.protectskin.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.utilities.Utilities;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class SchedulerDetailActivity extends AppCompatActivity implements ImageView.OnClickListener
{
	public static final String INTENT_YEAR = "year";
	public static final String INTENT_MONTH = "month";
	public static final String INTENT_DAY_OF_MONTH = "dayofweek";

	private DateTime currentDate;

	private TextView tvDateIndicator;
	private ImageView ivPrev;
	private ImageView ivNext;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scheduler_detail);

		Intent i = getIntent();
		currentDate = new DateTime(i.getIntExtra(INTENT_YEAR, 2016), i.getIntExtra(INTENT_MONTH, 12), i.getIntExtra(INTENT_DAY_OF_MONTH, 25), 0, 0);

		tvDateIndicator = (TextView)findViewById(R.id.tv_calendar_detail_indicator);
		ivPrev = (ImageView) findViewById(R.id.iv_calendar_detail_prev_day);
		ivNext = (ImageView) findViewById(R.id.iv_calendar_detail_next_day);

		Typeface tf = (Typeface.createFromAsset(getAssets(),"arittaDodumBold.ttf"));

		List<TextView> tvl = new ArrayList();
		tvl.add((TextView) findViewById(R.id.tv_calendar_this_months_practice_rate));
		tvl.add((TextView) findViewById(R.id.tv_scheduler_detail_fulfill_rate_indicator));
		tvl.add((TextView) findViewById(R.id.tv_scheduler_detail_fulfill_rate_day));
		for(int j = 0 ; j < tvl.size();j++)
		{
			tvl.get(j).setTypeface(tf);
		}

		List<Button> btnl = new ArrayList();
		btnl.add((Button) findViewById(R.id.btn_calendar_morning));
		btnl.add((Button) findViewById(R.id.btn_calendar_afternoon));
		btnl.add((Button) findViewById(R.id.btn_calendar_evening));
		for(int j = 0 ; j < tvl.size();j++)
		{
			btnl.get(j).setTypeface(tf);
		}

		tvDateIndicator.setTypeface(Typeface.createFromAsset(getAssets(), "NanumSquareB.ttf"));

		ivPrev.setOnClickListener(this);
		ivNext.setOnClickListener(this);

		refresh();
	}

	private void refresh()
	{
		tvDateIndicator.setText(String.format(Locale.KOREAN, "%d월 %d일 (%s)", currentDate.getMonthOfYear(), currentDate.getDayOfMonth(), Utilities.getDayOfWeekString(currentDate)));
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.iv_calendar_detail_prev_day:
				currentDate = currentDate.minusDays(1);
				break;
			case R.id.iv_calendar_detail_next_day:
				currentDate = currentDate.plusDays(1);
				break;
			default:
				break;
		}

		refresh();
	}
}
