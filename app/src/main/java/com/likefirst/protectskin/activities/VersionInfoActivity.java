package com.likefirst.protectskin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.PW486;

/**
 * Created by jinwoo on 2016-07-23.
 */
public class VersionInfoActivity extends AppCompatActivity
{
	private TextView tvVersion;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_version_info);

		tvVersion = (TextView) findViewById(R.id.tv_version_version);

		tvVersion.setText("현재 버전 : " + ((CustomApplication)getApplication()).getSetting().getString(PW486.PREFS_KEY_SETTING_VERSION));
	}
}
