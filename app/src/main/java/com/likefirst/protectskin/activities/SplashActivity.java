package com.likefirst.protectskin.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.results.UserResult;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.PW486;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-01.
 */
public class SplashActivity extends AppCompatActivity
{
	private static final String TAG = "SplashAct";
    private static final long WAIT = 2000;

	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
	    overridePendingTransition(R.anim.sliding_ltr, R.anim.sliding_rtl);
        setContentView(R.layout.activity_splash);

	    prefs = getSharedPreferences(PW486.PREFS_NAME, Context.MODE_PRIVATE);
	    editor = prefs.edit();

	    List<TextView> tvl = new ArrayList();
	    TextView tv;
	    Typeface tf = (Typeface.createFromAsset(getAssets(),"NanumBarunGothic-YetHangul.ttf"));
	    tvl.add((TextView) findViewById(R.id.tv_splash_text1));
	    tvl.add((TextView) findViewById(R.id.tv_splash_text2));
	    tvl.add((TextView) findViewById(R.id.tv_splash_text3));
	    tvl.add((TextView) findViewById(R.id.tv_splash_text4));
	    for(int i = 0 ; i < tvl.size();i++)
	    {
		    tvl.get(i).setTypeface(tf);
	    }

		tf = (Typeface.createFromAsset(getAssets(), "NanumGothicExtraBold.ttf"));
	    tv = (TextView) findViewById(R.id.tv_splash_logo);
	    tv.setTypeface(tf);
	    tv = (TextView) findViewById(R.id.tv_splash_teamName);
	    tv.setTypeface(tf);


        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                whileSplash();
                goNext();
            }
        },WAIT);




    } // #onCreate

    private void whileSplash()
    {
        // TODO : do something when sleeping
	    /*
	    if(prefs.getBoolean(PW486.PREF_KEY_INITIALIZED_DB, false)) // check initialized
	    {
		    LocalDBManager.getInstance(this).initDB();
		    editor.putBoolean(PW486.PREF_KEY_INITIALIZED_DB, true); editor.commit();
	    }
	    */


    }

    private void goNext()
    {
	    Intent i = null;
	    if(((CustomApplication)getApplication()).isLogged())
	    {
		    if(((CustomApplication)getApplication()).isFirst())
		    {
			    i = new Intent(this, SkinSelfDiagnosisStartActivity.class);
			    finish();
		    }
		    else
		    {
			    new LoadUserInfo().execute();
			    i = new Intent(this, MainActivity.class);
		    }
	    }
	    else
	    {
		    i = new Intent(this, SignInActivity.class);
	    }
	    startActivity(i);
                     /* Finish splash activity so user cant go back to it. */
		SplashActivity.this.finish();
                     /* Apply our splash exit (fade out) and main
                        entry (fade in) animation transitions. */


    }

	private class LoadUserInfo extends AsyncTask<Void, Void, UserDto>
	{

		@Override
		protected UserDto doInBackground(Void... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.UserService service = retrofit.create(API_Internal.Services.UserService.class);

				Call<UserResult> call = service.getUser(((CustomApplication)getApplication()).getUser().getId());

				Response<UserResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body().getUser();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
			}
			return null;
		} // #doInBackground

		@Override
		protected void onPostExecute(UserDto user)
		{
			if(user == null)
			{
				Toast.makeText(SplashActivity.this, "사용자 정보를 가져오지 못했습니다", Toast.LENGTH_SHORT).show();
				return;
			}
			((CustomApplication)getApplication()).setUser(user);
		}
	}
}
