package com.likefirst.protectskin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.views.SchedulerCalendarView;

import org.joda.time.DateTime;

/**
 * Created by jinwoo on 2016-07-13.
 */
public class SchedulerActivity extends AppCompatActivity implements SchedulerCalendarView.OnDateClickListener
{
	private static final String TAG = "SchedulerAct";

	private SchedulerCalendarView scvCalendar;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_submenu_care_scheduler);

		scvCalendar = (SchedulerCalendarView)findViewById(R.id.scv_scheduler_calendar);

		scvCalendar.setOnDateClickListener(this);
	}

	@Override
	public void onClick(DateTime t)
	{
		Log.d(TAG, "clicked date : " + t.toString("yyyy-MM-dd hh:mm:ss"));
	}
}
