package com.likefirst.protectskin.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.objects.SkinSelfDiagnosisQuestion;
import com.likefirst.protectskin.objects.SkinType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-10.
 */
public class SkinSelfDiagnosisQuestionActivityModel
{
	private static final String TAG = "SSDiagAct";
	public static final int ANSWER_YES = 1;
	public static final int ANSWER_NO = 2;
	public static final int ANSWER_NOT_KNOW = 3;
	public static final int ANSWER_START = 4;

	private Context mContext;

	private List<SkinSelfDiagnosisQuestion> list;
	private SkinSelfDiagnosisQuestion currentQuestion;

	public SkinSelfDiagnosisQuestionActivityModel(Context context)
	{
		mContext = context;
		list = getQuestionList_Type1();
	}

	public SkinSelfDiagnosisQuestion getNext(int ans)
	{
//		Log.d(TAG, "curr : " + (currentQuestion == null ? null : currentQuestion.getQuestion()) +
//				(currentQuestion == null ? null : ("\nY : " +
//						(currentQuestion.getYNext() == null ? null : currentQuestion.getYNext().getQuestion()) +
//				"\nN : " + (currentQuestion.getNNext() == null ? null : currentQuestion.getNNext().getQuestion()) +
//				"\nU : " + (currentQuestion.getUNext() == null ? null : currentQuestion.getUNext().getQuestion()))));
		switch (ans)
		{
			case ANSWER_YES:
				if(currentQuestion.getYNext() != null){currentQuestion.getYNext().setPrev(currentQuestion);}
				currentQuestion = currentQuestion.getYNext();
				break;
			case ANSWER_NO:
				if(currentQuestion.getNNext() != null){currentQuestion.getNNext().setPrev(currentQuestion);}
				currentQuestion = currentQuestion.getNNext();
				break;
			case ANSWER_NOT_KNOW:
				if(currentQuestion.getUNext() != null){currentQuestion.getUNext().setPrev(currentQuestion);}
				currentQuestion = currentQuestion.getUNext();
				break;
			case ANSWER_START:
				currentQuestion = list.get(0);
				break;
		}

		return currentQuestion;
	}

	public SkinSelfDiagnosisQuestion getPrev()
	{
		return currentQuestion = currentQuestion.getPrev();
	}

	public SkinType getResult(SkinSelfDiagnosisQuestion q, int ans)
	{
		switch(ans)
		{
			case ANSWER_YES:
				return q.getYResult();
			case ANSWER_NO:
				return q.getnResult();
			case ANSWER_NOT_KNOW:
				return q.getUResult();
		}
		return null;
	}


	public SkinSelfDiagnosisQuestion getCurrentQuestion(){return currentQuestion;}

	private List<SkinSelfDiagnosisQuestion> getQuestionList_Type1()
	{
		String[] m;
		List<SkinSelfDiagnosisQuestion> q = new ArrayList<>();
		List<SkinType> t = new ArrayList<>();

		t.add(new SkinType(SkinType.SKIN_TYPE_DRY));
		t.add(new SkinType(SkinType.SKIN_TYPE_GENERAL));
		t.add(new SkinType(SkinType.SKIN_TYPE_OILY));
		t.add(new SkinType(SkinType.SKIN_TYPE_COMPLEX));
		t.add(new SkinType(SkinType.SKIN_TYPE_SENSITIVE));

		m = mContext.getResources().getStringArray(R.array.question_type_1);

		for(String s : m)
		{
			q.add(new SkinSelfDiagnosisQuestion(s));
		}

		// set first/last question flag
		q.get(0).setIsFirst(true);
		for(int i = 20 ; i <= 24; i++) {q.get(i).setIsLast(true);}

		// set nodes
		q.get(0).setNode(q.get(5), q.get(1), q.get(6));
		q.get(1).setNode(q.get(5), q.get(2), q.get(7));
		q.get(2).setNode(q.get(7), q.get(3), q.get(8));
		q.get(3).setNode(q.get(6), q.get(4), q.get(5));
		q.get(4).setNode(q.get(9), q.get(5), q.get(8));
		q.get(5).setNode(q.get(10), q.get(6), q.get(11));
		q.get(6).setNode(q.get(11), q.get(7), q.get(12));
		q.get(7).setNode(q.get(12), q.get(8), q.get(13));
		q.get(8).setNode(q.get(13), q.get(9), q.get(14));
		q.get(9).setNode(q.get(14), q.get(10), q.get(10));
		q.get(10).setNode(q.get(15), q.get(11), q.get(17));
		q.get(11).setNode(q.get(16), q.get(12), q.get(15));
		q.get(12).setNode(q.get(17), q.get(13), q.get(16));
		q.get(13).setNode(q.get(18), q.get(14), q.get(17));
		q.get(14).setNode(q.get(19), q.get(16), q.get(18));
		q.get(15).setNode(q.get(20), q.get(16), q.get(21));
		q.get(16).setNode(q.get(21), q.get(17), q.get(23));
		q.get(17).setNode(q.get(22), q.get(18), q.get(23));
		q.get(18).setNode(q.get(23), q.get(19), null);
		q.get(19).setNode(q.get(24), q.get(10), null);
		q.get(20).setNode(null, q.get(21), null); q.get(20).setResult(t.get(0), null, t.get(1));
		q.get(21).setNode(null, q.get(22), null); q.get(21).setResult(t.get(1), null, t.get(0));
		q.get(22).setNode(null, q.get(23), null); q.get(22).setResult(t.get(2), null, t.get(3));
		q.get(23).setNode(null, q.get(24), null); q.get(23).setResult(t.get(3), null, t.get(2));
		q.get(24).setNode(null, null, null); q.get(24).setResult(t.get(4), null, t.get(3));
		return q;
	}

	public interface SkinSelfDiagnosisActivityPresenter
	{

		public void setView(View view);
		public void onCreate(Bundle savedInstanceState);
		public void doNext(int answer);
		public void doPrev();
		public void askLater();

		public interface View
		{
			public void showQuestion(SkinSelfDiagnosisQuestion q);
			public void showResult(SkinType t);
			public void exit();
		}
	}
}
