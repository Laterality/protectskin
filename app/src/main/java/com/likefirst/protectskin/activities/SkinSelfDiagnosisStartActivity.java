package com.likefirst.protectskin.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.objects.SkinSelfDiagnosisQuestion;
import com.likefirst.protectskin.utilities.Utilities;

/**
 * Created by jinwoo on 2016-07-20.
 */
public class SkinSelfDiagnosisStartActivity extends AppCompatActivity implements View.OnClickListener
{

	private LinearLayout llStart;
	private Button btnLater;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.sliding_ltr,R.anim.sliding_rtl);
		setContentView(R.layout.activity_skin_self_diagnosis_start);

		llStart = (LinearLayout)findViewById(R.id.ll_skin_self_diagnosis_start_start);
		btnLater = (Button) findViewById(R.id.btn_skin_self_diagnosis_start_skip);

		Typeface tf = (Typeface.createFromAsset(getAssets(), "NanumBarunGothic-YetHangul.ttf"));
		btnLater.setTypeface(tf);
		TextView tv = (TextView) findViewById(R.id.tv_self_diagnosis_descriptive_text1);
		tv.setTypeface(tf);
		tv = (TextView) findViewById(R.id.tv_self_diagnosis_descriptive_text2);
		tv.setTypeface(tf);

		llStart.setOnClickListener(this);
		btnLater.setOnClickListener(this);


	}

	@Override
	public void onClick(View view)
	{
		Intent i = null;
		switch (view.getId())
		{
			case R.id.ll_skin_self_diagnosis_start_start:
				i = new Intent(this, SkinSelfDiagnosisQuestionActivity.class);
				if(getIntent().getBooleanExtra("needMain", false)){i.putExtra("needMain", true);}
				startActivity(i);
				break;
			case R.id.btn_skin_self_diagnosis_start_skip:
				//i = new Intent(this, MainActivity.class);
				if(getIntent().getBooleanExtra("needMain", false))
				{
					i = new Intent(this, MainActivity.class);
					startActivity(i);
				}
				break;
			default:
				break;
		}
		finish();
	}

	@Override
	public void onBackPressed()
	{
		Utilities.askMeOut(this);
	}
}
