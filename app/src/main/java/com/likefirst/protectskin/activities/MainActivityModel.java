package com.likefirst.protectskin.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.likefirst.protectskin.API_External;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.fragments.MenuCareFragment;
import com.likefirst.protectskin.fragments.MenuCommunityFragment;
import com.likefirst.protectskin.fragments.MenuHomeFragment;
import com.likefirst.protectskin.fragments.SubmenuCareDiagnosisFragment;
import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetMinuteDustApiDto;
import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetUVApiDto;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.GPSManager;
import com.likefirst.protectskin.utilities.InteractionFragment;
import com.likefirst.protectskin.utilities.Utilities;
import com.likefirst.protectskin.views.MainTabTitleView;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-10.
 */
public class MainActivityModel implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, InteractionFragment.IFragmentRequest
{
	private static final String TAG = "MainAct";

	public final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
	public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;

	public static final int REQUEST_CAMERA = 1004;

	protected static final int REQUEST_CHECK_SETTINGS = 0x1;
	private AsyncCallback callback;
	private AppCompatActivity activity;
	private FragmentManager fm;

	private int flContent;

	private int currentPosition = 0;

	private static MenuCareFragment careFragment;
	private MenuHomeFragment homeFragment;
	private static MenuCommunityFragment commFragment;

	public MainActivityModel(AppCompatActivity activity)
	{
		Log.d(TAG, "model construct");
		Log.d(TAG, "destroyed? " + (activity.isDestroyed() ? "true" : "false") + " from " + this.getClass().getSimpleName());
		this.activity = activity;
		fm = activity.getSupportFragmentManager();
	}

	public void init(AsyncCallback callback, int contentFrame)
	{
		Log.d(TAG, "main activity init");
		Log.d(TAG, "destroyed? " + (activity.isDestroyed() ? "true" : "false") + " from " + this.getClass().getSimpleName());
		this.callback = callback;
		careFragment = new MenuCareFragment(); careFragment.setRequestListener(this);
		homeFragment = new MenuHomeFragment(); homeFragment.setRequestListener(this);
		commFragment = new MenuCommunityFragment(); commFragment.setRequestListener(this);
		flContent = contentFrame;
		((CustomApplication)activity.getApplication()).firstSee();
	}


	public void showSettingDialog(final Activity activity)
	{
		Log.d(TAG, "show dialog...entry");

		GoogleApiClient gpc = new GoogleApiClient.Builder(activity)
				.addApi(LocationServices.API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();

		gpc.connect();
		LocationRequest lr_ha = new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		LocationRequest lr_ba = new LocationRequest().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
				.setAlwaysShow(true)
				.addLocationRequest(lr_ha)
				.addLocationRequest(lr_ba);

		// builder.setNeedBle(true);

		PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(gpc, builder.build());

		result.setResultCallback(new ResultCallback<LocationSettingsResult>()
		{
			@Override
			public void onResult(LocationSettingsResult result)
			{
				final Status status = result.getStatus();
				final LocationSettingsStates lss = result.getLocationSettingsStates();
				switch (status.getStatusCode())
				{
					case LocationSettingsStatusCodes.SUCCESS:
						// All location settings are satisfied. The client the client can initialize location requests here
						final GPSManager gm = GPSManager.getInstance(MainActivityModel.this.activity);
						gm.getLocation(new GPSManager.LocationUpdateListener()
						{
							@Override
							public void onUpdate(double lat, double lon)
							{
								if(callback != null) {callback.onPreExecute();}
								new GetUVInfo().execute(lat, lon);
								new GetMinuteDustInfo().execute(lat, lon);
							}
						});
						break;
					case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
						// Location settings are not satisfied. But could be fixed by show the user a dialog
						try
						{
							// Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
							status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
						}
						catch (IntentSender.SendIntentException e)
						{
							// Ignore the error
						}
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						//Log.d(TAG, "setting change unavailable");
						// Location settings are not satisfied. However, we have no way to fix the settings so we won't show the dialog
						break;
				}
			}
		});
		Log.d(TAG, "end point");
	}

	public FragmentPagerAdapter getMenuPagerAdapter(AppCompatActivity activity)
	{
		return new SubmenuPagerAdapter(activity);
	}

	public Fragment getSubmenu(int position)
	{
		switch (position)
		{
			case 0:
				if(careFragment == null){careFragment = new MenuCareFragment(); careFragment.setRequestListener(this);}
				currentPosition = position;
				return careFragment;
			case 1:
				if(homeFragment == null){homeFragment = new MenuHomeFragment(); homeFragment.setRequestListener(this);}
				currentPosition = position;
				return homeFragment;
			case 2:
				if(commFragment == null){commFragment = new MenuCommunityFragment(); commFragment.setRequestListener(this);}
				currentPosition = position;
				return commFragment;
			default:
				return null;
		}
	}

	public void permissionProcess()
	{
		// Here, thisActivity is the current activity
		if (ContextCompat.checkSelfPermission(activity,
				Manifest.permission.READ_CONTACTS)
				!= PackageManager.PERMISSION_GRANTED) {

			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
					Manifest.permission.READ_CONTACTS)) {

				Log.d(TAG, "should explanation about the permissions");
				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.

			} else {
				Log.d(TAG, "no explanations are needed");
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(activity,
						new String[]{Manifest.permission.READ_CONTACTS,
						Manifest.permission.ACCESS_FINE_LOCATION,
						Manifest.permission.READ_EXTERNAL_STORAGE,
						Manifest.permission.WRITE_EXTERNAL_STORAGE,
						Manifest.permission.CAMERA},
						MY_PERMISSIONS_REQUEST_READ_CONTACTS);

				// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
				// app-defined int constant. The callback method gets the
				// result of the request.
			}
		}
	}

	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
	{
		switch (requestCode)
		{
			case MY_PERMISSIONS_REQUEST_READ_CONTACTS:
			{
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
				{
					Log.d(TAG, "permission grated");
					// permission was granted, yay! Do the
					// contacts-related task you need to do.

				} else
				{

					// permission denied, boo! Disable the
					// functionality that depends on this permission.
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}

	@Override
	public void showFragment(Fragment fragment)
	{
		if(fm == null){Log.d(TAG, "fm null");}
		else{
			try
			{
				Log.d(TAG, "destroyed ? " + (activity.isDestroyed() ? "true" : "false") + " before showFrag");
				activity.getSupportFragmentManager().beginTransaction().replace(flContent, fragment).commitAllowingStateLoss();
//				activity.getSupportFragmentManager().executePendingTransactions();
				Log.d(TAG, "destroyed? " + (activity.isDestroyed() ? "true" : "false") + " after showFrag");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
//		final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);

		switch (requestCode)
		{
			case REQUEST_CHECK_SETTINGS:
				switch (resultCode)
				{
					case Activity.RESULT_OK:
						// All required changes were successfully made
						Log.d(TAG, "Result OK");
						final GPSManager gm = GPSManager.getInstance(activity);
						final GPSManager.LocationUpdateListener lul = new GPSManager.LocationUpdateListener()
						{
							@Override
							public void onUpdate(double lat, double lon)
							{
								new GetUVInfo().execute(lat, lon);
								new GetMinuteDustInfo().execute(lat, lon);
							}
						};
						new Thread(new Runnable()
						{
							@Override
							public void run()
							{
								Looper.prepare();
								while(true)
								{
									gm.getLocation(lul);
									if(gm.getLatitude() != 0 && gm.getLon() != 0) {break;}
									else{try{Thread.sleep(100);}catch (Exception e){}}
								}
							}
						}).start();
						break;
					case Activity.RESULT_CANCELED:
						Log.d(TAG, "canceled");
						// The user was asked to change settings but chose not to
						break;
					default:
						break;
				}
				break;
			case REQUEST_CAMERA:
				switch (resultCode)
				{
					case Activity.RESULT_OK:
						Log.d(TAG, "got OK from camera intent");
						if(data == null)
						{
							Log.d(TAG, "gotten data is null");
						}
//						BitmapFactory.Options options = new BitmapFactory.Options();
//						options.inPreferredConfig = Bitmap.Config.RGB_565;
//						Bitmap bm = BitmapFactory.decodeFile(SubmenuCareDiagnosisFragment.filePath, options);
//						careFragment.setDiagnosisImage(bm);

//						ivCameraImage.setImageBitmap(bm);
//						Bundle extras = data.getExtras();
//						Bitmap imageBitmap = (Bitmap) extras.get("data");
						careFragment.setDiagnosisImage(data);
						break;
				}
				break;
		}
	}

	public void refreshHome()
	{
		homeFragment.invokeRefreshHome();
	}


	public class SubmenuPagerAdapter extends FragmentPagerAdapter
	{
		private static final int PAGE_COUNT = 3;

		public SubmenuPagerAdapter(AppCompatActivity activity)
		{
			super(activity.getSupportFragmentManager());
		}

		@Override
		public int getCount()
		{
			return PAGE_COUNT;
		}

		@Override
		public Fragment getItem(int position)
		{
			currentPosition = position;
			return getSubmenu(position);
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			String[] list = activity.getResources().getStringArray(R.array.tab_menu);
			return list[position];
		}

		public View getTabView(int position)
		{
			MainTabTitleView mttv = new MainTabTitleView(activity);
			mttv.setText(getPageTitle(position));
			return mttv;
		}
	}

	@Override
	public void onConnected(@Nullable Bundle bundle) {}

	@Override
	public void onConnectionSuspended(int i) {}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

	public void setAsyncCallback(AsyncCallback callback)
	{
		this.callback = callback;
	}

	public void onBackPressed()
	{
		if(((InteractionFragment)getSubmenu(currentPosition)).onBackPressed()){}
		else
		{
			Utilities.askMeOut(activity);
		}
	}

	public void setActvity(AppCompatActivity activity){this.activity = activity;}

	private class GetMinuteDustInfo extends AsyncTask<Double, Void, WeatherPlanetMinuteDustApiDto>
	{
		private static final String TAG = "GetMinuteDust";
		@Override
		protected WeatherPlanetMinuteDustApiDto doInBackground(Double... params)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder().baseUrl(API_External.BASE_URL_SKPLANET_API)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_External.Services.getSkplanetMinuteDustService service = retrofit.create(API_External.Services.getSkplanetMinuteDustService.class);

				Call call = service.getDustInfo("1", String.valueOf(params[0]), String.valueOf(params[1]));

				Response res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return (WeatherPlanetMinuteDustApiDto) res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
					return null;
				}
			}
			catch (Exception e)
			{
				Log.d(TAG, "Exception occurred");
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(WeatherPlanetMinuteDustApiDto body)
		{
			if(callback!=null){callback.onGetDustInfo(body);}
		}
	}// GetMinuteDustInfo

	private class GetUVInfo extends AsyncTask<Double, Void, WeatherPlanetUVApiDto>
	{
		private static final String TAG = "GetUVInfo";

		@Override
		protected WeatherPlanetUVApiDto doInBackground(Double... params)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_External.BASE_URL_SKPLANET_API)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_External.Services.getSkplanetUVService service = retrofit.create(API_External.Services.getSkplanetUVService.class);

				Call call = service.getUVInfo("1", String.valueOf(params[0]), String.valueOf(params[1]));

				Response res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return (WeatherPlanetUVApiDto)res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(WeatherPlanetUVApiDto body)
		{
			if(callback != null){callback.onGetUVInfo(body);}
		}
	}// GetUVInfo

	public interface AsyncCallback
	{
		public void onPreExecute();
		public void onGetDustInfo(WeatherPlanetMinuteDustApiDto dust);
		public void onGetUVInfo(WeatherPlanetUVApiDto uv);
	}

	public interface MainActivityPresenter
	{
		public void setView(View view);

		public void onCreate(Bundle savedInstanceState);
		public void onActivityResult(int requestCode, int resultCode, Intent data);
		public void onResume(AppCompatActivity activity);
		public void onBackPressed();
		public void showGPSRequireDialog(Activity activity);
		public void setAsyncCallback(AsyncCallback callback);
		public FragmentPagerAdapter getMenuPagerAdapter(AppCompatActivity activity);
		public void onTabSelected(TabLayout.Tab tab);
		public void checkPermission();
		public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
		public void refreshHome();

		public interface View
		{
			public void setUVIndicator(String value);
			public void setUVGrade(String grade);
			public void setDustIndicator(String value);
			public void setDustGrade(String grade);
			public void setRecommendation(String msg);
		}
	}
}
