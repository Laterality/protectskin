package com.likefirst.protectskin.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.fragments.SubmenuCareDiagnosisFragment;
import com.likefirst.protectskin.utilities.GPSActivity;
import com.likefirst.protectskin.utilities.PW486;
import com.likefirst.protectskin.utilities.ViewServer;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity implements MainActivityModel.MainActivityPresenter.View, View.OnClickListener, TabLayout.OnTabSelectedListener, GPSActivity
{
	private static final String TAG = "MainActivity";

	private MainActivityModel.MainActivityPresenter presenter;

	private TabLayout tlTab;

	private ViewPager vpMainmenu;

	private ImageView ivCameraImage;

	private TextView tvUVIndicator;
	private TextView tvDustIndicator;
	private TextView tvUVGrade;
	private TextView tvDustGrade;
	private TextView tvRecommendation;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);


		this.overridePendingTransition(R.anim.sliding_ltr, R.anim.sliding_rtl);
		setContentView(R.layout.activity_main);

		Log.d(TAG, "#onCreate");
		Log.d(TAG, "destroyed? " + (isDestroyed() ? "true" : "false"));
		Intent serviceIntent = new Intent(PW486.INTENT_FILTER_ACTION_SERVICE_SCHEDULE_MANAGER);
		serviceIntent.setPackage(getPackageName());
		startService(serviceIntent);

		tlTab = (TabLayout) findViewById(R.id.tl_main_menu);

		vpMainmenu = (ViewPager) findViewById(R.id.vp_main_content);

		ivCameraImage = (ImageView) findViewById(R.id.iv_care_camera_image);

		tvUVIndicator = (TextView) findViewById(R.id.tv_main_uv_indicator);
		tvDustIndicator = (TextView) findViewById(R.id.tv_main_pm_indicator);
		tvUVGrade = (TextView) findViewById(R.id.tv_main_uv_grade);
		tvDustGrade = (TextView) findViewById(R.id.tv_main_pm_grade);
		tvRecommendation = (TextView) findViewById(R.id.tv_main_recommendation_by_indicator);


		presenter = new MainActivityPresenterImpl(this, R.id.fl_main_content);
		presenter.setView(this);
		presenter.onCreate(savedInstanceState);
		presenter.checkPermission();

		MainActivityModel.SubmenuPagerAdapter adapter = (MainActivityModel.SubmenuPagerAdapter) presenter.getMenuPagerAdapter(this);

		vpMainmenu.setAdapter(adapter);
		tlTab.setupWithViewPager(vpMainmenu);

		for (int i = 0; i < adapter.getCount(); i++)
		{
			tlTab.getTabAt(i).setCustomView(adapter.getTabView(i));
		}

		tlTab.addOnTabSelectedListener(this);

		try
		{
			new Handler().postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					tlTab.getTabAt(1).select();
				}
			}, 100);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		ViewServer.get(this).addWindow(this);
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		presenter.onResume(this);
		presenter.refreshHome();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		//No call for super(). Bug on API Level > 11.
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		presenter.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		ViewServer.get(this).removeWindow(this);
		Log.d(TAG, "activity destroy");
	}


	@Override
	public void setUVIndicator(String value)
	{
		if (tvUVIndicator != null)
		{
			tvUVIndicator.setText(value);
		}
	}

	@Override
	public void setUVGrade(String grade)
	{
		if (tvUVGrade != null)
		{
			tvUVGrade.setText(grade);
		}
	}

	@Override
	public void setDustIndicator(String value)
	{
		if (tvDustIndicator != null)
		{
			tvDustIndicator.setText(value);
		}
	}

	@Override
	public void setDustGrade(String grade)
	{
		if (tvDustGrade != null)
		{
			tvDustGrade.setText(grade);
		}
	}

	@Override
	public void setRecommendation(String msg)
	{
		if (tvRecommendation != null)
		{
			tvRecommendation.setText(msg);
		}
	}


	@Override
	public void onClick(View view)
	{
		/*
		switch (view.getId())
		{
			case R.id.ll_main_menu_scheduler:
				presenter.goMenuScheduler();
				break;
		}
		*/
	}


	@Override
	public void onTabSelected(TabLayout.Tab tab)
	{
		presenter.onTabSelected(tab);
	}

	@Override
	public void onTabUnselected(TabLayout.Tab tab)
	{

	}

	@Override
	public void onTabReselected(TabLayout.Tab tab)
	{

	}

	@Override
	public void onBackPressed()
	{
		presenter.onBackPressed();
	}

	@Override
	public void showRequestGPSDialog(MainActivityModel.AsyncCallback callback)
	{
		Log.d(TAG, "show requesting gps dialog ...");
		if (callback != null)
		{
			presenter.setAsyncCallback(callback);
		}
		presenter.showGPSRequireDialog(this);
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
	{
		presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}



//	@Override
//	public void onStart()
//	{
//		super.onStart();
//
//		// ATTENTION: This was auto-generated to implement the App Indexing API.
//		// See https://g.co/AppIndexing/AndroidStudio for more information.
//		client.connect();
//		Action viewAction = Action.newAction(Action.TYPE_VIEW, // TODO: choose an action type.
//				"Main Page", // TODO: Define a title for the content shown.
//				// TODO: If you have web page content that matches this app activity's content,
//				// make sure this auto-generated web page URL is correct.
//				// Otherwise, set the URL to null.
//				Uri.parse("http://host/path"),
//				// TODO: Make sure this auto-generated app URL is correct.
//				Uri.parse("android-app://com.likefirst.protectskin.activities/http/host/path"));
//		AppIndex.AppIndexApi.start(client, viewAction);
//	}
//
//	@Override
//	public void onStop()
//	{
//		super.onStop();
//
//		// ATTENTION: This was auto-generated to implement the App Indexing API.
//		// See https://g.co/AppIndexing/AndroidStudio for more information.
//		Action viewAction = Action.newAction(Action.TYPE_VIEW, // TODO: choose an action type.
//				"Main Page", // TODO: Define a title for the content shown.
//				// TODO: If you have web page content that matches this app activity's content,
//				// make sure this auto-generated web page URL is correct.
//				// Otherwise, set the URL to null.
//				Uri.parse("http://host/path"),
//				// TODO: Make sure this auto-generated app URL is correct.
//				Uri.parse("android-app://com.likefirst.protectskin.activities/http/host/path"));
//		AppIndex.AppIndexApi.end(client, viewAction);
//		client.disconnect();
//	}
}
