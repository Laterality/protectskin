package com.likefirst.protectskin.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.SkinTypeDto;
import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.results.UserResult2;
import com.likefirst.protectskin.fragments.SkinTypeDescriptionDryFragment;
import com.likefirst.protectskin.objects.SkinType;
import com.likefirst.protectskin.utilities.CustomApplication;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-12.
 */
public class SkinSelfDiagnosisResultActivity extends AppCompatActivity implements View.OnClickListener
{
	private static final String TAG = "DiagResultAct";

	public static final String INTENT_KEY_RESULT = "result";
	public static final String INTENT_KEY_RESULT_ID = "resid";

	private ImageView ivClose;
	private TextView tvResult;
//	private TextView tvDesc;
	private int type;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.sliding_ltr,R.anim.sliding_rtl);
		setContentView(R.layout.activity_skin_self_diagnosis_result);

		tvResult = (TextView) findViewById(R.id.tv_skin_self_diagnosis_result_type);
//		tvDesc = (TextView) findViewById(R.id.tv_skin_self_diagnosis_result_desc);
		ivClose = (ImageView) findViewById(R.id.iv_skin_self_diagnosis_result_close);

		Typeface tf = Typeface.createFromAsset(getAssets(), "NanumSquareB.ttf");
		TextView tv = (TextView) findViewById(R.id.tv_diagnosis_result_title);
		tv.setTypeface(tf);
		tv = (TextView) findViewById(R.id.tv_diagnosis_result_subtitle);
		tv.setTypeface(tf);
		tf = Typeface.createFromAsset(getAssets(), "NanumBarunGothic-YetHangul.ttf");
		tv = (TextView) findViewById(R.id.tv_diagnosis_result_your_skin_type_is);
		tv.setTypeface(tf);
//		tvDesc.setTypeface(tf);
		tf = Typeface.createFromAsset(getAssets(), "NanumGothicExtraBold.ttf");
		tvResult.setTypeface(tf);

		type = getIntent().getIntExtra(INTENT_KEY_RESULT, -1);


		tvResult.setText(SkinType.codeToName(type));
//		tvDesc.setText("피.부.설.명\n크크큭");
		setDescription(type);
		ivClose.setOnClickListener(this);

		new UpdateUserSkinType().execute(SkinType.codeToId(type));




	}

	private void setDescription(int type)
	{
		switch (type)
		{
			case SkinType.SKIN_TYPE_DRY:
				getSupportFragmentManager().beginTransaction().replace(R.id.fl_self_diagnosis_result_description, new SkinTypeDescriptionDryFragment()).commit();
				break;
		}
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.iv_skin_self_diagnosis_result_close:
				onBackPressed();
				break;
		}
	}

	@Override
	public void onBackPressed()
	{
		if(getIntent().getBooleanExtra("needMain", false)){startActivity(new Intent(this, MainActivity.class));}
		finish();
	}

	private class UpdateUserSkinType extends AsyncTask<String, Void, UserResult2>
	{

		@Override
		protected UserResult2 doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.UserService service = retrofit.create(API_Internal.Services.UserService.class);

				UserDto user = ((CustomApplication)getApplication()).getUser();

				Call<UserResult2> call = service.updateUser(user.getId(),
						new UserDto(
							null,
							null,
							new SkinTypeDto(param[0])
				));


				Response<UserResult2> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(UserResult2 result)
		{
			if(result == null)
			{
				Toast.makeText(SkinSelfDiagnosisResultActivity.this, "결과 업데이트에 실패했습니다", Toast.LENGTH_SHORT).show();
				return;
			}

			String strResult = result.getResult();

			if (strResult.equals(Result.RESULT_SUCCESS))
			{
				Log.d(TAG, "user : " + result.getUser().getId() + ", from " + this.getClass().getSimpleName());
				((CustomApplication)getApplication()).setUser(result.getUser());
			}
		}


	}

}
