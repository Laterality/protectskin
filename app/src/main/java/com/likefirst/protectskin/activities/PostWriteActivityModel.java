package com.likefirst.protectskin.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.PostDto2;
import com.likefirst.protectskin.api_dto.PostElement;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.UploadDto;
import com.likefirst.protectskin.api_dto.results.ImageUploadResult;
import com.likefirst.protectskin.api_dto.results.PostResult;
import com.likefirst.protectskin.api_dto.results.PostResult2;
import com.likefirst.protectskin.utilities.Utilities;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-08-14.
 */
public class PostWriteActivityModel
{
	private static final String TAG = "PostWriteAct";
	private static final int PICK_IMAGE_REQUEST = 1;
	private Activity activity;
	private PostDto2 toPublish;
	private PostDto2 published;

	private ProgressDialog pdPost;
	private ProgressDialog pdUpload;


	public PostWriteActivityModel(Activity activity)
	{
		this.activity = activity;
	}


	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
	{
		EditText etText = (EditText) view.findViewById(R.id.tv_post_content_text);
		Utilities.setEditable(etText, true);
		etText.requestFocus();
	}




	public void onBackPressed()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("게시물 작성 취소");
		builder.setMessage("작성을 취소하시겠습니까?");
		builder.setNegativeButton("취소", null);
		builder.setPositiveButton("확인", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				activity.finish();
			}
		});
		builder.show();
	}

	public void showImagePicker()
	{
		Intent intent = new Intent();
		// Show only images, no videos or anything else
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		// Always show the chooser (if there are multiple options available)
		activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
	}

	protected Uri onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

			Uri uri = data.getData();

			//String realpath = Utilities.RealPathUtils.getRealPathFromURI_API19(activity, uri);

//			try {
//				Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
//				 // Log.d(TAG, String.valueOf(bitmap));
//
//				return uri;
//			}
//			catch (IOException e) {
//				e.printStackTrace();
//			}
			return uri;
		}
		return null;
	}

	public void publish(PostDto2 post)
	{
		toPublish = post;
		List<PostElement> elems = post.getContent();
		List<PostElement> selems = new ArrayList<>();
		for(PostElement e : elems)
		{
			selems.add(e.sanitize());
		}
		pdPost = new ProgressDialog(activity);
		pdPost.setMessage("게시물 전송중");
		pdPost.setCancelable(false);
		pdPost.show();
		new PublishPost().execute(new PostDto2(post.getAuthor(), post.getBoardId(), post.getSkinTypeId(), post.getTitle(), selems));

	}

	private class PublishPost extends AsyncTask<PostDto2, Void, PostResult2>
	{


		@Override
		protected PostResult2 doInBackground(PostDto2... post)
		{
			try
			{
				Log.d(TAG, "publish post entry point");
				Retrofit retrofit = new Retrofit.Builder().baseUrl(API_Internal.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

				API_Internal.Services.PostService postService = retrofit.create(API_Internal.Services.PostService.class);

				Call<PostResult2> call = postService.createPost(post[0]);

				Response<PostResult2> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(PostResult2 result)
		{
			pdPost.dismiss();
			if(result == null)
			{
				Utilities.showToast(activity, "게시물을 전송하는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}
			else if(result.getResult().equals(Result.RESULT_ERROR))
			{
				Utilities.showToast(activity, "게시물을 전송하는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}
			else if(result.getResult().equals(Result.RESULT_SUCCESS))
			{
				// 이미지 업로드 Task로 이행
				published = result.getPost();

				pdUpload = new ProgressDialog(activity);
				pdUpload.setMessage("이미지 전송 중");
				pdUpload.setCancelable(false);
				pdUpload.show();
				new ImageUpload().execute();
			}
		}
	}

	private class ImageUpload extends AsyncTask<PostDto2, Void, Boolean>
	{


		@Override
		protected Boolean doInBackground(PostDto2... post)
		{
			try
			{

				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();


				API_Internal.Services.ImageUploadService service = retrofit.create(API_Internal.Services.ImageUploadService.class);

				List<PostElement> elms = toPublish.getContent();

				File f;
				RequestBody rb;
				Call<ImageUploadResult> call;
				Response<ImageUploadResult> res;
				for(PostElement e : elms)
				{
					if(e.getType().equals(PostElement.TYPE_IMAGE_LOCAL))
					{
						String realpath = Utilities.RealPathUtils.getPath(activity, e.getUri());
						Log.d(TAG, "image path : " + realpath);
						f = new File(realpath);
						String ext = FilenameUtils.getExtension(f.getName());
						rb = RequestBody.create(MediaType.parse("image/*"), f);
						MultipartBody.Part mp = MultipartBody.Part.createFormData("boardimg", f.getName(), rb);
						call = service.uploadBoardImage(mp, RequestBody.create(MediaType.parse("application/json"),
								"{" +
								"\"id\" : \"" + published.getId() + "\", " +
								"\"no\" : " + e.getIndex() + "," +
										"\"ext\" : \"" + ext + "\"" +
										"}"
						));
						res = call.execute();

						if(res.isSuccessful())
						{
							Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
						}
						else
						{
							Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
							Log.d(TAG, res.code() + " : " + res.message());
							return false;
						}
					}
				}
				return true;
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			pdUpload.dismiss();
			if(!result)
			{
				Log.d(TAG, "Upload fail from " + this.getClass().getSimpleName());
				Utilities.showToast(activity, "이미지 업로드에 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}
			else
			{
				Utilities.showToast(activity, "게시물 전송을 완료했습니다", Toast.LENGTH_SHORT);
			}
			activity.finish();
		}

	}

	public interface PostWriteActivityPresenter
	{

		public void setView(View view);

		public void onBackPressed();
		public void submit(PostDto2 post);
		public void showImagePicker();
		public void onActivityResult(int requestCode, int resultCode, Intent data);
		public void onItemClick(AdapterView<?> adapterView, android.view.View view, int i, long l);

		public interface View
		{
			public void addImage(Uri uri);
		}
	}
}
