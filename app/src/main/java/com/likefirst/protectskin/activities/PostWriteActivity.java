package com.likefirst.protectskin.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Dimension;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.adapters.PostCategorySpinnerAdapter;
import com.likefirst.protectskin.adapters.PostContentAdapter;
import com.likefirst.protectskin.api_dto.PostDto2;
import com.likefirst.protectskin.api_dto.PostElement;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.PW486;
import com.likefirst.protectskin.utilities.Utilities;
import com.mobeta.android.dslv.DragSortListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.latera.reorderablelistview.ReorderableListView;

/**
 * Created by jinwoo on 2016-08-12.
 */
public class PostWriteActivity extends AppCompatActivity
		implements PostWriteActivityModel.PostWriteActivityPresenter.View, View.OnClickListener,
		DragSortListView.DragSortListener, AdapterView.OnItemClickListener, View.OnFocusChangeListener,
		TextWatcher
{
	private static final String TAG = "PostWriteAct";
	private static final int MODE_NONE = 0;
	private static final int MODE_TEXT_NEW = 1;
	private static final int MODE_TEXT_EDIT = 2;

	private TextView tvCancel;
	private TextView tvSubmit;
	private TextView tvAddImg;
	private TextView tvAddText;
	private EditText etTitle;
//	private EditText etText;
//	private DragSortListView lvContents;
	private LinearLayout llContentBlock;
	private Spinner spCategory;
	private EditText editView;

//	private PostContentAdapter pcAdapter;

	private PostWriteActivityModel.PostWriteActivityPresenter presenter;
	private InputMethodManager imm;


	private List<PostElement> elements;
	private String currentSkinType;
	private String boardId;
	private int indexCount = 0;
	private int editMode = 0;
	private int editIndex = -1;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post_write);


		boardId = getIntent().getStringExtra("board");

		presenter = new PostWriteActivityPresenterImpl(this);
		presenter.setView(this);

		tvCancel = (TextView) findViewById(R.id.tv_write_cancel);
		tvSubmit = (TextView) findViewById(R.id.tv_write_submit);
		tvAddImg = (TextView) findViewById(R.id.tv_write_add_img);
		tvAddText = (TextView) findViewById(R.id.tv_write_content_footer_text);
		etTitle = (EditText) findViewById(R.id.et_write_title);
//		etText = (EditText) findViewById(R.id.et_write_text);
//		lvContents = (DragSortListView) findViewById(R.id.lv_write_contents);
		llContentBlock = (LinearLayout) findViewById(R.id.ll_write_content_block);
		spCategory = (Spinner) findViewById(R.id.sp_write_category);

		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		elements = new ArrayList<>();

//		lvContents.setAdapter(pcAdapter = new PostContentAdapter(this, null));
		spCategory.setAdapter(new PostCategorySpinnerAdapter(this, false));

		tvCancel.setOnClickListener(this);
		tvSubmit.setOnClickListener(this);
		tvAddImg.setOnClickListener(this);
		tvAddText.setOnClickListener(this);



//		etText.setOnFocusChangeListener(new View.OnFocusChangeListener()
//		{
//			@Override
//			public void onFocusChange(View view, boolean b)
//			{
//				if(b)
//				{
//					// got focus
//					if(editMode == MODE_TEXT_EDIT)
//					{
//						tvSubmit.setText("텍스트 수정");
//					}
//				}
//				else
//				{
//					// lose focus
//					tvSubmit.setText("등록");
//					editMode = MODE_NONE;
//				}
//			}
//		});

//		etText.addTextChangedListener(new TextWatcher()
//		{
//			@Override
//			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
//			{
//
//			}
//
//			@Override
//			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
//			{
//				if(charSequence.length() >= 3)
//				{
//					if(editMode == MODE_NONE)
//					{
//						tvSubmit.setText("텍스트 추가");
//						editMode = MODE_TEXT_NEW;
//					}
//				}
//			}
//
//			@Override
//			public void afterTextChanged(Editable editable)
//			{
//
//			}
//		});


//		lvContents.setDragSortListener(this);
//		lvContents.setFloatViewManager(new DragSortListView.FloatViewManager()
//		{
//			@Override
//			public View onCreateFloatView(int i)
//			{
//				return pcAdapter.getView(i, null, null);
//			}
//
//			@Override
//			public void onDragFloatView(View view, Point point, Point point1)
//			{
//
//			}
//
//			@Override
//			public void onDestroyFloatView(View view)
//			{
//
//			}
//		});

//		lvContents.setOnItemClickListener(this);


		spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
			{
				switch (i)
				{
					case 0:
						currentSkinType = PW486.SKINTYPE_DRY;
						break;
					case 1:
						currentSkinType = PW486.SKINTYPE_GENERAL;
						break;
					case 2:
						currentSkinType = PW486.SKINTYPE_OILY;
						break;
					case 3:
						currentSkinType = PW486.SKINTYPE_COMPLEX;
						break;
					case 4:
						currentSkinType = PW486.SKINTYPE_SENSITIVE;
						break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView)
			{

			}
		});

	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.tv_write_cancel:
				presenter.onBackPressed();
				break;
			case R.id.tv_write_submit:
				presenter.submit(new PostDto2(((CustomApplication) getApplication()).getUser().getId(),
						boardId,
						currentSkinType,
						etTitle.getText().toString(),
						elements));
				break;
			case R.id.tv_write_add_img:
				presenter.showImagePicker();
				break;
			case R.id.tv_write_content_footer_text:
//				pcAdapter.addContent(new PostElement(indexCount++, PostElement.TYPE_TEXT, ""));
				int pad = Utilities.dpToPx(this, 8);
				PostElement e = new PostElement(indexCount++, PostElement.TYPE_TEXT, "");
				elements.add(e);
				EditText et = new EditText(this);
				et.setHint("내용 입력");
				et.setTextColor(Color.BLACK);
				et.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
				et.setPadding(pad, pad, pad, pad);
				et.setBackgroundColor(Color.WHITE);
				et.setTag(e);
				et.addTextChangedListener(this);
				et.setOnFocusChangeListener(this);
				llContentBlock.addView(et);

//				etText.setText("");
//				imm.hideSoftInputFromWindow(etText.getWindowToken(), 0);
//				lvContents.requestFocus();
				break;
		}
	}

	@Override
	public void onBackPressed()
	{
		presenter.onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);

		presenter.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void addImage(Uri uri)
	{
		if (uri == null)
		{
			return;
		}
		try
		{
			Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//			pcAdapter.addContent(new PostElement(indexCount++, PostElement.TYPE_IMAGE_LOCAL, uri, bm));
			elements.add(new PostElement(indexCount++, PostElement.TYPE_IMAGE_LOCAL, uri, bm));
			ImageView iv = new ImageView(this);
			iv.setAdjustViewBounds(true);
			iv.setImageBitmap(bm);
			llContentBlock.addView(iv);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void drag(int i, int i1)
	{
		Log.d(TAG, "#drag\ni : " + i + "\ni1 : " + i1);
	}

	@Override
	public void drop(int i, int i1)
	{
		Log.d(TAG, "#drop\ni : " + i + "\ni1 : " + i1);
//		lvContents.requestFocus();
//		pcAdapter.swapTo(i, i1);
	}

	@Override
	public void remove(int i)
	{
		Log.d(TAG, "#remove\ni : " + i);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
	{
		Log.d(TAG, "pos : " + i);
		presenter.onItemClick(adapterView, view, i, l);
	}

	@Override
	public void onFocusChange(View view, boolean b)
	{
		if(b)
		{
			// edit text get focused, change into edit mode
			editMode = MODE_TEXT_EDIT;
			PostElement e = (PostElement) view.getTag();
			editIndex = e.getIndex();
			editView = (EditText) view;
		}
		else
		{
			// edit text loses focus
			editMode = MODE_NONE;
			editIndex = -1;
		}
	}

	@Override
	public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
	{

	}

	@Override
	public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
	{
		elements.get(editIndex).setContent(editView.getText().toString());
	}

	@Override
	public void afterTextChanged(Editable editable)
	{

	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		for(PostElement e : elements)
		{
			if(e.getLocalImage() != null)
			{
				e.getLocalImage().recycle();
			}
		}
	}
}
