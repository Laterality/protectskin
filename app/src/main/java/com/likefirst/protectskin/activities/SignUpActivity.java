package com.likefirst.protectskin.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.model.people.Person;
import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.UserLoginDto;
import com.likefirst.protectskin.api_dto.UserPostDto;
import com.likefirst.protectskin.api_dto.results.UserResult;
import com.likefirst.protectskin.utilities.CustomApplication;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-22.
 */
public class SignUpActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener
{

	private static final String TAG = "SignUpAct";
	public static final String INTENT_REGISTER_LOGIN_TYPE = "loginType";
	public static final String INTENT_REGISTER_EXTERNAL_EMAIL = "email";
	public static final String INTENT_REGISTER_EXTERNAL_ID = "extId";

	private static final int RC_SIGN_IN = 1;

	private GoogleApiClient mGoogleApiClient;

	private LinearLayout llNoti;
	private LinearLayout llFormEmail;
	private LinearLayout llFormPassword;
	private LinearLayout llOr;
	private SignInButton btnSignin;
	private Button btnSubmit;
	private Button btnLogin;
	private EditText etEmail;
	private EditText etPassword;
	private EditText etUsername;
	private CheckBox cbMale;
	private CheckBox cbFemale;

	private String gender;
	private String currentLoginType = UserLoginDto.LOGIN_TYPE_NATIVE;

	private UserLoginDto latestLoginDto;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.sliding_ltr, R.anim.sliding_rtl);
		setContentView(R.layout.activity_signup);

		btnSignin = (SignInButton) findViewById(R.id.btn_signup_sign_in_button);
		btnSubmit = (Button) findViewById(R.id.btn_signup_submit);
		btnLogin = (Button) findViewById(R.id.btn_signup_goto_login);
		etEmail = (EditText) findViewById(R.id.et_signup_email);
		etPassword = (EditText) findViewById(R.id.et_signup_pw);
		etUsername = (EditText) findViewById(R.id.et_signup_username);
		cbMale = (CheckBox) findViewById(R.id.cb_signup_male);
		cbFemale = (CheckBox) findViewById(R.id.cb_signup_female);

		llNoti = (LinearLayout) findViewById(R.id.ll_signup_notice_additional);
		llFormEmail = (LinearLayout) findViewById(R.id.ll_signup_form_email);
		llFormPassword = (LinearLayout) findViewById(R.id.ll_signup_form_password);
		llOr = (LinearLayout) findViewById(R.id.ll_signup_or);

		Typeface tf = (Typeface.createFromAsset(getAssets(), "NanumBarunGothic-YetHangul.ttf"));
		List <TextView> tvl = new ArrayList();
		tvl.add((TextView) findViewById(R.id.tv_sign_up));
		tvl.add((TextView) findViewById(R.id.tv_sign_up_email));
		tvl.add((TextView) findViewById(R.id.tv_sign_up_gender));
		tvl.add((TextView) findViewById(R.id.tv_sign_up_or));
		tvl.add((TextView) findViewById(R.id.tv_sign_up_password));
		tvl.add((TextView) findViewById(R.id.tv_sign_up_user_name));
		for(int i=0;i<tvl.size();i++){
			tvl.get(i).setTypeface(tf);
		}
		btnSubmit.setTypeface(tf);
		btnLogin.setTypeface(tf);
		etEmail.setTypeface(tf);
		etPassword.setTypeface(tf);
		etUsername.setTypeface(tf);
		cbMale.setTypeface(tf);
		cbFemale.setTypeface(tf);

		btnSignin.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
		btnLogin.setOnClickListener(this);

		cbMale.setOnCheckedChangeListener(this);
		cbFemale.setOnCheckedChangeListener(this);
		// Configure sign-in to request the user's ID, email address, and basic
		// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestId()
				.requestScopes(new Scope(Scopes.EMAIL),
						new Scope(Scopes.PROFILE))
				.build();

		// Build a GoogleApiClient with access to the Google Sign-In API and the
		// options specified by gso.
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
				.addApi(Auth.GOOGLE_SIGN_IN_API, gso)
				.build();


		String loginType = getIntent().getStringExtra(INTENT_REGISTER_LOGIN_TYPE);
		String extEmail = getIntent().getStringExtra(INTENT_REGISTER_EXTERNAL_EMAIL);
		String extId = getIntent().getStringExtra(INTENT_REGISTER_EXTERNAL_ID);

		if(loginType != null && extEmail != null && extId != null)
		{
			Log.d(TAG, "loginType : " + loginType);
			Log.d(TAG, "email : " + extEmail);
			Log.d(TAG, "ext id : " + extId);
			new Login().execute(latestLoginDto = new UserLoginDto(loginType, extEmail, null, extId));
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_signup_sign_in_button:
				signIn();
				break;
			case R.id.btn_signup_submit:
				submit();
				break;
			case R.id.btn_signup_goto_login:
				startActivity(new Intent(this, SignInActivity.class));
				finish();
				break;
			// ...
		}
	}

	private void signIn() {
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	private void submit()
	{
		if(!(cbFemale.isChecked() || cbMale.isChecked()))
		{
			Toast.makeText(SignUpActivity.this, "성별을 선택해 주세요", Toast.LENGTH_SHORT).show();
			return;
		}
		Log.d(TAG, "submit, gender : " + gender);
		if(latestLoginDto != null)
		{
			new RegisterUser().execute(new UserPostDto(currentLoginType, latestLoginDto.getEmail(), etPassword.getText().toString(), etUsername.getText().toString(), latestLoginDto.getExternal_id(), gender));
		}
		else
		{
			new RegisterUser().execute(new UserPostDto(currentLoginType, etEmail.getText().toString(), etPassword.getText().toString(), etUsername.getText().toString(), null, gender));
		}

	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
		if (requestCode == RC_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			handleSignInResult(result);
		}
	}

	private void handleSignInResult(GoogleSignInResult result)
	{

		Log.d(TAG, "handleSignInResult:" + result.isSuccess());
		if (result.isSuccess())
		{
			// Signed in successfully, show authenticated UI.
			GoogleSignInAccount acct = result.getSignInAccount();

			Log.d(TAG, "Email : " + acct.getEmail());
			Log.d(TAG, "Id : " + acct.getId());
			new Login().execute(latestLoginDto = new UserLoginDto(UserLoginDto.LOGIN_TYPE_GOOGLE, acct.getEmail(), null, acct.getId()));
			//mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
			//updateUI(true);
		}
		else
		{
			// Signed out, show unauthenticated UI.
			//updateUI(false);
		}
	}

	private void updateUI(boolean googleLoggedIn)
	{
		if(googleLoggedIn)
		{
			llNoti.setVisibility(View.VISIBLE);
			llFormEmail.setVisibility(View.GONE);
			llFormPassword.setVisibility(View.GONE);
			currentLoginType = UserLoginDto.LOGIN_TYPE_GOOGLE;
			btnSignin.setVisibility(View.GONE);
			llOr.setVisibility(View.GONE);
		}
		else
		{
			llNoti.setVisibility(View.GONE);
			llFormEmail.setVisibility(View.VISIBLE);
			llFormPassword.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult)
	{
		Toast.makeText(SignUpActivity.this, "요청에 실패했습니다", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean b)
	{
		switch(compoundButton.getId())
		{
			case R.id.cb_signup_male:
				if(b)
				{
					gender = "MALE";
					cbFemale.setChecked(false);
				}
				break;
			case R.id.cb_signup_female:
				if(b)
				{
					gender = "FEMALE";
					cbMale.setChecked(false);
				}
				break;
		}
		if(!cbFemale.isChecked() && !cbMale.isChecked())
		{
			gender = null;
		}
		Log.d(TAG, "check changed, gender : " + gender);
	}

	@Override
	public void onBackPressed()
	{
		//Utilities.askMeOut(this);
		startActivity(new Intent(this, SignInActivity.class));
		finish();
	}

	private class RegisterUser extends AsyncTask<UserPostDto, Void, UserResult>
	{
		private ProgressDialog pd;

		@Override
		protected void onPreExecute()
		{
			this.pd = new ProgressDialog(SignUpActivity.this);
			pd.setMessage("등록 중");
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected UserResult doInBackground(UserPostDto... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.UserService service = retrofit.create(API_Internal.Services.UserService.class);

				Call<UserResult> call = service.registerUser(param[0]);

				Response<UserResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "execution is success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "execution is fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return null;
		} // doInBackground

		@Override
		protected void onPostExecute(UserResult result)
		{
			pd.dismiss();

			if(result == null)
			{
				Toast.makeText(SignUpActivity.this, "로그인 실패", Toast.LENGTH_SHORT).show();
				return;
			}

			String strResult = result.getResult();

			if(strResult.equals(Result.RESULT_SUCCESS))
			{
				UserDto user = result.getUser();
				Log.d(TAG, "registration success");
				((CustomApplication)getApplication()).setUser(user);
				if(((CustomApplication)getApplication()).isFirst())
				{
					startActivity(new Intent(SignUpActivity.this, SkinSelfDiagnosisStartActivity.class));
					finish();
				}
				else
				{
					startActivity(new Intent(SignUpActivity.this, MainActivity.class));
					finish();
				}
			}
			else if(strResult.equals(Result.RESULT_ERROR))
			{
				Toast.makeText(SignUpActivity.this, "등록 실패", Toast.LENGTH_SHORT).show();
				Log.d(TAG, "registration fail");
			}
		}
	}

	private class Login extends AsyncTask<UserLoginDto, Void, UserResult>
	{
		UserLoginDto loginInfo;
		private ProgressDialog pd;

		@Override
		protected void onPreExecute()
		{
			this.pd = new ProgressDialog(SignUpActivity.this);
			pd.setMessage("로그인");
			pd.setCancelable(false);
			pd.show();
		}


		@Override
		protected UserResult doInBackground(UserLoginDto... param)
		{
			loginInfo = param[0];
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.UserService service = retrofit.create(API_Internal.Services.UserService.class);
				Call call = service.loginUser(param[0]);

				Response<UserResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "execution is success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "execution is fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}// doInBackground

		@Override
		protected void onPostExecute(UserResult result)
		{
			pd.dismiss();

			if(result == null)
			{
				Log.d(TAG, "result is null from " + this.getClass().getSimpleName());
				return;
			}

			String strResult = result.getResult();
			UserDto user = result.getUser();

			if(strResult.equals(Result.RESULT_ERROR))
			{
				Toast.makeText(SignUpActivity.this, "로그인 실패", Toast.LENGTH_SHORT).show();
				Log.d(TAG, "login error");
				updateUI(false);
			}
			else if(strResult.equals(Result.RESULT_SUCCESS))
			{
				Log.d(TAG, "login success");
				((CustomApplication)getApplication()).setUser(user);
				if(((CustomApplication)getApplication()).isFirst())
				{
					Intent i = new Intent(SignUpActivity.this, SkinSelfDiagnosisStartActivity.class);
					i.putExtra("needMain", true);
					startActivity(i);
					startActivity(new Intent(SignUpActivity.this, MainActivity.class));
					finish();
				}
				else
				{
					startActivity(new Intent(SignUpActivity.this, MainActivity.class));
					finish();
				}
			}
			else if (strResult.equals(Result.RESULT_REGISTER))
			{
				Log.d(TAG, "register new account");
				updateUI(true);
			}
		}
	} // Login
}
