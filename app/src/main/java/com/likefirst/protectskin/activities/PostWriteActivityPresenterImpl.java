package com.likefirst.protectskin.activities;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.likefirst.protectskin.api_dto.PostDto2;

/**
 * Created by jinwoo on 2016-08-14.
 */
public class PostWriteActivityPresenterImpl implements PostWriteActivityModel.PostWriteActivityPresenter
{

	private View view;
	private PostWriteActivityModel model;

	public PostWriteActivityPresenterImpl(Activity activity)
	{
		model = new PostWriteActivityModel(activity);
	}

	@Override
	public void setView(View view)
	{
		this.view = view;
	}

	@Override
	public void onBackPressed()
	{
		model.onBackPressed();
	}

	@Override
	public void submit(PostDto2 post)
	{
		model.publish(post);
	}

	@Override
	public void showImagePicker()
	{
		model.showImagePicker();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		view.addImage(model.onActivityResult(requestCode, resultCode, data));
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, android.view.View view, int i, long l)
	{
		model.onItemClick(adapterView, view, i, l);
	}
}
