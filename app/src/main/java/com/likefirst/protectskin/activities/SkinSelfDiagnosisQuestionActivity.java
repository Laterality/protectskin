package com.likefirst.protectskin.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.objects.SkinSelfDiagnosisQuestion;
import com.likefirst.protectskin.objects.SkinType;

/**
 * Created by jinwoo on 2016-07-12.
 */
public class SkinSelfDiagnosisQuestionActivity extends AppCompatActivity implements SkinSelfDiagnosisQuestionActivityModel.SkinSelfDiagnosisActivityPresenter.View, View.OnClickListener
{
	private static final String TAG = "SSDiagAct";

	private SkinSelfDiagnosisQuestionActivityModel.SkinSelfDiagnosisActivityPresenter presenter;

	private TextView tvQuestion;
	private TextView tvYes;
	private TextView tvNo;
	private TextView tvNotKnow;
	private ImageView ivBack;
	private Button btnLater;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.sliding_ltr,R.anim.sliding_rtl);
		setContentView(R.layout.activity_skin_self_diagnosis_question);

		tvQuestion = (TextView) findViewById(R.id.tv_skin_self_diagnosis_question_question);
		tvYes = (TextView) findViewById(R.id.tv_skin_self_diagnosis_question_answer_yes);
		tvNo = (TextView) findViewById(R.id.tv_skin_self_diagnosis_question_answer_no);
		tvNotKnow = (TextView) findViewById(R.id.tv_skin_self_diagnosis_question_answer_not_know);
		ivBack = (ImageView) findViewById(R.id.iv_skin_self_diagnosis_question_go_back);
		btnLater = (Button) findViewById(R.id.btn_skin_self_diagnosis_question_skip);

		Typeface tf = (Typeface.createFromAsset(getAssets(), "NanumBarunGothic-YetHangul.ttf"));
		tvQuestion.setTypeface(tf);
		btnLater.setTypeface(tf);
		tvYes.setTypeface(tf);
		tvNo.setTypeface(tf);
		tvNotKnow.setTypeface(tf);

		presenter = new SkinSelfDiagnosisQuestionActivityPresenterImpl(this);
		presenter.setView(this);

		tvYes.setOnClickListener(this);
		tvNo.setOnClickListener(this);
		tvNotKnow.setOnClickListener(this);
		ivBack.setOnClickListener(this);
		btnLater.setOnClickListener(this);

		presenter.onCreate(savedInstanceState);

	}

	@Override
	public void showQuestion(SkinSelfDiagnosisQuestion q)
	{
		tvQuestion.setText(q.getQuestion());
		if(!q.isYPosiible()){
			tvYes.setVisibility(View.GONE);}else{
			tvYes.setVisibility(View.VISIBLE);}
		if(!q.isNPosiible()){
			tvNo.setVisibility(View.GONE);}else{
			tvNo.setVisibility(View.VISIBLE);}
		if(!q.isUPosiible()){
			tvNotKnow.setVisibility(View.GONE);}else{
			tvNotKnow.setVisibility(View.VISIBLE);}
	}

	@Override
	public void showResult(SkinType t)
	{
		Log.d(TAG, "result : type string\n" + t.getSkinTypeString());
		Intent i = new Intent(this, SkinSelfDiagnosisResultActivity.class);
		if(getIntent().getBooleanExtra("needMain", false)){i.putExtra("needMain", true);}
		i.putExtra(SkinSelfDiagnosisResultActivity.INTENT_KEY_RESULT, t.getSkinType());
		startActivity(i);
		finish();
	}

	@Override
	public void exit()
	{
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}

	@Override
	public void onClick(View view)
	{
		int ans = 0;
		switch (view.getId())
		{
			case R.id.tv_skin_self_diagnosis_question_answer_yes:
				ans = SkinSelfDiagnosisQuestionActivityModel.ANSWER_YES;
				break;
			case R.id.tv_skin_self_diagnosis_question_answer_no:
				ans = SkinSelfDiagnosisQuestionActivityModel.ANSWER_NO;
				break;
			case R.id.tv_skin_self_diagnosis_question_answer_not_know:
				ans = SkinSelfDiagnosisQuestionActivityModel.ANSWER_NOT_KNOW;
				break;
			case R.id.iv_skin_self_diagnosis_question_go_back:
				presenter.doPrev();
				break;
			case R.id.btn_skin_self_diagnosis_question_skip:
				presenter.askLater();
				break;
		}
		if(ans != 0) {presenter.doNext(ans);}
	}

	@Override
	public void onBackPressed()
	{
		presenter.askLater();
	}

}
