package com.likefirst.protectskin.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.UserLoginDto;
import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.results.UserResult;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-23.
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener
{

	private static final String TAG = "SignInAct";
	private static final int RC_SIGN_IN = 1;

	private EditText etEmail;
	private EditText etPassword;
	private Button btnLogin;
	private Button btnSignup;
	private SignInButton btnSignin;

	private GoogleApiClient mGoogleApiClient;
	private UserLoginDto latestLoginDto;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.sliding_ltr, R.anim.sliding_rtl);
		setContentView(R.layout.activity_signin);

		etEmail = (EditText) findViewById(R.id.et_signin_email);
		etPassword = (EditText) findViewById(R.id.et_signin_password);
		btnLogin = (Button) findViewById(R.id.btn_signin_login);
		btnSignup = (Button) findViewById(R.id.btn_signin_signup);
		btnSignin = (SignInButton) findViewById(R.id.btn_signin_sign_in_button);

		Typeface tf = (Typeface.createFromAsset(getAssets(), "NanumGothicExtraBold.ttf"));
		TextView tv = (TextView) findViewById(R.id.tv_login_logo_pz);
		tv.setTypeface(tf);
		List <TextView> tvl = new ArrayList();
		tf = (Typeface.createFromAsset(getAssets(), "NanumBarunGothic-YetHangul.ttf"));
		tvl.add((TextView) findViewById(R.id.tv_login_descriptive_text));
		tvl.add((TextView) findViewById(R.id.tv_login_or));
		tvl.add((TextView) findViewById(R.id.tv_login_email));
		tvl.add((TextView) findViewById(R.id.tv_login_password));
		tvl.add((TextView) findViewById(R.id.tv_login_find_id_or_password));
		for(int i=0;i<tvl.size();i++){
			tvl.get(i).setTypeface(tf);
		}
		etEmail.setTypeface(tf);
		etPassword.setTypeface(tf);
		btnLogin.setTypeface(tf);
		btnSignup.setTypeface(tf);


		btnLogin.setOnClickListener(this);
		btnSignup.setOnClickListener(this);
		btnSignin.setOnClickListener(this);

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestId()
				.requestScopes(new Scope(Scopes.EMAIL),
						new Scope(Scopes.PROFILE))
				.build();

		// Build a GoogleApiClient with access to the Google Sign-In API and the
		// options specified by gso.
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
				.addApi(Auth.GOOGLE_SIGN_IN_API, gso)
				.build();

	}


	@Override
	public void onBackPressed()
	{
		//startActivity(new Intent(this, SignUpActivity.class));
		//finish();
		Utilities.askMeOut(this);
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.btn_signin_login:
				new Login().execute(new UserLoginDto(UserLoginDto.LOGIN_TYPE_NATIVE, etEmail.getText().toString(), etPassword.getText().toString(), null));
				break;
			case R.id.btn_signin_signup:
				register(UserLoginDto.LOGIN_TYPE_NATIVE, null, null);
				break;
			case R.id.btn_signin_sign_in_button:
				signIn();
				break;
		}
	}

	public void register(@NonNull String loginType, @Nullable String email, @Nullable String ext_id)
	{
		Intent i = new Intent(this, SignUpActivity.class);
		i.putExtra(SignUpActivity.INTENT_REGISTER_LOGIN_TYPE, loginType);
		i.putExtra(SignUpActivity.INTENT_REGISTER_EXTERNAL_ID, ext_id);
		i.putExtra(SignUpActivity.INTENT_REGISTER_EXTERNAL_EMAIL, email);
		startActivity(i);
		finish();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult)
	{
		Toast.makeText(SignInActivity.this, "연결 실패", Toast.LENGTH_SHORT).show();
	}

	private void signIn() {
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
		if (requestCode == RC_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			handleSignInResult(result);
		}
	}

	private void handleSignInResult(GoogleSignInResult result)
	{

		Log.d(TAG, "handleSignInResult:" + result.isSuccess());
		if (result.isSuccess())
		{
			// Signed in successfully, show authenticated UI.
			GoogleSignInAccount acct = result.getSignInAccount();

			Log.d(TAG, "Email : " + acct.getEmail());
			Log.d(TAG, "Id : " + acct.getId());
			new Login().execute(latestLoginDto = new UserLoginDto(UserLoginDto.LOGIN_TYPE_GOOGLE, acct.getEmail(), null, acct.getId()));
			//mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
			//updateUI(true);
		}
		else
		{
			// Signed out, show unauthenticated UI.
			//updateUI(false);
		}
	}

	private class Login extends AsyncTask<UserLoginDto, Void, UserResult>
	{
		UserLoginDto loginInfo;
		private ProgressDialog pd;

		@Override
		protected void onPreExecute()
		{
			this.pd = new ProgressDialog(SignInActivity.this);
			pd.setMessage("로그인");
			pd.setCancelable(false);
			pd.show();
		}


		@Override
		protected UserResult doInBackground(UserLoginDto... param)
		{
			loginInfo = param[0];
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.UserService service = retrofit.create(API_Internal.Services.UserService.class);
				Call call = service.loginUser(param[0]);

				Response<UserResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "execution is success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "execution is fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}// doInBackground

		@Override
		protected void onPostExecute(UserResult result)
		{
			pd.dismiss();
			if(result == null)
			{
				Log.d(TAG, "result is null from " + this.getClass().getSimpleName());
				return;
			}

			String strResult = result.getResult();
			UserDto user = result.getUser();


			if(strResult.equals(Result.RESULT_ERROR))
			{
				Toast.makeText(SignInActivity.this, "로그인 실패", Toast.LENGTH_SHORT).show();
				Log.d(TAG, "login error");
				// TODO : handling login fail
			}
			else if(strResult.equals(Result.RESULT_FAIL))
			{
				Toast.makeText(SignInActivity.this, "아이디 혹은 비밀번호가 일치하지 않습니다", Toast.LENGTH_SHORT).show();
				Log.d(TAG, "login fail");
			}
			else if(strResult.equals(Result.RESULT_SUCCESS))
			{
				Log.d(TAG, "login success : " + user.getId());
				((CustomApplication)getApplication()).setUser(user);
				if(((CustomApplication)getApplication()).isFirst())
				{
					startActivity(new Intent(SignInActivity.this, MainActivity.class));
					startActivity(new Intent(SignInActivity.this, SkinSelfDiagnosisStartActivity.class));
				}
				else
				{
					startActivity(new Intent(SignInActivity.this, MainActivity.class));
				}
				finish();
			}
			else if (strResult.equals(Result.RESULT_REGISTER))
			{
				Log.d(TAG, "register new account");
				register(latestLoginDto.getLoginType(), latestLoginDto.getEmail(), latestLoginDto.getExternal_id());
			}
		}
	} // Login
}
