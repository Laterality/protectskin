package com.likefirst.protectskin.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.likefirst.protectskin.objects.SkinSelfDiagnosisQuestion;

/**
 * Created by jinwoo on 2016-07-12.
 */
public class SkinSelfDiagnosisQuestionActivityPresenterImpl implements SkinSelfDiagnosisQuestionActivityModel.SkinSelfDiagnosisActivityPresenter
{
	private static final String TAG = "SSDiagAct";
	private View view;
	private Activity activity;
	private SkinSelfDiagnosisQuestionActivityModel model;

	public SkinSelfDiagnosisQuestionActivityPresenterImpl(Activity activity)
	{
		this.activity = activity;
	}

	@Override
	public void setView(View view)
	{
		this.view = view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		if(activity == null){Log.d(TAG, "context is null");}
		else{model = new SkinSelfDiagnosisQuestionActivityModel(activity);}
		view.showQuestion(model.getNext(SkinSelfDiagnosisQuestionActivityModel.ANSWER_START));
	}


	@Override
	public void doNext(int answer)
	{
		SkinSelfDiagnosisQuestion q = model.getCurrentQuestion();
		if(q.isLast())
		{
			SkinSelfDiagnosisQuestion next = null;
			switch(answer)
			{
				case SkinSelfDiagnosisQuestionActivityModel.ANSWER_YES:
					next = q.getYNext();
					break;
				case SkinSelfDiagnosisQuestionActivityModel.ANSWER_NO:
					next = q.getNNext();
					break;
				case SkinSelfDiagnosisQuestionActivityModel.ANSWER_NOT_KNOW:
					next = q.getUNext();
					break;
			}
			if(next == null){view.showResult(model.getResult(q, answer));}
			else{view.showQuestion(model.getNext(answer));}
		}
		else
		{
			view.showQuestion(model.getNext(answer));
		}
	}

	@Override
	public void doPrev()
	{
		SkinSelfDiagnosisQuestion q = model.getCurrentQuestion();
		if(!q.isFirst())
		{
			view.showQuestion(model.getPrev());
		}
	}

	@Override
	public void askLater()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);

		builder.setTitle("넘어가기");
		builder.setMessage("다음에 하시겠어요?");
		builder.setNegativeButton("아니오", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				dialogInterface.cancel();
			}
		});
		builder.setPositiveButton("예", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				view.exit();
			}
		});
		builder.show();
	}
}
