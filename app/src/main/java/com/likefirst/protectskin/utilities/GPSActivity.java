package com.likefirst.protectskin.utilities;

import android.support.v7.app.AppCompatActivity;

import com.likefirst.protectskin.activities.MainActivityModel;

/**
 * Created by jinwoo on 2016-07-17.
 */
public interface GPSActivity
{

	public void showRequestGPSDialog(MainActivityModel.AsyncCallback callback);
}
