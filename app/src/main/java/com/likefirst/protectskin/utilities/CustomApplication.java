package com.likefirst.protectskin.utilities;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.MonthlyScheduleSet;
import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.UserDto2;
import com.likefirst.protectskin.objects.SettingObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jinwoo on 2016-07-19.
 */
public class CustomApplication extends Application
{
	private static final String TAG = "CustomApp";
	private List<SettingObject> settings;
	private Bundle setting;

	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	private UserDto user;
	private Map<String, MonthlyScheduleSet> schedules;

	private boolean first;

	@Override
	protected void attachBaseContext(Context base)
	{
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	@Override
	public void onCreate()
	{
		super.onCreate();

		settings = new ArrayList<>();
		setting = new Bundle();
		schedules = new HashMap<>();

		prefs = getSharedPreferences(PW486.PREFS_NAME, Context.MODE_PRIVATE);
		editor = prefs.edit();

		loadSetting();// get or make default settings with preference
	}

	/**
	 * Load settings to list with preferences
	 */
	private void loadSetting()
	{
		first = prefs.getBoolean(PW486.PREFS_KEY_IS_FIRST, true);
		Log.d(TAG, "First ? " + first);

		String[] names = getResources().getStringArray(R.array.option_items);
		PackageInfo pi = null;
		try{pi = getPackageManager().getPackageInfo(getPackageName(), 0);}catch (Exception e){e.printStackTrace();}
		settings.add(new SettingObject(names[0], pi.versionName));
		settings.add(new SettingObject(names[1]));
		settings.add(new SettingObject(names[2], prefs.getBoolean(PW486.PREFS_KEY_SETTING_PUSH, true))); settings.get(2).setKey(PW486.PREFS_KEY_SETTING_PUSH);
		settings.add(new SettingObject(names[3]));
		settings.add(new SettingObject(names[4]));
		settings.add(new SettingObject(names[5]));

		setting.putString(names[0], pi.versionName);
		setting.putBoolean(names[2], prefs.getBoolean(PW486.PREFS_KEY_SETTING_PUSH, true));

		if(first)
		{
			// when user execute app at first time
			editor.putBoolean(PW486.PREFS_KEY_SETTING_PUSH, settings.get(2).getBoolValue());
			editor.commit();
		}
		else
		{
			if(prefs.getBoolean(PW486.PREFS_KEY_ACC_LOGGED, false))
			{
				user = new UserDto(prefs.getString(PW486.PREFS_KEY_ACC_USER_ID, null));
				Log.d(TAG, "pref id : " + user.getId());
			}
		}
	}

	public void changeSetting(SettingObject setting)
	{
		String key = null;


		switch (setting.getSettingType())
		{
			case SettingObject.TYPE_VAL_INT:
				editor.putInt(key, setting.getIntValue());
				break;
			case SettingObject.TYPE_VAL_BOOL:
				editor.putBoolean(key, setting.getBoolValue());
				break;
			case SettingObject.TYPE_VAL_STRING:
				editor.putString(key, setting.getStringValue());
				break;
		}
		editor.commit();
	}

	public boolean isFirst(){return first;}

	public boolean isLogged(){return prefs.getBoolean(PW486.PREFS_KEY_ACC_LOGGED, false);}

	public List<SettingObject> getSettings() {return settings;}

	public void logout()
	{
		editor.putBoolean(PW486.PREFS_KEY_ACC_LOGGED, false);
		editor.commit();
	}

	public void firstSee()
	{
		editor.putBoolean(PW486.PREFS_KEY_IS_FIRST, false);
		editor.commit();
	}

	//
	// BEGIN : getter/setter
	//
	public Bundle getSetting(){return setting;}
	public UserDto getUser() { return user;}
	public MonthlyScheduleSet getSchedule(int year, int month)
	{
		String key = year + "/" + month;
		Log.d(TAG, "get schedule " + year + "/" + month);
		return schedules.get(key);
	}

	public void setUser(UserDto user)
	{
		this.user = user;
		editor.putString(PW486.PREFS_KEY_ACC_USER_ID, user.getId());
		editor.putBoolean(PW486.PREFS_KEY_ACC_LOGGED, true);
		Log.d(TAG,"set user : " + user.getId());
		editor.commit();
	}
	public void setUser(UserDto2 user)
	{
		this.user = UserDto.cloneFrom2(user);

		editor.putString(PW486.PREFS_KEY_ACC_USER_ID, user.getId());
		editor.putBoolean(PW486.PREFS_KEY_ACC_LOGGED, true);
		Log.d(TAG,"set user : " + user.getId());
		editor.commit();
	}

	public void setSchedule(MonthlyScheduleSet scheduleSet)
	{
		String key = scheduleSet.getDate().getYear() + "/" + (scheduleSet.getDate().getMonth() + 1);
		Log.d(TAG, "set schedule " + scheduleSet.getDate().getYear() + "/" + (scheduleSet.getDate().getMonth() + 1));
		schedules.put(key, scheduleSet);
	}
	//
	// END : getter/setter
	//
}
