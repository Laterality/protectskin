package com.likefirst.protectskin.utilities;

/**
 * Created by jinwoo on 2016-07-12.
 */
public class PW486
{

	// preference kyes
	public static final String PREFS_NAME = "PMS_PREFS";
	public static final String PREFS_KEY_SELF_DIAGNOSED = "diagnosed";
	public static final String PREFS_KEY_INITIALIZED_DB = "DBinitialized";
	public static final String PREFS_KEY_IS_FIRST = "isFirst";

	// account
	public static final String PREFS_KEY_ACC_LOGGED = "logged";
	public static final String PREFS_KEY_ACC_USER_ID = "user_Id";
	public static final String PREFS_KEY_ACC_LOGIN_TYPE = "login_type";
	public static final String PREFS_KEY_ACC_EMAIL = "email";
	public static final String PREFS_KEY_ACC_PASSWORD = "password";
	public static final String PREFS_KEY_ACC_USERNAME = "username";
	public static final String PREFS_KEY_ACC_GENDER = "gender";
	public static final String PREFS_KEY_ACC_EXTERNAL_ID = "external_id";

	// setting
	public static final String PREFS_KEY_SETTING_VERSION = "vsersion";
	public static final String PREFS_KEY_SETTING_LOGIN = "login";
	public static final String PREFS_KEY_SETTING_PUSH = "push";
	public static final String PREFS_KEY_SETTING_NOTICE = "notice";
	public static final String PREFS_KEY_SETTING_AGREE_FOR_PGI = "pgi";
	public static final String PREFS_KEY_SETTING_QUIT = "quit";


	// intent filters
	public static final String INTENT_FILTER_ACTION_SERVICE_SCHEDULE_MANAGER = "com.likefirst.protectskin.utilities.ScheduleManagerService";


	// oid
	public static final String BOARD_KNOWHOW = "57a3352919cdb4164ce0431e";
	public static final String BOARD_REVIEW = "57a3353219cdb4164ce0431f";
	public static final String BOARD_QNA = "57a3353819cdb4164ce04320";

	public static final String SKINTYPE_DRY = "57a323493ae16c744938a845";
	public static final String SKINTYPE_GENERAL = "57a3245a3ae16c744938a846";
	public static final String SKINTYPE_OILY = "57a325203ae16c744938a847";
	public static final String SKINTYPE_COMPLEX = "57a325253ae16c744938a848";
	public static final String SKINTYPE_SENSITIVE = "57a3252d3ae16c744938a849";
}
