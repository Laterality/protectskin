package com.likefirst.protectskin.utilities;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class ScheduleManagerService extends Service
{
	private static final String TAG = "ScheduleManager";

	@Override
	public void onCreate()
	{
		super.onCreate();

		BroadcastReceiver receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				if(intent.getAction().equals(Intent.ACTION_DATE_CHANGED))
				{
					Log.d(TAG, "DATE CHANGED");
				}
			}
		};
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		super.onStartCommand(intent, flags, startId);


		return START_STICKY;
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent arg0)
	{
		return null;
	}
}
