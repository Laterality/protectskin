package com.likefirst.protectskin.utilities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by jinwoo on 2016-07-14.
 */
public class LocalDBManager
{

	private final String TAG = "LocalDB";
	static final String DB_NAME = "schedules.db";
	static final int DB_VERSION = 1;
	private Context mContext = null;

	private static LocalDBManager DBManager = null;
	private static SQLiteDatabase mDatabase = null;


	public static LocalDBManager getInstance(Context context)
	{
		if(DBManager == null) {DBManager = new LocalDBManager(context);}

		return DBManager;
	}

	private LocalDBManager(Context context)
	{
		mContext = context;
		mDatabase = context.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);
	}

	public void initDB()
	{
		String query = "CREATE TABLE scheduleSet (set_id INT AUTO_INCREMENT, set_date DATE, PRIMARY KEY(set_id));\n" +
				"CREATE TABLE schedule (sch_id INT AUTO_INCREMENT, sch_msg VARCHAR(255), sch_fulfilled BOOLEAN, set_id INT, PRIMARY KEY(sch_id), FOREIGN KEY(set_id) REFERENCES scheduleSet(set_id));";
		mDatabase.execSQL(query);
	}

}
