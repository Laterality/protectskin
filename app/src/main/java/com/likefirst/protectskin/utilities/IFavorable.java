package com.likefirst.protectskin.utilities;

/**
 * Created by jinwoo on 2016-07-17.
 */
public interface IFavorable
{

	public void favor();
}
