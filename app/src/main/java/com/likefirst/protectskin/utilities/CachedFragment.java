package com.likefirst.protectskin.utilities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by jinwoo on 2016-07-16.
 *
 * caching fragment by keep view on static memory
 */
public abstract class CachedFragment extends Fragment
{

	private static View mView;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView = initOnCreateView(inflater, container, savedInstanceState);
		}

		return mView;
	}

	/**
	 * initialized fragment
	 * this method will be called just once in an application lifecycle
	 * codes some need every #onCreateView should be written in derived class's #onCreateView
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return inflated view
	 */
	protected abstract @NonNull View initOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
}
