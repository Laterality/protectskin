package com.likefirst.protectskin.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by jinwoo on 2016-07-01.
 */
public class GPSManager implements LocationListener
{
    private static final String TAG = "GPSManager";
    private static GPSManager _this = null;
    private static Context mContext;

    // GPS info
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;

    // Location
    private Location location;
    private double lat;
    private double lon;

    // Update info
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATE = 100; // meter unit
    private static final long MIN_TIME_GPS_UPDATE = 1000 * 60 * 60; // update location least an hour

    private LocationManager locationManager;

    public static GPSManager getInstance(Context context)
    {
        mContext = context;
        if(_this == null)
        {
            _this = new GPSManager(context);
        }
        return _this;
    }


    private GPSManager(Context context)
    {
        mContext = context;
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
    }

    public void getLocation(LocationUpdateListener listener)
    {
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(isNetworkEnabled)
        {
            try
            {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_GPS_UPDATE, MIN_DISTANCE_CHANGE_FOR_UPDATE, this);

                if(locationManager != null)
                {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if(location != null)
                    {
                        lat = location.getLatitude();
                        lon = location.getLongitude();
                    }
                }
            }
            catch (SecurityException e)
            {
                Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
                e.printStackTrace();
            }
        }

        if(isGPSEnabled)
        {

            try
            {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_GPS_UPDATE, MIN_DISTANCE_CHANGE_FOR_UPDATE, this);

                if(locationManager != null)
                {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    if(location != null)
                    {
                        lat = location.getLatitude();
                        lon = location.getLongitude();
                    }
                }
            }
            catch (SecurityException e)
            {
                Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
                e.printStackTrace();
            }
        }
        if(listener != null)
        {
            if(!(lat == 0 || lon == 0)){listener.onUpdate(lat, lon);}
        }
        Log.d(TAG, "location\nlatitude : " + lat +"\nlongitude : " + lon);
    }



    public void showSettingDialog(Context context, final DialogInterface.OnClickListener listener)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        dialogBuilder.setTitle("GPS 활성화");
        dialogBuilder.setMessage("위치정보를 받기 위한 GPS가 활성화되지 않았습니다.\nGPS를 활성화 하시겠습니까?");
        dialogBuilder.setPositiveButton("설정", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                if(listener != null)
                {
                    listener.onClick(dialogInterface, i);
                }

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);


            }
        });

        dialogBuilder.setNegativeButton("취소", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.cancel();

                if(listener != null)
                {
                    listener.onClick(dialogInterface, i);
                }
            }
        });

        dialogBuilder.show();
    }

    @Override
    public void onLocationChanged(Location location)
    {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle)
    {

    }

    @Override
    public void onProviderEnabled(String s)
    {

    }

    @Override
    public void onProviderDisabled(String s)
    {

    }

    public boolean isGPSEnabled(){return isGPSEnabled;}

    public double getLatitude()
    {
        return lat;
    }

    public double getLon()
    {
        return lon;
    }

	public interface LocationUpdateListener
    {
        public void onUpdate(double lat, double lon);
    }

}
