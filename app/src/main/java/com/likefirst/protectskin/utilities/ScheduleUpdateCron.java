package com.likefirst.protectskin.utilities;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

/**
 * Created by jinwoo on 2016-08-16.
 */
public class ScheduleUpdateCron extends BroadcastReceiver
{


	@Override
	public void onReceive(Context context, Intent intent)
	{
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
		wl.acquire();

		// code

		wl.release();
	}
}
