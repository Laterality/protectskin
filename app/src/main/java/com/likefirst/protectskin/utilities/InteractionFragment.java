package com.likefirst.protectskin.utilities;

import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by jinwoo on 2016-07-15.
 */
public abstract class InteractionFragment extends Fragment
{
	private static final String TAG = "InteractionFragment";
	private IFragmentRequest request;

	abstract public Fragment getChild(int position);

	abstract public boolean onBackPressed();

	public void setRequestListener(IFragmentRequest request)
	{
		this.request = request;
	}

	/**
	 *
	 * @param fragment fragment to request to show
	 * @return is request arrived to activity
	 */
	protected boolean requestShowFragment(Fragment fragment)
	{
		if(request != null && fragment != null) {request.showFragment(fragment); return true;}
		else
		{
			Log.d(TAG, "request " + (request == null ? "null" : "ok") + ", fragment " + (fragment == null ? "null" : "ok"));
			return false;
		}
	}


	public interface IFragmentRequest
	{
		public void showFragment(Fragment fragment);
	}
}
