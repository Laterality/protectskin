package com.likefirst.protectskin.utilities;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.likefirst.protectskin.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class Utilities
{
	private static final String TAG = "utilities";

	/**
	 *
	 * @param date
	 * @return return string with one character of day of week
	 */
	public static String getDayOfWeekString(DateTime date)
	{
		String str;
		switch (date.getDayOfWeek())
		{
			case DateTimeConstants.MONDAY:
				str = "월";
				break;
			case DateTimeConstants.TUESDAY:
				str = "화";
				break;
			case DateTimeConstants.WEDNESDAY:
				str = "수";
				break;
			case DateTimeConstants.THURSDAY:
				str = "목";
				break;
			case DateTimeConstants.FRIDAY:
				str = "금";
				break;
			case DateTimeConstants.SATURDAY:
				str = "토";
				break;
			case DateTimeConstants.SUNDAY:
				str = "일";
				break;
			default:
				str = "";
				break;
		}

		return str;
	}

	public static String getCityStringMini(String city)
	{
		String str;
		if(city.equals("충청남도")){str = "충남";}
		else if(city.equals("충청북도")){str = "충북";}
		else if(city.equals("전라남도")){str = "전남";}
		else if(city.equals("전라북도")){str = "전북";}
		else if(city.equals("경상남도")){str = "경남";}
		else if(city.equals("경상북도")){str = "경북";}
		else if(city.equals("강원도")){str = "강원";}
		else if(city.equals("서울특별시")){str = "서울";}
		else if(city.equals("인천광역시")){str = "인천";}
		else if(city.equals("광주광역시")){str = "광주";}
		else if(city.equals("울산광역시")){str = "울산";}
		else if(city.equals("대구광역시")){str = "대구";}
		else if(city.equals("부산광역시")){str = "부산";}
		else if(city.equals("세종특별자치시")){str = "세종";}
		else if(city.equals("제주특별자치시")){str = "제주";}
		else{str = "";}

		return str;
	}

	public static int setListViewHeightBasedOnChildren(ListView lv)
	{
		ListAdapter listAdapter = lv.getAdapter();
		if (listAdapter == null)
			return 0;

		int desiredWidth = View.MeasureSpec.makeMeasureSpec(lv.getWidth(), View.MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, lv);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

			view.measure(0, 0);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = lv.getLayoutParams();
		params.height = totalHeight + (lv.getDividerHeight() * (listAdapter.getCount() - 1));
		lv.setLayoutParams(params);
		lv.requestLayout();

		Log.d(TAG , "Height : " + params.height);

		return params.height;
	}

	public static void askMeOut(Context context)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("종료");
		builder.setNegativeButton("취소", null);
		builder.setMessage("정말 종료하시겠습니까?");
		builder.setPositiveButton("확인", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				System.exit(0);
			}
		});
		builder.show();
	}

	public static void showToast(Context context, String msg, int duration)
	{
		LayoutInflater inflater = LayoutInflater.from(context);

		View v = inflater.inflate(R.layout.view_toast, null);

		TextView tvMsg = (TextView) v.findViewById(R.id.tv_toast_msg);
		tvMsg.setTypeface(Typeface.createFromAsset(context.getAssets(), "NanumSquareB.ttf"));

		tvMsg.setText(msg);

		Toast toast = new Toast(context.getApplicationContext());

		//toast.setGravity(Gravity.BOTTOM, 0, 0);

		toast.setDuration(duration);

		toast.setView(v);

		toast.show();
	}

	public static void setEditable(EditText et, boolean editable)
	{
		et.setFocusable(editable);
		et.setFocusableInTouchMode(editable);
		et.setClickable(editable);
	}

	public static String getPathFromUri(Context context, Uri uri){
		Cursor cursor = context.getContentResolver().query(uri, null, null, null, null );
		cursor.moveToNext();
		String path = cursor.getString( cursor.getColumnIndex( "_data" ) );
		cursor.close();


		return path;
	}

	public static int dpToPx(Context context, float dp)
	{
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
	}

	public static class RealPathUtils
	{
		/**
		 * Get a file path from a Uri. This will get the the path for Storage Access
		 * Framework Documents, as well as the _data field for the MediaStore and
		 * other file-based ContentProviders.
		 *
		 * @param context The context.
		 * @param uri The Uri to query.
		 * @author paulburke
		 */
		public static String getPath(final Context context, final Uri uri) {

			final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

			// DocumentProvider
			if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
				// ExternalStorageProvider
				if (isExternalStorageDocument(uri)) {
					final String docId = DocumentsContract.getDocumentId(uri);
					final String[] split = docId.split(":");
					final String type = split[0];

					if ("primary".equalsIgnoreCase(type)) {
						return Environment.getExternalStorageDirectory() + "/" + split[1];
					}

					// TODO handle non-primary volumes
				}
				// DownloadsProvider
				else if (isDownloadsDocument(uri)) {

					final String id = DocumentsContract.getDocumentId(uri);
					final Uri contentUri = ContentUris.withAppendedId(
							Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

					return getDataColumn(context, contentUri, null, null);
				}
				// MediaProvider
				else if (isMediaDocument(uri)) {
					final String docId = DocumentsContract.getDocumentId(uri);
					final String[] split = docId.split(":");
					final String type = split[0];

					Uri contentUri = null;
					if ("image".equals(type)) {
						contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
					} else if ("video".equals(type)) {
						contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
					} else if ("audio".equals(type)) {
						contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
					}

					final String selection = "_id=?";
					final String[] selectionArgs = new String[] {
							split[1]
					};

					return getDataColumn(context, contentUri, selection, selectionArgs);
				}
			}
			// MediaStore (and general)
			else if ("content".equalsIgnoreCase(uri.getScheme())) {

				// Return the remote address
				if (isGooglePhotosUri(uri))
					return uri.getLastPathSegment();

				return getDataColumn(context, uri, null, null);
			}
			// File
			else if ("file".equalsIgnoreCase(uri.getScheme())) {
				return uri.getPath();
			}

			return null;
		}

		/**
		 * Get the value of the data column for this Uri. This is useful for
		 * MediaStore Uris, and other file-based ContentProviders.
		 *
		 * @param context The context.
		 * @param uri The Uri to query.
		 * @param selection (Optional) Filter used in the query.
		 * @param selectionArgs (Optional) Selection arguments used in the query.
		 * @return The value of the _data column, which is typically a file path.
		 */
		public static String getDataColumn(Context context, Uri uri, String selection,
		                                   String[] selectionArgs) {

			Cursor cursor = null;
			final String column = "_data";
			final String[] projection = {
					column
			};

			try {
				cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
						null);
				if (cursor != null && cursor.moveToFirst()) {
					final int index = cursor.getColumnIndexOrThrow(column);
					return cursor.getString(index);
				}
			} finally {
				if (cursor != null)
					cursor.close();
			}
			return null;
		}


		/**
		 * @param uri The Uri to check.
		 * @return Whether the Uri authority is ExternalStorageProvider.
		 */
		public static boolean isExternalStorageDocument(Uri uri) {
			return "com.android.externalstorage.documents".equals(uri.getAuthority());
		}

		/**
		 * @param uri The Uri to check.
		 * @return Whether the Uri authority is DownloadsProvider.
		 */
		public static boolean isDownloadsDocument(Uri uri) {
			return "com.android.providers.downloads.documents".equals(uri.getAuthority());
		}

		/**
		 * @param uri The Uri to check.
		 * @return Whether the Uri authority is MediaProvider.
		 */
		public static boolean isMediaDocument(Uri uri) {
			return "com.android.providers.media.documents".equals(uri.getAuthority());
		}

		/**
		 * @param uri The Uri to check.
		 * @return Whether the Uri authority is Google Photos.
		 */
		public static boolean isGooglePhotosUri(Uri uri) {
			return "com.google.android.apps.photos.content".equals(uri.getAuthority());
		}
	}
}
