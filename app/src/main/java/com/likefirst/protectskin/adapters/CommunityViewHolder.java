package com.likefirst.protectskin.adapters;

import android.widget.TextView;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class CommunityViewHolder
{
	public TextView tvPostName;
	public TextView tvPostContent;
	public TextView tvRegDate;
	public TextView tvAuthor;
	public TextView tvReccText;
	public TextView tvReccCnt;

}
