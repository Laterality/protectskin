package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.objects.ScheduleDetailListItem;

import java.util.List;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class SchedulerDetailListAdapter extends BaseAdapter
{

	private Context mContext;
	private List<ScheduleDetailListItem> list;

	private SchedulerDetailListAdapter(Context context, List<ScheduleDetailListItem> list)
	{
		mContext = context;
		this.list = list;
	}

	@Override
	public int getCount()
	{
		return list.size();
	}

	@Override
	public Object getItem(int i)
	{
		return list.get(i);
	}

	@Override
	public long getItemId(int i)
	{
		// TODO : need modify
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup)
	{
		ScheduleDetailListItem item = list.get(i);
		ViewHolder holder;
		if(view == null)
		{
			holder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(R.layout.view_calendar_detail_list, null);

			holder.tvMsg = (TextView) view.findViewById(R.id.tv_calendar_detail_content);
			holder.ivFulfilled = (ImageView) view.findViewById(R.id.iv_calendar_detail_check);

			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)view.getTag();
		}

		holder.tvMsg.setText(item.getMsg());
		holder.ivFulfilled.setImageDrawable(
				item.isFulfilled() ?
						mContext.getResources().getDrawable(R.drawable.check_o_icon_196_gray) // 변경해야 하는 부분
				: mContext.getResources().getDrawable(R.drawable.check_o_icon_196_gray)
		);

		return null;
	}

	private class ViewHolder
	{
		TextView tvMsg;
		ImageView ivFulfilled;
	}
}
