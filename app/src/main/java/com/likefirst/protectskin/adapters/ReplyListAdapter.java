package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.ReplyDto;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by jinwoo on 2016-08-11.
 */
public class ReplyListAdapter extends BaseAdapter
{
	private static final String TAG = "ReplyListAdapter";
	private Context mContext;
	private List<ReplyDto> list;

	public ReplyListAdapter(Context context, List<ReplyDto> replies)
	{
		list = new ArrayList<>();

		if(replies != null){list.addAll(replies);}
		mContext = context;
	}

	public void setList(List<ReplyDto> replies)
	{
		list.clear();
		list.addAll(replies);
		notifyDataSetChanged();
	}

	public void addList(List<ReplyDto> replies)
	{
		list.addAll(replies);
		notifyDataSetChanged();
	}


	@Override
	public int getCount()
	{
		return list.size();
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public ReplyDto getItem(int position)
	{
		return list.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder;
		ReplyDto item = list.get(position);

		if(convertView == null)
		{
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_reply, parent, false);

			holder = new ViewHolder();

			holder.tvAuthor = (TextView) convertView.findViewById(R.id.tv_reply_author);
			holder.tvRegDate = (TextView) convertView.findViewById(R.id.tv_reply_regdate);
			holder.tvContent = (TextView) convertView.findViewById(R.id.tv_reply_content);
			holder.ivProfile = (ImageView) convertView.findViewById(R.id.iv_reply_profile_img);

			Typeface tf = (Typeface.createFromAsset(mContext.getAssets(), "NanumSquareB.ttf"));
			holder.tvAuthor.setTypeface(tf);
			holder.tvRegDate.setTypeface(tf);
			holder.tvContent.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "NanumBarunGothic-YetHangul.ttf"));

			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		DateTime d = new DateTime(item.getRegDate());

		holder.tvAuthor.setText(item.getAuthor().getUsername());
		holder.tvRegDate.setText(String.format(Locale.KOREA, "%d.%d.%02d", d.getYear(), d.getMonthOfYear(), d.getDayOfMonth()));
		holder.tvContent.setText(item.getContent());



		return convertView;
	}

	private class ViewHolder
	{
		TextView tvAuthor;
		TextView tvRegDate;
		TextView tvContent;
		ImageView ivProfile;
	}
}
