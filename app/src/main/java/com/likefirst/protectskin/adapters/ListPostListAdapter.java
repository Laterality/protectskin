package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.PostDto;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by jinwoo on 2016-08-10.
 */
public class ListPostListAdapter extends BaseAdapter
{
	private Context mContext;
	private List<PostDto> list;

	public ListPostListAdapter(Context context, List<PostDto> posts)
	{
		list = new ArrayList<>();
		mContext = context;
		if(posts != null){list.addAll(posts);}
	}

	public void setList(List<PostDto> posts)
	{
		list.clear();
		list.addAll(posts);
		notifyDataSetChanged();
	}

	public void addList(List<PostDto> posts)
	{
		list.addAll(posts);
		notifyDataSetChanged();
	}

	public long getLastPostRegDate()
	{
		return list.get(list.size() - 1).getRegDate();
	}



	@Override
	public int getCount()
	{
		return list.size();
	}

	@Override
	public PostDto getItem(int i)
	{
		return list.get(i);
	}

	@Override
	public long getItemId(int i)
	{
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup)
	{
		ViewHolder holder;
		PostDto post = list.get(i);

		if(view == null)
		{
			holder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(R.layout.item_post_list, null);

			holder.tvTitle = (TextView) view.findViewById(R.id.tv_item_post_list_title);
			holder.tvContent = (TextView) view.findViewById(R.id.tv_item_post_list_content);
			holder.tvAuthor = (TextView) view.findViewById(R.id.tv_item_post_list_author);
			holder.tvDate = (TextView) view.findViewById(R.id.tv_item_post_list_regdate);
			holder.tvRecommend = (TextView) view.findViewById(R.id.tv_item_post_list_recommends);

			Typeface tf = (Typeface.createFromAsset(mContext.getAssets(), "NanumSquareB.ttf"));
			List<TextView> tvl = new ArrayList();
			tvl.add(holder.tvTitle);
			tvl.add(holder.tvContent);
			tvl.add(holder.tvAuthor);
			tvl.add(holder.tvDate);
			tvl.add(holder.tvRecommend);
			for(int j=0;j<tvl.size();j++){
				tvl.get(j).setTypeface(tf);
			}

			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}

		List<String> text = post.getTextContent();

		holder.tvContent.setText(text.size() == 0 ? "텍스트 없음" : text.get(0));
		for(int j = 1; j < text.size(); j++)
		{
			holder.tvContent.append("\n" + text.get(j));
			if(holder.tvContent.getText().length() > 85)
			{
				holder.tvContent.setText(holder.tvContent.getText().toString().substring(0, 85) + "...");
				break;
			}
		}

		holder.tvAuthor.setText(post.getAuthor().getUsername());
		holder.tvTitle.setText(post.getTitle());
		DateTime d = new DateTime(Long.valueOf(post.getRegDate()));
		holder.tvDate.setText(String.format(Locale.KOREA, "%d.%d.%02d", d.getYear(), d.getMonthOfYear(), d.getDayOfMonth()));
		holder.tvRecommend.setText("추천수 : " + String.valueOf(post.getCountRecommend()));





		return view;
	}

	private class ViewHolder
	{
		TextView tvTitle;
		TextView tvContent;
		TextView tvDate;
		TextView tvAuthor;
		TextView tvRecommend;
	}
}
