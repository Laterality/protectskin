package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.likefirst.protectskin.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jinwoo on 2016-08-07.
 */
public class PostCategorySpinnerAdapter extends BaseAdapter
{

	private Context mContext;
	private List<String> list;

	public PostCategorySpinnerAdapter(Context context)
	{
		mContext = context;
		list = Arrays.asList(context.getResources().getStringArray(R.array.post_categories));
	}

	public PostCategorySpinnerAdapter(Context context, boolean useEntire)
	{
		mContext = context;
		if(!useEntire){list = Arrays.asList(context.getResources().getStringArray(R.array.post_categories_ne));}
		else{list = Arrays.asList(context.getResources().getStringArray(R.array.post_categories));}
	}

	@Override
	public int getCount()
	{
		return list.size();
	}

	@Override
	public String getItem(int position)
	{
		return list.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder;

		if(convertView == null)
		{
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_spinner_category, null);

			Typeface tf = (Typeface.createFromAsset(mContext.getAssets(), "NanumBarunGothic-YetHangul.ttf")) ;
			holder.tvString = (TextView) convertView.findViewById(R.id.tv_item_post_category_string);
			holder.tvString.setTypeface(tf);
			TextView tv = (TextView) convertView.findViewById(R.id.tv_item_post_category);
			tv.setTypeface(tf);

			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvString.setText(list.get(position));


		return convertView;
	}


	private class ViewHolder
	{
		TextView tvString;
	}
}
