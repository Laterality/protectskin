package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.objects.SettingObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-19.
 */
public class SettingListAdapter extends BaseAdapter
{
	private List<SettingObject> mList;
	private Context mContext;

	public SettingListAdapter(Context context, @NonNull List<SettingObject> settings)
	{
		mContext = context;
		mList = new ArrayList<>();
		mList.addAll(settings);
	}


	@Override
	public int getCount()
	{
		return mList.size();
	}

	@Override
	public Object getItem(int i)
	{
		return mList.get(i);
	}

	@Override
	public long getItemId(int i)
	{
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup)
	{
		SettingObject item = mList.get(i);
		ViewHolder holder;

		if(view == null)
		{
			holder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(R.layout.view_setting_list, null);

			holder.tvName = (TextView) view.findViewById(R.id.tv_item_list_setting_name);
			holder.tvOn = (TextView) view.findViewById(R.id.tv_item_list_setting_on);
			holder.tvOff = (TextView) view.findViewById(R.id.tv_item_list_setting_off);
			holder.tvDivider = (TextView) view.findViewById(R.id.tv_item_list_setting_on_off_divider);

			List<TextView> tvl = new ArrayList();
			tvl.add(holder.tvName);
			tvl.add(holder.tvOn);
			tvl.add(holder.tvOff);
			tvl.add(holder.tvDivider);
			for (int j = 0; j < tvl.size(); j++)
			{
				tvl.get(j).setTypeface(Typeface.createFromAsset(mContext.getAssets(), "NanumSquareB.ttf"));
			}

			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}

		holder.setting = item;

		int sType = item.getSettingType();
		holder.tvName.setText(item.getSettingName());

		holder.tvOff.setVisibility(View.GONE);
		holder.tvOn.setVisibility(View.GONE);
		holder.tvDivider.setVisibility(View.GONE);

		if(sType == SettingObject.TYPE_VAL_BOOL)
		{
			boolean state = item.getBoolValue();

			holder.tvOff.setVisibility(View.VISIBLE);
			holder.tvOn.setVisibility(View.VISIBLE);
			holder.tvDivider.setVisibility(View.VISIBLE);

			if(state)
			{
				holder.tvOn.setTextColor(mContext.getResources().getColor(R.color.PrimaryBlue));
				holder.tvOff.setTextColor(Color.parseColor("#000000"));
			}
			else
			{

				holder.tvOff.setTextColor(mContext.getResources().getColor(R.color.PrimaryBlue));
				holder.tvOn.setTextColor(Color.parseColor("#000000"));
			}
		}
		else if(sType == SettingObject.TYPE_VAL_INT)
		{
			// show value type setting item
		}
		else if(sType == SettingObject.TYPE_VAL_STRING)
		{
			// show value type setting item
		}
		else if(sType == SettingObject.TYPE_NONE)
		{

		}
		else
		{
			// Exception handling
		}


		return view;
	}

	public class ViewHolder
	{
		TextView tvName;
		TextView tvOn;
		TextView tvOff;
		TextView tvDivider;
		SettingObject setting;

		public SettingObject getSetting(){return setting;}
	}
}
