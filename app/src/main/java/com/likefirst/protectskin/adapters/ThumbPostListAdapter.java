package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.PostDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-08-06.
 */
public class ThumbPostListAdapter extends BaseAdapter
{
	private Context mContext;
	private List<PostDto> posts;

	public ThumbPostListAdapter(Context context, List<PostDto> list)
	{
		mContext = context;
		posts = new ArrayList<>();
		if(list != null) posts.addAll(list);
	}

	public void setList(List<PostDto> posts)
	{
		this.posts.clear();
		this.posts.addAll(posts);
		notifyDataSetChanged();
	}

	public void addList(List<PostDto> posts)
	{
		this.posts.addAll(posts);
		notifyDataSetChanged();
	}

	public void clear()
	{
		posts.clear();
	}

	@Override
	public int getCount()
	{
		return posts.size();
	}

	@Override
	public PostDto getItem(int position)
	{
		return posts.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if(mContext == null)
		{
			throw new NullPointerException("Context is null");
		}

		ViewHolder holder;
		PostDto post = posts.get(position);

		if(convertView == null)
		{
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_posts, null);

			holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_posts_title);
			holder.tvTitle.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "NanumBarunGothic.ttf"));

			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}

		holder.tvTitle.setText(post.getTitle());

		return convertView;
	}

	private class ViewHolder
	{
		TextView tvTitle;
	}
}
