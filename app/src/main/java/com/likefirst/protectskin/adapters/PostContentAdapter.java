package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.PostElement;
import com.likefirst.protectskin.utilities.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-08-14.
 */
public class PostContentAdapter extends BaseAdapter
{
	private static final String TAG = "PostContentAdapter";
	private Context mContext;
	private List<PostElement> list;

	public PostContentAdapter(Context context, List<PostElement> elements)
	{
		list = new ArrayList();
		mContext = context;
		if (elements != null)
		{
			list.addAll(elements);
		}
	}

	public void setContent(List<PostElement> elements)
	{
		list.addAll(elements);
		notifyDataSetChanged();
	}

	public void addContents(List<PostElement> elements)
	{
		list.addAll(elements);
		notifyDataSetChanged();
	}

	public void addContent(PostElement element)
	{
		list.add(element);
		notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		return list.size();
	}

	@Override
	public PostElement getItem(int position)
	{
		return list.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final PostElement e = list.get(position);
		ViewHolder holder;

		Log.d(TAG, "content type : " + e.getType() + ", content : " + e.getContent());

		if (convertView == null)
		{
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_post_content, null);

			holder.etText = (EditText) convertView.findViewById(R.id.tv_post_content_text);
			holder.ivImage = (ImageView) convertView.findViewById(R.id.iv_post_content_image);

			Utilities.setEditable(holder.etText, false);

			holder.etText.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{
					e.setContent(charSequence.toString());
				}

				@Override
				public void afterTextChanged(Editable editable)
				{

				}
			});
			holder.etText.setOnFocusChangeListener(new View.OnFocusChangeListener()
			{
				@Override
				public void onFocusChange(View view, boolean b)
				{
					EditText etText = (EditText)view;
					if(b)
					{
						((EditText) view).setSelection(((EditText) view).getText().length());
						InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(etText, InputMethodManager.SHOW_IMPLICIT);
					}
					if (!b)
					{
						// make edit text non-focusable when it loses focus
						etText.setSelection(etText.getText().length());
						Utilities.setEditable((EditText) view, false);
					}
				}
			});

			convertView.setTag(holder);
		} else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		if (e.getType().equals(PostElement.TYPE_TEXT))
		{
			holder.etText.setVisibility(View.VISIBLE);
			holder.ivImage.setVisibility(View.GONE);
			holder.etText.setText(e.getContent());
		} else if (e.getType().equals(PostElement.TYPE_IMAGE))
		{
			holder.etText.setVisibility(View.GONE);
			holder.ivImage.setVisibility(View.VISIBLE);
			Picasso.with(mContext).load(API_Internal.BASE_URL + e.getContent()).into(holder.ivImage);
		} else if (e.getType().equals(PostElement.TYPE_IMAGE_LOCAL))
		{
			holder.etText.setVisibility(View.GONE);
			holder.ivImage.setVisibility(View.VISIBLE);
			holder.ivImage.setImageBitmap(e.getLocalImage());
		}


		return convertView;
	}

	public void swapTo(int idx1, int idx2)
	{
		int from = idx1 > idx2 ? idx2 : idx1;
		int to = idx1 > idx2 ? idx1 : idx2;
		PostElement t;

		for (int i = from; i < to; i++)
		{
			swapWithoutNotifying(i, i + 1);
		}
		notifyDataSetChanged();
	}

	private void swapWithoutNotifying(int idx1, int idx2)
	{
		PostElement t = list.get(idx1);
		list.set(idx1, list.get(idx2));
		list.set(idx2, t);
		PostElement.swapIndex(list.get(idx1), list.get(idx2));
	}

	public List<PostElement> getList()
	{
		List<PostElement> elements = new ArrayList<>();
		elements.addAll(list);
		return elements;
	}


	private class ViewHolder
	{
		EditText etText;
		ImageView ivImage;
	}
}
