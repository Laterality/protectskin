package com.likefirst.protectskin.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.objects.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class MyInfoNotificationListAdapter extends BaseAdapter
{
	private static final String TAG = "NotiListAdapter";
	private Context mContext;
	private List<Notification> list;

	public MyInfoNotificationListAdapter(Context context, List<Notification> list)
	{
		mContext = context;
		this.list = new ArrayList<>();
		if(list != null){this.list.addAll(list);}
	}

	public void setList(List<Notification> list)
	{
		this.list.clear();
		this.list.addAll(list);
		Log.d(TAG, "list set...size : " + list.size());
		notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		if(list==null){Log.d(TAG, "list is null"); return 0;}
		return list.size();
	}

	@Override
	public Object getItem(int i)
	{
		if(list == null){return null;}
		return list.get(i);
	}

	@Override
	public long getItemId(int i)
	{
		if(list == null){return 0;}
		return list.get(i).getId();
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup)
	{
		Notification item = list.get(i);
		CommunityViewHolder holder;
		if(view == null)
		{
			holder = new CommunityViewHolder();
			view = LayoutInflater.from(mContext).inflate(R.layout.view_myinfo_content_list, null);
			
			holder.tvPostName = (TextView) view.findViewById(R.id.tv_myinfo_post_name);
			holder.tvPostContent = (TextView) view.findViewById(R.id.tv_myinfo_post_content);
			holder.tvRegDate = (TextView) view.findViewById(R.id.tv_myinfo_post_date);
			holder.tvAuthor = (TextView) view.findViewById(R.id.tv_myinfo_post_user);
			holder.tvReccText = (TextView) view.findViewById(R.id.tv_myinfo_post_get_recommend);
			holder.tvReccCnt = (TextView) view.findViewById(R.id.tv_myinfo_post_recommend_count);

			List<TextView> tvl = new ArrayList();
			tvl.add(holder.tvPostName);
			tvl.add(holder.tvPostContent);
			tvl.add(holder.tvRegDate);
			tvl.add(holder.tvAuthor);
			tvl.add(holder.tvReccText);
			tvl.add(holder.tvReccCnt);
			Typeface tf = (Typeface.createFromAsset(mContext.getAssets(), "NanumSquareB.ttf"));
			for (int j = 0; j < tvl.size(); j++)
			{
				tvl.get(j).setTypeface(tf);
			}

			holder.tvAuthor.setVisibility(View.GONE);
			holder.tvReccCnt.setVisibility(View.GONE);
			holder.tvReccText.setVisibility(View.GONE);

			view.setTag(holder);
		}
		else
		{
			holder = (CommunityViewHolder) view.getTag();
		}

		holder.tvPostName.setText(item.getTitle());
		holder.tvPostContent.setText(item.getMsg());
		holder.tvRegDate.setText(item.getDate());

		Log.d(TAG, "item view height : " + view.getHeight());
		
		return view;
	}

}
