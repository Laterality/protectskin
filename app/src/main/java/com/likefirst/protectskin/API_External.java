package com.likefirst.protectskin;

import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetGridDto;
import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetMinuteDustApiDto;
import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetUVApiDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by jinwoo on 2016-07-09.
 */
public class API_External
{
	public static final String API_KEY_KWEATHER = "5F5DB42E-FFFF00AA9";

    public static final String API_KEY_SKPLANET = "f86ced87-c75f-30ab-8360-32cf662c9760";
    public static final String BASE_URL_SKPLANET_API = "http://apis.skplanetx.com";

    // https://dvelopers.skplanetx.com/apidoc/kor/weather/living/#doc1379
    public static final String PATH_SKPLANET_WEATHER_DUST = "/weather/dust";
    public static final String PATH_SKPLANET_WEATHER_UV = "/weather/windex/uvindex";
	public static final String PATH_SKPLANET_WEATHER_GRID = "/weather/grid";

    public static final String QUERY_SKPLANET_WEATHER_VER = "version"; // just use 1
    public static final String QUERY_SKPLANET_WEATHER_LAT = "lat";
    public static final String QUERY_SKPLANET_WEATHER_LON = "lon";


    public static class Services
    {


        public interface getSkplanetMinuteDustService
        {
            @Headers(
                    {"Accept: application/json",
                    "appKey:" + API_KEY_SKPLANET})
            @GET(PATH_SKPLANET_WEATHER_DUST)
            public Call<WeatherPlanetMinuteDustApiDto> getDustInfo(@Query(QUERY_SKPLANET_WEATHER_VER)String version, @Query(QUERY_SKPLANET_WEATHER_LAT)String latitude, @Query(QUERY_SKPLANET_WEATHER_LON)String longitude);
        }

	    public interface getSkplanetUVService
	    {
		    @Headers({"appKey:" + API_KEY_SKPLANET})
	        @GET(PATH_SKPLANET_WEATHER_UV)
	        public Call<WeatherPlanetUVApiDto> getUVInfo(@Query(QUERY_SKPLANET_WEATHER_VER)String version, @Query(QUERY_SKPLANET_WEATHER_LAT)String latitude, @Query(QUERY_SKPLANET_WEATHER_LON)String longitude);

	    }

	    public interface getSkplanetGridService
	    {
		    @Headers({"appKey:" + API_KEY_SKPLANET})
		    @GET(PATH_SKPLANET_WEATHER_GRID)
		    public Call<WeatherPlanetGridDto> getGridInfo(@Query(QUERY_SKPLANET_WEATHER_VER)String version, @Query(QUERY_SKPLANET_WEATHER_LAT)String latitude, @Query(QUERY_SKPLANET_WEATHER_LON)String longitude);
	    }
    }
}
