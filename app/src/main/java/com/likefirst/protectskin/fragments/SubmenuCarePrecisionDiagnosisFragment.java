package com.likefirst.protectskin.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.likefirst.protectskin.R;

import java.io.File;

/**
 * Created by qkrpsg on 2016-08-18.
 */
public class SubmenuCarePrecisionDiagnosisFragment extends Fragment implements View.OnClickListener{

    private static View mView;

    private final String TAG = "Arcanelux_CameraIntent";

    private ImageView ivImage;
    private Button btnCamera;

    private Activity activity;
    private Uri outputFileUri;
    // 카메라 찍은 후 저장될 파일 경로
    public static String filePath;
    private String folderName = "Arcanelux";// 폴더명
    private String fileName = "CameraIntent"; // 파일명

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mView = inflater.inflate(R.layout.fragment_submenu_care_precision_diagnosis, null);

        activity = getActivity();

        ivImage = (ImageView) mView.findViewById(R.id.iv_care_camera_image);
        btnCamera = (Button) mView.findViewById(R.id.btn_turn_on_camera);

        btnCamera.setOnClickListener(this);

        return mView;
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId()){
            case R.id.btn_turn_on_camera:
                Intent intent = new Intent();

                // 저장할 파일 설정
                // 외부저장소 경로
                String path = Environment.getExternalStorageDirectory().getAbsolutePath();

                String folderPath = path + File.separator + folderName;
                filePath = path + File.separator + folderName + File.separator +  fileName + ".jpg";

                File fileFolderPath = new File(folderPath);
                fileFolderPath.mkdir();

                File file = new File(filePath);
                outputFileUri = Uri.fromFile(file);

                intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                intent.putExtra("return-data", true);
                activity.startActivityForResult(intent, 1004);
                break;

        }
    }

    public void onCameraIntentResult(Intent data)
    {
        //Uri uri = data.getData();
        try
        {
//			Log.d(TAG, "captured image path : " + uri.getPath());
//			Bitmap bm = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), outputFileUri);
            Bitmap bm = BitmapFactory.decodeFile(outputFileUri.getPath());

            if(bm.getHeight() < bm.getWidth()){
                bm = imgRotate(bm);
            }

            //scaling
            int srcW = ivImage.getWidth();
            float width = bm.getWidth();
            float height = bm.getHeight();
            if(width > srcW)
            {
                float percent = (float)(width / 100);
                float scale = (float)(srcW / percent);
                width *= (scale / 100);
                height *= (scale / 100);
            }
            Bitmap rbm = Bitmap.createScaledBitmap(bm, (int)width, (int)height, true);
            bm.recycle();

            ivImage.setImageBitmap(rbm);
            Log.d(TAG, "image set");

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private Bitmap imgRotate(Bitmap bmp){
        int width = bmp.getWidth();
        int height = bmp.getHeight();


        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, true);
        bmp.recycle();

        return resizedBitmap;
    }
}
