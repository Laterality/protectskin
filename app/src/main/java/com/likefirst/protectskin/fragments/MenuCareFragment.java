package com.likefirst.protectskin.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.utilities.InteractionFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class MenuCareFragment extends InteractionFragment implements View.OnClickListener, SubmenuCareDiagnosisFragment.RequestPrecisionListener
{
	private static View mView;

	private Button btnScheduler;
	private Button btnDiagnosis;
	private Button btnHowtoCare;

	private static SubmenuCareSchedulerFragment schedulerFragment;
	private static SubmenuCareDiagnosisFragment diagnosisFragment;
	private static SubmenuCareHowtoCareFragment howtoCareFragment;

	private SubmenuCarePrecisionDiagnosisFragment precDiagFragment;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView= inflater.inflate(R.layout.fragment_menu_care, null);

			btnScheduler = (Button) mView.findViewById(R.id.btn_menu_scheduler);
			btnDiagnosis = (Button) mView.findViewById(R.id.btn_menu_diagnosis);
			btnHowtoCare = (Button) mView.findViewById(R.id.btn_menu_howto_care);

			btnScheduler.setOnClickListener(this);
			btnDiagnosis.setOnClickListener(this);
			btnHowtoCare.setOnClickListener(this);

			schedulerFragment = new SubmenuCareSchedulerFragment(); schedulerFragment.injectView(
				inflater.inflate(R.layout.fragment_submenu_care_scheduler, null),
				inflater.inflate(R.layout.fragment_submenu_care_calendar, null),
				inflater.inflate(R.layout.activity_scheduler_detail, null));
			diagnosisFragment = new SubmenuCareDiagnosisFragment();
			diagnosisFragment.setOnRequestPrecisionListener(this);
			howtoCareFragment = new SubmenuCareHowtoCareFragment();

			List<Button> btnl = new ArrayList();
			Typeface tf = (Typeface.createFromAsset(getActivity().getAssets(),"NanumSquareB.ttf"));
			btnl.add(btnScheduler);
			btnl.add(btnDiagnosis);
			btnl.add(btnHowtoCare);
			for(int i = 0 ; i < btnl.size();i++)
			{
				btnl.get(i).setTypeface(tf);
			}
		}



		return mView;
	}

	@Override
	public void onClick(View view)
	{
		Fragment req = null;
		switch (view.getId())
		{
			case R.id.btn_menu_scheduler:
				req = schedulerFragment;
				schedulerFragment.setCurrentPage(0);
				break;
			case R.id.btn_menu_diagnosis:
				req = diagnosisFragment;
				break;
			case R.id.btn_menu_howto_care:
				req = howtoCareFragment;
				break;
			default:
				req = null;
				break;
		}
		super.requestShowFragment(req);
	}


	public void setDiagnosisImage(Intent data)
	{
		precDiagFragment.onCameraIntentResult(data);
	}

	@Override
	public Fragment getChild(int position)
	{
		switch (position)
		{
			case 0:
				return schedulerFragment;
			case 1:
				return diagnosisFragment;
			case 2:
				return howtoCareFragment;
			default:
				return null;
		}
	}

	@Override
	public boolean onBackPressed()
	{
		return schedulerFragment.onBackPressed();
	}

	@Override
	public void onRequest()
	{
		super.requestShowFragment(precDiagFragment = new SubmenuCarePrecisionDiagnosisFragment());
	}
}
