package com.likefirst.protectskin.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.DailyScheduleSet;
import com.likefirst.protectskin.api_dto.MonthlyScheduleSet;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.results.MonthlyScheduleSetResult;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.Utilities;
import com.likefirst.protectskin.views.CalendarViewIncl;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class SubmenuCareCalendarFragment extends Fragment implements CalendarViewIncl.OnDateClickListener
{
	private static final String TAG = "ScheduleFrag";
	private CalendarViewIncl.OnDateClickListener listener;

	private ProgressDialog pdLoad;
	private boolean injected = false;
	private String user_id;

	private View mView;

	private CalendarViewIncl cvCalendar;

	private Activity mContext;

	public SubmenuCareCalendarFragment()
	{
		//if(mView == null) {mView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_submenu_care_calendar, null);}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mContext = getActivity();
		user_id = ((CustomApplication)mContext.getApplication()).getUser().getId();
		if(mView == null || injected)
		{
			if(mView == null ){mView = inflater.inflate(R.layout.fragment_submenu_care_calendar, null);}
			injected = false;

			cvCalendar = (CalendarViewIncl) mView.findViewById(R.id.cv_submenu_care_scheduler_calendar);

			cvCalendar.setOnDateClickListener(this);
			// init here

			pdLoad = new ProgressDialog(mContext);
			pdLoad.setMessage("불러오는 중");
			pdLoad.setCancelable(false);

			refresh();
		}


		return mView;
	}

	public void refresh()
	{
		pdLoad.show();
		new LoadMonthlySchedule().execute(user_id);
	}

	public void setSchedule(MonthlyScheduleSet monthly)
	{
		List<DailyScheduleSet> daily = monthly.getSchedules();

		for(DailyScheduleSet s : daily)
		{
			DateTime d = new DateTime(s.getDate());
			cvCalendar.setCellState(
					cvCalendar.getIndexOfFirstDay() + d.getDayOfMonth() - 1,
					s.isFulfilled() ? CalendarViewIncl.CalendarCell.STATE_FULFILLED : CalendarViewIncl.CalendarCell.STATE_UNFULFILLED
			);
		}
	}

	public void setOnDateClickListener(CalendarViewIncl.OnDateClickListener l)
	{
		listener = l;
	}

	@Override
	public void onClick(DateTime t)
	{
		if(listener != null){listener.onClick(t);}
	}

	public void injectView(View v){mView = v; injected = true;}

	private class LoadMonthlySchedule extends AsyncTask<String, Void, MonthlyScheduleSetResult>
	{
		@Override
		protected MonthlyScheduleSetResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.ScheduleService service = retrofit.create(API_Internal.Services.ScheduleService .class);

				Call<MonthlyScheduleSetResult> call = service.getMonthlySchedule(param[0]);

				Response<MonthlyScheduleSetResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
			}
			return null;
		}// #doInBackground

		@Override
		protected void onPostExecute(MonthlyScheduleSetResult result)
		{
			pdLoad.dismiss();
			if(result == null)
			{
				Utilities.showToast(mContext, "스케줄을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}
			else if(result.getResult().equals(Result.RESULT_ERROR))
			{
				Utilities.showToast(mContext, "스케줄을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}

			((CustomApplication)mContext.getApplication()).setSchedule(result.getMonthlyScheduleSet());
			setSchedule(result.getMonthlyScheduleSet());

		}
	}

}
