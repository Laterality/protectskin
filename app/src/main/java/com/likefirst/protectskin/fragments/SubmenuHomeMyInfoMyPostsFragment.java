package com.likefirst.protectskin.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.likefirst.protectskin.R;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class SubmenuHomeMyInfoMyPostsFragment extends Fragment
{

	private static View mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_submenu_home_myinfo_myposts, null);


		}


		return mView;
	}

	@Override
	public View getView()
	{
		return mView;
	}
}
