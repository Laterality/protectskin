package com.likefirst.protectskin.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.utilities.InteractionFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class MenuCommunityFragment extends InteractionFragment implements View.OnClickListener, SubmenuCommunityThumbFragment.OnMoreEventListener, SubmenuCommunityListFragment.OnDetailRequestListener
{
	private String TAG = "CommunityFrag";

	private View mView;
	
	private Button btnKnowhow;
	private Button btnReview;
	private Button btnQnA;

	private static SubmenuCommunityThumbFragment knowhowFragment;
	private static SubmenuCommunityThumbFragment reviewFragment;
	private static SubmenuCommunityThumbFragment qnAFragment;


	private Fragment currentThumbFragment;
	private Fragment currentListFragment;
	private String prevDetailState;
	private int current_state;

	private static final int STATE_NONE = -1;
	private static final int STATE_THUMB = 0;
	private static final int STATE_LIST = 1;
	private static final int STATE_DETAIL = 2;




	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_menu_commnunity, null);

			btnKnowhow = (Button) mView.findViewById(R.id.btn_menu_knowhow);
			btnReview = (Button) mView.findViewById(R.id.btn_menu_review);
			btnQnA = (Button) mView.findViewById(R.id.btn_menu_qna);

			btnKnowhow.setOnClickListener(this);
			btnReview.setOnClickListener(this);
			btnQnA.setOnClickListener(this);

			knowhowFragment = new SubmenuCommunityThumbFragment();
			reviewFragment = new SubmenuCommunityThumbFragment();
			qnAFragment = new SubmenuCommunityThumbFragment();

			knowhowFragment.setArguments(new Bundle());
			reviewFragment.setArguments(new Bundle());
			qnAFragment.setArguments(new Bundle());

			knowhowFragment.setOnMoreEventListener(this);
			reviewFragment.setOnMoreEventListener(this);
			qnAFragment.setOnMoreEventListener(this);

			List<Button> btnl = new ArrayList();
			Typeface tf = (Typeface.createFromAsset(getActivity().getAssets(),"NanumSquareB.ttf"));
			btnl.add(btnKnowhow);
			btnl.add(btnReview);
			btnl.add(btnQnA);
			for(int i = 0 ; i < btnl.size();i++)
			{
				btnl.get(i).setTypeface(tf);
			}

		}

		currentThumbFragment = knowhowFragment;
		current_state = STATE_THUMB;

		return mView;
	}


	@Override
	public void onClick(View view)
	{
		Fragment req;
		Bundle b;

		switch (view.getId())
		{
			case R.id.btn_menu_knowhow:
				req = knowhowFragment;
				b = req.getArguments();
				b.putString("name", "노하우");
				break;
			case R.id.btn_menu_review:
				req = reviewFragment;
				b = req.getArguments();
				b.putString("name", "후기");
				break;
			case R.id.btn_menu_qna:
				req = qnAFragment;
				b = req.getArguments();
				b.putString("name", "Q&A");
				break;
			default:
				req = null;
		}
		currentThumbFragment = req;
		current_state = STATE_THUMB;
		super.requestShowFragment(req);
	}

	@Override
	public Fragment getChild(int position)
	{
		switch (position)
		{
			case 0:
				return knowhowFragment;
			case 1:
				return reviewFragment;
			case 2:
				return qnAFragment;
			default:
				return null;
		}
	}

	@Override
	public boolean onBackPressed()
	{
		Log.d(TAG, "back pressed, state : " + current_state);
		if(current_state == STATE_LIST)
		{
			super.requestShowFragment(currentThumbFragment);
			current_state = STATE_THUMB;
			return true;
		}
		else if(current_state == STATE_DETAIL)
		{
			if(prevDetailState.equals("thumb"))
			{
				super.requestShowFragment(currentThumbFragment);
				current_state = STATE_THUMB;
				return true;
			}
			else if(prevDetailState.equals("list"))
			{
				super.requestShowFragment(currentListFragment);
				current_state = STATE_LIST;
				return true;
			}
		}
		return false;
	}

	@Override
	public void onMore(String sort, String board, String skin, String keyword)
	{
		SubmenuCommunityListFragment f = new SubmenuCommunityListFragment();
		Bundle b = new Bundle();
		b.putString("sort", sort);
		b.putString("board", board);
		b.putString("skin", skin);
		b.putString("keyword", keyword);
		f.setArguments(b);
		f.setOnDetaiRequestListener(this);
		super.requestShowFragment(f);
		current_state = STATE_LIST;
		currentListFragment = f;
	}

	@Override
	public void onDetailRequest(String state, String id)
	{
		SubmenuCommunityDetailFragment f = new SubmenuCommunityDetailFragment();
		Bundle b = new Bundle();
		b.putString("id", id);
		f.setArguments(b);
		super.requestShowFragment(f);
		current_state = STATE_DETAIL;
		prevDetailState = state;
	}
}
