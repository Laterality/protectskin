package com.likefirst.protectskin.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.activities.SchedulerDetailActivity;
import com.likefirst.protectskin.utilities.CachedFragment;
import com.likefirst.protectskin.views.CalendarViewIncl;

import org.joda.time.DateTime;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class SubmenuCareSchedulerFragment extends Fragment implements CalendarViewIncl.OnDateClickListener
{
	private static final String TAG = "SchedulerFragment";

	private static View mView;
	private static boolean injected = false;

	private ViewPager vpPager;

	private static SubmenuCareCalendarFragment fCalendar;
	private static SubmenuCareCalendarDetailFragment fDetail;

	//private CalendarViewIncl cvCalendar;

	public SubmenuCareSchedulerFragment()
	{
		if(fCalendar == null) {fCalendar = new SubmenuCareCalendarFragment(); fCalendar.setOnDateClickListener(this);}
		if(fDetail == null){fDetail = new SubmenuCareCalendarDetailFragment();}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null || injected)
		{
			if(mView == null) {mView = inflater.inflate(R.layout.fragment_submenu_care_scheduler, null);}
			injected = false;


			vpPager = (ViewPager) mView.findViewById(R.id.vp_submenu_scheduler_pager);

			vpPager.setAdapter(new ViewPagerAdapter(getActivity().getSupportFragmentManager()));



			// init here
		}

		return mView;
	}

	public void setCurrentPage(int position)
	{
		vpPager.setCurrentItem(position);
	}

	public boolean onBackPressed()
	{
		Log.d(TAG, "current Item : " + vpPager.getCurrentItem());
		if(vpPager.getCurrentItem() == 1){vpPager.setCurrentItem(0); return true;}
		else{return false;}
	}


	@Override
	public void onClick(DateTime t)
	{
		vpPager.setCurrentItem(1);
		fDetail.setDate(t);
	}

	public void injectView(View view, View view2, View view3){mView = view; injected = true; fCalendar.injectView(view2); fDetail.injectView(view3);}

	private class ViewPagerAdapter extends FragmentPagerAdapter
	{
		private static final int COUNT = 2;

		public ViewPagerAdapter(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			switch (position)
			{
				case 0:
					return fCalendar;
				case 1:
					return fDetail;
			}

			return null;
		}

		@Override
		public int getCount()
		{
			return COUNT;
		}
	}
}
