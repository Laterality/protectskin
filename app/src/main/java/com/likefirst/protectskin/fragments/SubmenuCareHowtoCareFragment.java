package com.likefirst.protectskin.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.utilities.CachedFragment;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class SubmenuCareHowtoCareFragment extends Fragment
{

	private static View mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_submenu_care_howto_care, null);

			// init here


		}

		return mView;
	}
}
