package com.likefirst.protectskin.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.activities.SkinSelfDiagnosisStartActivity;


/**
 * Created by jinwoo on 2016-07-16.
 */
public class SubmenuCareDiagnosisFragment extends Fragment implements View.OnClickListener
{
	private static final String TAG = "careFrag";

	private RequestPrecisionListener listener;

	private Activity activity;
	private static View mView;

	private ImageView ivPrecisionDiagnosis;
	private ImageView ivSelfDiagnosis;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		activity = getActivity();

		if(mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_submenu_care_diagnosis, null);

			ivPrecisionDiagnosis = (ImageView) mView.findViewById(R.id.iv_care_go_to_precision_diagnosis);
			ivSelfDiagnosis = (ImageView) mView.findViewById(R.id.iv_care_go_to_self_diagnosis);

			ivPrecisionDiagnosis.setOnClickListener(this);
			ivSelfDiagnosis.setOnClickListener(this);

			// init here
		}

		return mView;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.iv_care_go_to_precision_diagnosis:
				if(listener != null)
				{
					listener.onRequest();
				}
				break;

			case R.id.iv_care_go_to_self_diagnosis:
				startActivity(new Intent(activity, SkinSelfDiagnosisStartActivity.class));
				break;

		}
	}

	public void setOnRequestPrecisionListener(RequestPrecisionListener listener)
	{
		this.listener = listener;
	}

	public interface RequestPrecisionListener
	{
		public void onRequest();
	}
}
