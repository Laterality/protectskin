package com.likefirst.protectskin.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.utilities.IFavorable;
import com.likefirst.protectskin.views.CustomViewPager;
import com.likefirst.protectskin.views.DynamicHeightViewPager;
import com.likefirst.protectskin.views.MainTabTitleView;
import com.likefirst.protectskin.views.MyInfoTabTitleView;

import java.lang.reflect.Field;

import xyz.santeri.wvp.WrappingFragmentStatePagerAdapter;
import xyz.santeri.wvp.WrappingViewPager;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class SubmenuHomeMyInfoFragment extends Fragment
{
	private static final String TAG = "MyInfo";
	private static View mView;

	private TabLayout tlTab;
	private WrappingViewPager vpPager;

	private ViewPagerAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_submenu_home_myinfo, null);

			TextView tv = (TextView) mView.findViewById(R.id.tv_myinfo_pz);
			Typeface tf = (Typeface.createFromAsset(getActivity().getAssets(), "seguibl.ttf"));
			tv.setTypeface(tf);
			tv = (TextView) mView.findViewById(R.id.tv_myinfo_user_name);
			tv.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "NanumSquareB.ttf"));

			tlTab = (TabLayout) mView.findViewById(R.id.tl_myinfo_menu);
			vpPager = (WrappingViewPager) mView.findViewById(R.id.vp_myinfo_content);

			// init here

			vpPager.setAdapter(adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager()));
			tlTab.setupWithViewPager(vpPager);

			for(int i = 0 ; i < tlTab.getTabCount(); i++)
			{
				tlTab.getTabAt(i).setCustomView(adapter.getTabView(i));
			}



		}


		return mView;
	}

	@Override
	public View getView()
	{
		return mView;
	}

	private class ViewPagerAdapter extends WrappingFragmentStatePagerAdapter
	{

		private static final int COUNT = 4;

		private SubmenuHomeMyInfoNotificationFragment fNoti;
		private SubmenuHomeMyInfoMyPostsFragment fMyPosts;
		private SubmenuHomeMyInfoRepliesFragment fReplies;
		private SubmenuHomeMyInfoBookmarksFragments fBookmarks;

		public ViewPagerAdapter(FragmentManager fm)
		{
			super(fm);

			fNoti = new SubmenuHomeMyInfoNotificationFragment();
			fNoti.setFavor(new IFavorable()
			{
				@Override
				public void favor()
				{
/*					Log.d(TAG, "mView.measuredHeight : " + fNoti.getView().getMeasuredHeight());

					ViewGroup.LayoutParams lp = vpPager.getLayoutParams();
					Log.d(TAG, "lp : " + lp.height);
					lp.height = fNoti.getView().getMeasuredHeight();
					vpPager.setLayoutParams(lp);

					Log.d(TAG, "vp height : " + vpPager.getHeight());
*/


				}
			});
			fMyPosts = new SubmenuHomeMyInfoMyPostsFragment();
			fReplies = new SubmenuHomeMyInfoRepliesFragment();
			fBookmarks = new SubmenuHomeMyInfoBookmarksFragments();

			vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
			{
				@Override
				public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
				{

				}

				@Override
				public void onPageSelected(int position)
				{
					Log.d(TAG, "page changed..." + position);
				}

				@Override
				public void onPageScrollStateChanged(int state)
				{

				}
			});
		}

		@Override
		public int getCount()
		{
			return COUNT;
		}

		@Override
		public Fragment getItem(int position)
		{
			switch (position)
			{
				case 0:
					return fNoti;
				case 1:
					return fMyPosts;
				case 2:
					return fReplies;
				case 3:
					return fBookmarks;
				default:
					return null;
			}
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			String[] list = getActivity().getResources().getStringArray(R.array.tab_myinfo);
			return list[position];
		}

		public View getTabView(int position)
		{
/*
			View rootView = LayoutInflater.from(mContext).inflate(R.layout.view_main_tab, null);
			TextView title = (TextView)rootView.findViewById(R.id.tv_main_tab_title);
			title.setText(getPageTitle(position));
			return rootView;
*/
			MyInfoTabTitleView mttv = new MyInfoTabTitleView(getActivity());
			mttv.setText(getPageTitle(position));
			return mttv;
		}
	}

}

