package com.likefirst.protectskin.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.utilities.InteractionFragment;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class MenuHomeFragment extends InteractionFragment implements View.OnClickListener
{
	private static final String TAG = "MenuHome";

	private static View mView;
	
	private Button btnMain;
	private Button btnMyInfo;
	private Button btnSetting;

	private static SubmenuHomeMainFragment mainFragment;
	private static SubmenuHomeMyInfoFragment myInfoFragment;
	private static SubmenuHomeSettingFragment settingFragment;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			Log.d(TAG, "destroyed? " + (getActivity().isDestroyed() ? "true" : "false") + " from " + this.getClass().getSimpleName() + "#onCreateView");
			mView = inflater.inflate(R.layout.fragment_menu_home, null);

			btnMain = (Button) mView.findViewById(R.id.btn_menu_home);
			btnMyInfo = (Button) mView.findViewById(R.id.btn_menu_my_info);
			btnSetting = (Button) mView.findViewById(R.id.btn_menu_setting);

			btnMain.setOnClickListener(this);
			btnMyInfo.setOnClickListener(this);
			btnSetting.setOnClickListener(this);

			mainFragment = new SubmenuHomeMainFragment();
			myInfoFragment = new SubmenuHomeMyInfoFragment(); myInfoFragment.setRetainInstance(true);
			settingFragment = new SubmenuHomeSettingFragment();

			List<Button> btnl = new ArrayList();
			Typeface tf = (Typeface.createFromAsset(getActivity().getAssets(),"NanumSquareB.ttf"));
			btnl.add(btnMain);
			btnl.add(btnMyInfo);
			btnl.add(btnSetting);
			for(int i = 0 ; i < btnl.size();i++)
			{
				btnl.get(i).setTypeface(tf);
			}
		}



		return mView;
	}

	public void invokeRefreshHome()
	{
		if(mainFragment != null){mainFragment.refresh();}
	}


	@Override
	public void onClick(View view)
	{
		Fragment req = null;
		switch (view.getId())
		{
			case R.id.btn_menu_home:
				req = mainFragment;
				break;
			case R.id.btn_menu_my_info:
				req = myInfoFragment;
				break;
			case R.id.btn_menu_setting:
				req = settingFragment;
				break;
			default:
				req = null;
		}
		Log.d(TAG, "destroyed? " + (getActivity().isDestroyed() ? "true" : "false") + " from " + this.getClass().getSimpleName() + "#onClick-1");
		boolean res = super.requestShowFragment(req);
		Log.d(TAG, "destroyed? " + (getActivity().isDestroyed() ? "true" : "false") + " from " + this.getClass().getSimpleName() + "#onClick-2");
		Log.d(TAG, "request " + (res? "success" : "fail"));
	}

	@Override
	public Fragment getChild(int position)
	{
		switch (position)
		{
			case 0:
				return mainFragment;
			case 1:
				return myInfoFragment;
			case 2:
				return settingFragment;
			default:
				return null;
		}
	}

	@Override
	public boolean onBackPressed()
	{
		return false;
	}


}
