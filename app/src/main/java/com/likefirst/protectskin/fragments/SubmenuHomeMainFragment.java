package com.likefirst.protectskin.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.activities.MainActivityModel;
import com.likefirst.protectskin.activities.SkinSelfDiagnosisStartActivity;
import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.results.UserResult;
import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetMinuteDustApiDto;
import com.likefirst.protectskin.skplanet_api_dto.WeatherPlanetUVApiDto;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.GPSActivity;
import com.likefirst.protectskin.utilities.GPSManager;
import com.likefirst.protectskin.utilities.Utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class SubmenuHomeMainFragment extends Fragment implements MainActivityModel.AsyncCallback
{
	private static final String TAG = "MainFrag";

	private static final int GRADE_GOOD = 1;
	private static final int GRADE_NORAML = 2;
	private static final int GRADE_BAD = 3;
	private static final int GRADE_WORSE = 4;
	private static final int GRADE_WORST = 5;

	private static final String COLOR_GOOD = "#4281F5";
	private static final String COLOR_NORMAL = "#5BD464";
	private static final String COLOR_BAD = "#FFBC3B";
	private static final String COLOR_WORSE = "#FE7F41";
	private static final String COLOR_WORST = "#F94A4B";

	private String[] gradeStr;
	private String[] msgUV;
	private String[] msgDust;
	private Activity activity;

	private boolean uvLoaded;
	private boolean dustLoaded;

	private String id;
	private List<String> sanitizedRec;

	private static View mView;

	private LinearLayout llOn;
	private LinearLayout llOff;
	private LinearLayout llSkintypeExists;
	private LinearLayout llSkintypeNotExists;
	private TextView tvSkinType;
	private TextView tvDustIndicator;
	private TextView tvDustGrade;
	private TextView tvUVIndicator;
	private TextView tvUVGrade;
	private TextView tvRecommendation;
	private TextView tvLocation;




	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		activity = getActivity();
		if (mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_submenu_home_main, null);

			llOn = (LinearLayout) mView.findViewById(R.id.ll_home_main_outer_gps_on);
			llOff = (LinearLayout) mView.findViewById(R.id.ll_home_main_outer_gps_off);
			llSkintypeExists = (LinearLayout) mView.findViewById(R.id.ll_home_main_skintype_exists);
			llSkintypeNotExists = (LinearLayout) mView.findViewById(R.id.ll_home_main_skintype_not_exists);
			tvSkinType = (TextView) mView.findViewById(R.id.tv_main_skin_type);
			tvDustIndicator = (TextView) mView.findViewById(R.id.tv_main_pm_indicator);
			tvDustGrade = (TextView) mView.findViewById(R.id.tv_main_pm_grade);
			tvUVIndicator = (TextView) mView.findViewById(R.id.tv_main_uv_indicator);
			tvUVGrade = (TextView) mView.findViewById(R.id.tv_main_uv_grade);
			tvRecommendation = (TextView) mView.findViewById(R.id.tv_main_recommendation_by_indicator);
			tvLocation = (TextView) mView.findViewById(R.id.tv_main_location);

			gradeStr = activity.getResources().getStringArray(R.array.grade_string);
			msgUV = activity.getResources().getStringArray(R.array.rec_msg_uv);
			msgDust = activity.getResources().getStringArray(R.array.rec_msg_dust);

			sanitizedRec = new ArrayList<>();

			Typeface tf = (Typeface.createFromAsset(activity.getAssets(), "NanumGothicExtraBold.ttf"));
			tvDustIndicator.setTypeface(tf);
			tvDustGrade.setTypeface(tf);
			tvUVIndicator.setTypeface(tf);
			tvUVGrade.setTypeface(tf);
			tvSkinType.setTypeface(tf);
			TextView tv = (TextView) mView.findViewById(R.id.tv_main_skin_type2);
			tv.setTypeface(tf);
			tv = (TextView) mView.findViewById(R.id.tv_main_go_to_self_diagnosis);
			tv.setTypeface(Typeface.createFromAsset(activity.getAssets(), "NanumSquareB.ttf"));

			tf = (Typeface.createFromAsset(activity.getAssets(), "NanumBarunGothic-YetHangul.ttf"));
			List<TextView> tvl = new ArrayList();
			tvl.add(tvRecommendation);
			tvl.add(tvLocation);
			tvl.add((TextView) mView.findViewById(R.id.tv_main_your_skin_type_is));
			tvl.add((TextView) mView.findViewById(R.id.tv_your_skin_type_is2));
			tvl.add((TextView) mView.findViewById(R.id.tv_main_daily_care_tip));
			tvl.add((TextView) mView.findViewById(R.id.tv_main_daily_care_tip_content));
			tvl.add((TextView) mView.findViewById(R.id.tv_main_daily_weather));
			tvl.add((TextView) mView.findViewById(R.id.tv_uv_data));
			tvl.add((TextView) mView.findViewById(R.id.tv_dust_data));
			tvl.add((TextView) mView.findViewById(R.id.tv_main_need_gps));
			for(int i=0;i<tvl.size();i++){
				tvl.get(i).setTypeface(tf);
			}

			llSkintypeNotExists.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					startActivity(new Intent(activity, SkinSelfDiagnosisStartActivity.class));
					//activity.finish();
				}
			});

			llOff.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					((GPSActivity)activity).showRequestGPSDialog(SubmenuHomeMainFragment.this);
				}
			});


		}

		//tvRecommendation.setText("");

		GPSManager gm = GPSManager.getInstance(activity);

		llOn.setVisibility(View.GONE);
		llOff.setVisibility(View.VISIBLE);

		llSkintypeExists.setVisibility(View.GONE);
		llSkintypeNotExists.setVisibility(View.VISIBLE);

		if(gm.isGPSEnabled())
		{
			llOn.setVisibility(View.VISIBLE);
			llOff.setVisibility(View.GONE);
		}

		id = ((CustomApplication)activity.getApplication()).getUser().getId();
		refresh();

		return mView;

	}

	public void refresh()
	{
		new LoadUserInfo().execute(id);
	}

	private void onRefreshed()
	{
		if(((CustomApplication)activity.getApplication()).getUser().getSkinType() != null)
		{
			llSkintypeExists.setVisibility(View.VISIBLE);
			tvSkinType.setText(String.format("\"%s\"", ((CustomApplication)activity.getApplication()).getUser().getSkinType().getName()));
			llSkintypeNotExists.setVisibility(View.GONE);
		}
	}

	@Override
	public void onPreExecute()
	{
		uvLoaded = false;
		dustLoaded = false;
	}

	@Override
	public void onGetDustInfo(WeatherPlanetMinuteDustApiDto dust)
	{
		Log.d(TAG, "callback executed");
		if (dust != null)
		{
			llOn.setVisibility(View.VISIBLE); llOff.setVisibility(View.GONE);
			float  pm10 = Float.valueOf(dust.getWeather().getDusts().get(0).getPm10().getValue());
			if(pm10 >= 0 && pm10 < 30) {setDust(GRADE_GOOD);}
			else if(pm10 >= 30 && pm10 < 80) {setDust(GRADE_NORAML);}
			else if(pm10 >= 80 && pm10 < 150) {setDust(GRADE_WORSE);}
			else if(pm10 >= 150){setDust(GRADE_WORST);}
			tvDustIndicator.setText(String.format(Locale.KOREAN,"%.2f", pm10));
			//tvDustGrade.setText(dust.getWeather().getDusts().get(0).getPm10().getGrade());
		} else
		{
			llOn.setVisibility(View.GONE); llOff.setVisibility(View.VISIBLE);
			tvDustIndicator.setTextColor(Color.BLACK);
			tvDustIndicator.setText("알수없음");
			tvDustGrade.setText(" ");
		}
		dustLoaded = true;
		if(uvLoaded && dustLoaded){setRecommendation();}
	}

	@Override
	public void onGetUVInfo(WeatherPlanetUVApiDto uv)
	{
		if (uv != null)
		{
			llOn.setVisibility(View.VISIBLE); llOff.setVisibility(View.GONE);
			tvLocation.setText(String.format("(%s %s)", Utilities.getCityStringMini(uv.getWeather().getwIndex().getUvindex().get(0).getGrid().getCity()),
					uv.getWeather().getwIndex().getUvindex().get(0).getGrid().getCounty()));
			int day = uv.getWeather().getwIndex().getUvindex().get(0).getDay(0).getIndex().isEmpty() ? 1 : 0;
			float _uv = Float.valueOf(uv.getWeather().getwIndex().getUvindex().get(0).getDay(day).getIndex());
			if(_uv >= 0 && _uv < 20){setUV(GRADE_GOOD);}
			else if(_uv >= 20 && _uv < 50){setUV(GRADE_NORAML);}
			else if(_uv >= 50 && _uv < 80){setUV(GRADE_BAD);}
			else if(_uv >= 80 && _uv < 100){setUV(GRADE_WORSE);}
			else if(_uv >= 100){setUV(GRADE_WORST);}
			tvUVIndicator.setText(String.format(Locale.KOREAN, "%.2f", _uv));
			sanitizedRec.add(uv.getWeather().getwIndex().getUvindex().get(0).getDay(day).getComment());
			//tvRecommendation.setText(uv.getWeather().getwIndex().getUvindex().get(0).getDay(day).getComment());
		} else
		{
			llOn.setVisibility(View.GONE); llOff.setVisibility(View.VISIBLE);
			tvDustIndicator.setTextColor(Color.BLACK);
			tvLocation.setText("");
			tvUVIndicator.setText("알수없음");
			tvUVGrade.setText(" ");
		}
		uvLoaded = true;
		if(uvLoaded && dustLoaded){setRecommendation();}
	}

	private void setDust(int grade)
	{
		String col = null;
		String msg = null;
		String gradeStr = null;
		switch (grade)
		{
			case GRADE_GOOD:
				col = COLOR_GOOD;
				msg = msgDust[0];
				gradeStr = this.gradeStr[0];
				break;
			case GRADE_NORAML:
				col = COLOR_NORMAL;
				msg = msgDust[1];
				gradeStr = this.gradeStr[1];
				break;
			case GRADE_BAD:
				col = COLOR_BAD;
				msg = msgDust[2];
				gradeStr = this.gradeStr[2];
				break;
			case GRADE_WORSE:
				col = COLOR_WORSE;
				msg = msgDust[3];
				gradeStr = this.gradeStr[3];
				break;
		}
		tvDustIndicator.setTextColor(Color.parseColor(col));
		tvDustGrade.setTextColor(Color.parseColor(col));
		tvDustGrade.setText(gradeStr);
		sanitizedRec.add(msg);
	}

	private void setUV(int uv)
	{
		String col = null;
		String msg = null;
		String gradeStr = null;
		switch (uv)
		{
			case GRADE_GOOD:
				col = COLOR_GOOD;
				msg = msgUV[0];
				gradeStr = this.gradeStr[0];
				break;
			case GRADE_NORAML:
				col = COLOR_NORMAL;
				msg = msgUV[1];
				gradeStr = this.gradeStr[1];
				break;
			case GRADE_BAD:
				col = COLOR_BAD;
				msg = msgUV[2];
				gradeStr = this.gradeStr[2];
				break;
			case GRADE_WORSE:
				col = COLOR_WORSE;
				msg = msgUV[3];
				gradeStr = this.gradeStr[3];
				break;
			case GRADE_WORST:
				col = COLOR_WORST;
				msg = msgUV[4];
				gradeStr = this.gradeStr[4];
		}
		tvUVIndicator.setTextColor(Color.parseColor(col));
		tvUVGrade.setTextColor(Color.parseColor(col));
		tvUVGrade.setText(gradeStr);
		sanitizedRec.add(msg);
	}

	private void setRecommendation()
	{
		Log.d(TAG,"set rec msg");
		tvRecommendation.setText("");
//		sanitizedRec.clear();
		for(int i = 0 ; i < sanitizedRec.size(); i++)
		{
			tvRecommendation.append((i == 0 ? "" : "\n") + sanitizedRec.get(i));
		}
//		sanitizedRec.clear();
	}



	private class LoadUserInfo extends AsyncTask<String, Void, UserDto>
	{

		@Override
		protected UserDto doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.UserService service = retrofit.create(API_Internal.Services.UserService.class);

				Call<UserResult> call = service.getUser(param[0]);

				Response<UserResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body().getUser();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		} // #doInBackground

		@Override
		protected void onPostExecute(UserDto user)
		{
			if(user == null)
			{
				Toast.makeText(activity, "사용자 정보를 가져오지 못했습니다", Toast.LENGTH_SHORT).show();
				return;
			}
			((CustomApplication) activity.getApplication()).setUser(user);
			Log.d(TAG, "user : " + user.getId() + ", from " + this.getClass().getSimpleName());
			onRefreshed();
		}
	}
}
