package com.likefirst.protectskin.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.api_dto.DailyScheduleSet;
import com.likefirst.protectskin.api_dto.MonthlyScheduleSet;
import com.likefirst.protectskin.api_dto.ScheduleDto;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.Utilities;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class SubmenuCareCalendarDetailFragment extends Fragment implements View.OnClickListener
{
	private static final String TAG = "SchedulerDetailFrag";
	private DateTime currentDate;
	private boolean injected = false;

	private MonthlyScheduleSet scheduleSet;
	private Animation animSpreadOut;
	private Animation animSpreadIn;

	private Activity mContext;
	private View mView;

	private TextView tvDateIndicator;
	private TextView tvFulfilledDay;
	private TextView tvFulfillRate;
	private ImageView ivPrev;
	private ImageView ivNext;
	private Button btnMorning;
	private Button btnNoon;
	private Button btnEvening;
	private LinearLayout llMorning;
	private LinearLayout llNoon;
	private LinearLayout llEvening;
	private View.OnClickListener scheduleClickListener;

	private boolean visibleMorning;
	private boolean visibleNoon;
	private boolean visibleEvening;



	public SubmenuCareCalendarDetailFragment()
	{

	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mContext = getActivity();
		if(mView == null || injected)
		{
			if(mView == null) {mView = inflater.inflate(R.layout.activity_scheduler_detail, null);}
			injected = false;
			
			// init here
			animSpreadOut = AnimationUtils.loadAnimation(mContext, R.anim.sliding_0_100);
			animSpreadIn = AnimationUtils.loadAnimation(mContext, R.anim.sliding_100_0);
			tvDateIndicator = (TextView)mView.findViewById(R.id.tv_calendar_detail_indicator);
			tvFulfillRate = (TextView) mView.findViewById(R.id.tv_scheduler_detail_fulfill_rate_indicator);
			tvFulfilledDay = (TextView) mView.findViewById(R.id.tv_scheduler_detail_fulfill_rate_day);
			ivPrev = (ImageView) mView.findViewById(R.id.iv_calendar_detail_prev_day);
			ivNext = (ImageView) mView.findViewById(R.id.iv_calendar_detail_next_day);
			btnMorning = (Button) mView.findViewById(R.id.btn_calendar_morning);
			btnNoon = (Button) mView.findViewById(R.id.btn_calendar_afternoon);
			btnEvening = (Button) mView.findViewById(R.id.btn_calendar_evening);
			llMorning = (LinearLayout) mView.findViewById(R.id.ll_scheduler_detail_morning);
			llNoon = (LinearLayout) mView.findViewById(R.id.ll_scheduler_detail_afternoon);
			llEvening = (LinearLayout) mView.findViewById(R.id.ll_scheduler_detail_evening);

			ivPrev.setOnClickListener(this);
			ivNext.setOnClickListener(this);
			btnMorning.setOnClickListener(this);
			btnNoon.setOnClickListener(this);
			btnEvening.setOnClickListener(this);

			scheduleClickListener = new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					ScheduleDto s = (ScheduleDto) view.getTag();
				}
			};

			Typeface tf = (Typeface.createFromAsset(getActivity().getAssets(),"arittaDodumBold.ttf"));

			List<TextView> tvl = new ArrayList();
			tvl.add((TextView) mView.findViewById(R.id.tv_calendar_this_months_practice_rate));
			tvl.add((TextView) mView.findViewById(R.id.tv_scheduler_detail_fulfill_rate_indicator));
			tvl.add((TextView) mView.findViewById(R.id.tv_scheduler_detail_fulfill_rate_day));
			for(int j = 0 ; j < tvl.size();j++)
			{
				tvl.get(j).setTypeface(tf);
			}

			List<Button> btnl = new ArrayList();
			btnl.add((Button) mView.findViewById(R.id.btn_calendar_morning));
			btnl.add((Button) mView.findViewById(R.id.btn_calendar_afternoon));
			btnl.add((Button) mView.findViewById(R.id.btn_calendar_evening));
			for(int j = 0 ; j < tvl.size();j++)
			{
				btnl.get(j).setTypeface(tf);
			}

			tvDateIndicator.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "NanumSquareB.ttf"));
			
		}

		//refresh();
		
		return mView;
	}

	private void refresh()
	{
		if(scheduleSet == null){return;}
		tvDateIndicator.setText(String.format(Locale.KOREAN, "%d월 %d일 (%s)", currentDate.getMonthOfYear(), currentDate.getDayOfMonth(), Utilities.getDayOfWeekString(currentDate)));
		tvFulfilledDay.setText(String.format(Locale.KOREAN, "(%d일 중 %d일 성공)", scheduleSet.getDayEntire(), scheduleSet.getDayFulfilled()));
		tvFulfillRate.setText(String.format(Locale.KOREAN, "%.1f", (float)scheduleSet.getDayFulfilled()/scheduleSet.getDayEntire()) + "%");
		List<DailyScheduleSet> dss = scheduleSet.getSchedules();
		for(DailyScheduleSet s : dss)
		{
			DateTime d = new DateTime(s.getDate());
			if(d.getYear() == currentDate.getYear() &&
					d.getMonthOfYear() == currentDate.getMonthOfYear() &&
					d.getDayOfMonth() == currentDate.getDayOfMonth())
			{
				setDailySchedule(s);
				break;
			}
		}
		// refresh codes here
	}

	public void setDailySchedule(DailyScheduleSet d)
	{
		visibleMorning = false;
		visibleNoon = false;
		visibleEvening = false;
		llMorning.removeAllViews(); llMorning.setVisibility(View.GONE); btnMorning.setText("아침▽");
		llNoon.removeAllViews();    llNoon.setVisibility(View.GONE); btnNoon.setText("점심▽");
		llEvening.removeAllViews(); llEvening.setVisibility(View.GONE); btnEvening.setText("저녁▽");
		for(ScheduleDto s : d.getSchedules())
		{
			View v = LayoutInflater.from(mContext).inflate(R.layout.view_calendar_detail_list, null);
			((TextView)v.findViewById(R.id.tv_calendar_detail_content)).setText(s.getText());
			if(s.isFulfilled()){((ImageView)v.findViewById(R.id.iv_calendar_detail_check)).setImageDrawable(mContext.getResources().getDrawable(R.drawable.check_o_icon));}
			v.setTag(s);
			if(s.getPeriod().equals(ScheduleDto.PERIOD_MORNING))
			{
				llMorning.addView(v);
			}
			else if(s.getPeriod().equals(ScheduleDto.PERIOD_NOON))
			{
				llNoon.addView(v);
			}
			else if(s.getPeriod().equals(ScheduleDto.PERIOD_EVENING))
			{
				llEvening.addView(v);
			}
		}
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.iv_calendar_detail_prev_day:
				currentDate = currentDate.minusDays(1);
				refresh();
				break;
			case R.id.iv_calendar_detail_next_day:
				currentDate = currentDate.plusDays(1);
				refresh();
				break;
			default:
				break;
			case R.id.btn_calendar_morning:
				if(visibleMorning)
				{
					llMorning.startAnimation(animSpreadIn);
					llMorning.setVisibility(View.GONE);
					btnMorning.setText("아침▽");
					visibleMorning = false;
				}
				else
				{
					llMorning.startAnimation(animSpreadOut);
					llMorning.setVisibility(View.VISIBLE);
					btnMorning.setText("아침△");
					visibleMorning = true;
				}
				break;
			case R.id.btn_calendar_afternoon:
				if(visibleNoon)
				{
					llNoon.startAnimation(animSpreadIn);
					llNoon.setVisibility(View.GONE);
					btnNoon.setText("점심▽");
					visibleNoon = false;
				}
				else
				{
					llNoon.startAnimation(animSpreadOut);
					llNoon.setVisibility(View.VISIBLE);
					btnNoon.setText("점심△");
					visibleNoon = true;
				}
				break;
			case R.id.btn_calendar_evening:
				if(visibleEvening)
				{
					llEvening.startAnimation(animSpreadIn);
					llEvening.setVisibility(View.GONE);
					btnEvening.setText("저녁▽");
					visibleEvening = false;
				}
				else
				{
					llEvening.startAnimation(animSpreadOut);
					llEvening.setVisibility(View.VISIBLE);
					btnEvening.setText("저녁△");
					visibleEvening = true;
				}
				break;
		}


	}
	
	public void setDate(DateTime t)
	{
		currentDate = t;
		scheduleSet = ((CustomApplication)mContext.getApplication()).getSchedule(currentDate.getYear(), currentDate.getMonthOfYear());
		refresh();
	}
	

	public void injectView(View v){mView = v; injected = true;}
}
