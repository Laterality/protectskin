package com.likefirst.protectskin.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.activities.VersionInfoActivity;
import com.likefirst.protectskin.adapters.SettingListAdapter;
import com.likefirst.protectskin.objects.SettingObject;
import com.likefirst.protectskin.utilities.CachedFragment;
import com.likefirst.protectskin.utilities.CustomApplication;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class SubmenuHomeSettingFragment extends Fragment
{

	private static View mView;

	private ListView lvSettings;
	private SettingListAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null) {mView = inflater.inflate(R.layout.fragment_submenu_home_setting, null);}

		lvSettings = (ListView) mView.findViewById(R.id.lv_setting_list);

		lvSettings.setAdapter(adapter = new SettingListAdapter(getActivity(), ((CustomApplication)getActivity().getApplication()).getSettings()));
		lvSettings.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
			{
				SettingObject selectedSetting = ((SettingListAdapter.ViewHolder)view.getTag()).getSetting();
				switch (i)
				{
					case 0:
						// show version
						startActivity(new Intent(getActivity(), VersionInfoActivity.class));
						break;
					case 1:
						// login setting
						break;
					case 2:
						selectedSetting.setBoolValue(!selectedSetting.getBoolValue());
						break;
					case 3:
						// show notices
						break;
					case 4:
						// show agreement for providing geometric information
						break;
					case 5:
						// show quitting dialog
						break;
				}
				((CustomApplication)getActivity().getApplication()).changeSetting(selectedSetting);
				adapter.notifyDataSetChanged();
			}
		});


		return mView;
	}

}
