package com.likefirst.protectskin.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.adapters.PostCategorySpinnerAdapter;
import com.likefirst.protectskin.adapters.ThumbPostListAdapter;
import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.results.PostsResult;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.PW486;
import com.likefirst.protectskin.utilities.Utilities;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class SubmenuCommunityQnAFragment extends Fragment
{
	private static final String TAG = "QnaFrag";
	private static View mView;



	private PullToRefreshScrollView svBackground;
	private Spinner spCategory;
	private ListView lvLatest;
	private ListView lvPopular;

	private String currentSkinType = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_submenu_community_qna, null);

			// init here

			svBackground = (PullToRefreshScrollView) mView.findViewById(R.id.sv_qna_background);
			spCategory = (Spinner) mView.findViewById(R.id.sp_qna_category);
			lvLatest = (ListView) mView.findViewById(R.id.lv_qna_posts_latest);
			lvPopular = (ListView) mView.findViewById(R.id.lv_qna_posts_popular);

			svBackground.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>()
			{
				@Override
				public void onRefresh(PullToRefreshBase<ScrollView> refreshView)
				{
					refresh();
				}
			});

			spCategory.setAdapter(new PostCategorySpinnerAdapter(getActivity()));

			spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
			{
				@Override
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
				{
					switch (i)
					{
						case 0:
							currentSkinType = null;
							break;
						case 1:
							currentSkinType = PW486.SKINTYPE_DRY;
							break;
						case 2:
							currentSkinType = PW486.SKINTYPE_GENERAL;
							break;
						case 3:
							currentSkinType = PW486.SKINTYPE_OILY;
							break;
						case 4:
							currentSkinType = PW486.SKINTYPE_COMPLEX;
							break;
						case 5:
							currentSkinType = PW486.SKINTYPE_SENSITIVE;
							break;
					}

					refresh();
				}

				@Override
				public void onNothingSelected(AdapterView<?> adapterView)
				{
				}
			});

		}


		refresh();

		return mView;
	}


	private void refresh()
	{
		new LoadPostList().execute(
				((CustomApplication)getActivity().getApplication()).getUser().getId(),
				currentSkinType,
				String.valueOf(new Date().getTime()),
				"3",
				"date");

		new LoadPostList().execute(
				((CustomApplication)getActivity().getApplication()).getUser().getId(),
				currentSkinType,
				String.valueOf(new Date().getTime()),
				"3",
				"recommend");
	}

	private class LoadPostList extends AsyncTask<String, Void, PostsResult>
	{
		private String sort;
		@Override
		protected PostsResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();


				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<PostsResult> call = service.getPosts(
						param[0],
						String.valueOf(PW486.BOARD_QNA),
						param[1],
						Long.valueOf(param[2]),
						Integer.valueOf(param[3]),
						sort = param[4],
						"false"
				);
				Response<PostsResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				Log.d(TAG, e.getMessage());
				e.printStackTrace();
			}
			return null;
		}// #doInBackground

		@Override
		protected void onPostExecute(PostsResult result)
		{

			if(result == null)
			{
				Toast.makeText(getActivity(), "게시물을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
				return;
			}

			String strResult = result.getResult();

			if(strResult.equals(Result.RESULT_ERROR))
			{
				Toast.makeText(getActivity(), "게시물을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
				return;
			}

			List<PostDto> posts = result.getPosts();
			if(posts == null)
			{
				Toast.makeText(getActivity(), "게시물 목록을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT).show();
				return;
			}
			if(sort.equals("date"))
			{
				lvLatest.setAdapter(new ThumbPostListAdapter(getActivity(), posts));
				Utilities.setListViewHeightBasedOnChildren(lvLatest);
				svBackground.onRefreshComplete();
			}
			else if(sort.equals("recommend"))
			{
				lvPopular.setAdapter(new ThumbPostListAdapter(getActivity(), posts));
				Utilities.setListViewHeightBasedOnChildren(lvPopular);
				svBackground.onRefreshComplete();
			}
		}
	}

}
