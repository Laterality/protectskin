package com.likefirst.protectskin.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.activities.PostWriteActivity;
import com.likefirst.protectskin.adapters.ListPostListAdapter;
import com.likefirst.protectskin.adapters.PostCategorySpinnerAdapter;
import com.likefirst.protectskin.adapters.PostContentAdapter;
import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.results.PostsResult;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.PW486;
import com.likefirst.protectskin.utilities.Utilities;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-08-08.
 *
 * 게시물 상세 목록 프래그먼트
 */
public class SubmenuCommunityListFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener
{
	private static final String TAG = "CommListFrag";


	private Spinner spCategory;
	private PullToRefreshListView lvPosts;
	private FloatingActionButton fabWrite;
	private ImageView ivSearch;
	private ImageView ivClear;
	private EditText etKeyword;

	private OnDetailRequestListener listener;
	private Context mContext;

	private ListPostListAdapter plAdapter;


	private String currentSort;
	private String currentSkinType;
	private String board_id;
	private String keyword;

	private boolean clearFlag = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_submenu_community_list, container, false);

		mContext = getActivity();

		lvPosts = (PullToRefreshListView) rootView.findViewById(R.id.lv_community_list_posts);
		spCategory = (Spinner) rootView.findViewById(R.id.sp_community_list_category);
		fabWrite = (FloatingActionButton) rootView.findViewById(R.id.fab_list_write_post);
		ivSearch = (ImageView) rootView.findViewById(R.id.iv_list_search);
		ivClear = (ImageView) rootView.findViewById(R.id.iv_list_clear);
		etKeyword = (EditText) rootView.findViewById(R.id.et_community_list_search_keyword);

		etKeyword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "NanumBarunGothic-YetHangul.ttf"));

		currentSort = getArguments().getString("sort");
		currentSkinType = getArguments().getString("skin");
		keyword = getArguments().getString("keyword");
		board_id = getArguments().getString("board");
		if(keyword != null){etKeyword.setText(keyword);}

		Log.d(TAG, "sort : " + currentSort + ", skin : " + currentSkinType);

		fabWrite.setOnClickListener(this);
		ivClear.setOnClickListener(this);
		ivSearch.setOnClickListener(this);

		spCategory.setAdapter(new PostCategorySpinnerAdapter(mContext));

		spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
			{
				if(keyword != null)
				{
					return;
				}
				switch (i)
				{
					case 0:
						currentSkinType = null;
						break;
					case 1:
						currentSkinType = PW486.SKINTYPE_DRY;
						break;
					case 2:
						currentSkinType = PW486.SKINTYPE_GENERAL;
						break;
					case 3:
						currentSkinType = PW486.SKINTYPE_OILY;
						break;
					case 4:
						currentSkinType = PW486.SKINTYPE_COMPLEX;
						break;
					case 5:
						currentSkinType = PW486.SKINTYPE_SENSITIVE;
						break;
				}

				refresh();
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView)
			{

			}
		});

		lvPosts.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
		{
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refresh();
			}
		});

		etKeyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					search();
					return true;
				}
				return false;
			}
		});

		lvPosts.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener()
		{
			@Override
			public void onLastItemVisible()
			{
				clearFlag = false;
				addRefresh();
			}
		});

		lvPosts.setAdapter(plAdapter = new ListPostListAdapter(mContext, null));
		lvPosts.setOnItemClickListener(this);



		spinnerInit();

		return rootView;
	}

	private void spinnerInit()
	{
		if(currentSkinType == null) // 전체
		{}
		else if(currentSkinType.equals(PW486.SKINTYPE_DRY))
		{
			spCategory.setSelection(1);
		}
		else if(currentSkinType.equals(PW486.SKINTYPE_GENERAL))
		{
			spCategory.setSelection(2);
		}
		else if(currentSkinType.equals(PW486.SKINTYPE_OILY))
		{
			spCategory.setSelection(3);
		}
		else if(currentSkinType.equals(PW486.SKINTYPE_COMPLEX))
		{
			spCategory.setSelection(4);
		}
		else if(currentSkinType.equals(PW486.SKINTYPE_SENSITIVE))
		{
			spCategory.setSelection(5);
		}

		if(keyword != null)
		{
			search();
			return;
		}
		refresh();
	}


	private void refresh()
	{
		new LoadPostList().execute(((CustomApplication) getActivity().getApplication()).getUser().getId(),
				currentSkinType,
				String.valueOf(new Date().getTime()),
				"10",
				currentSort);
	}

	private void addRefresh()
	{
		new LoadPostList().execute(((CustomApplication) getActivity().getApplication()).getUser().getId(),
				currentSkinType,
				String.valueOf(plAdapter.getLastPostRegDate()),
				"10",
				currentSort);
	}

	private void search()
	{
		new SearchPosts().execute(
				etKeyword.getText().toString(),
				board_id,
				currentSkinType,
				currentSort
		);

	}


	public void setOnDetaiRequestListener(OnDetailRequestListener listener)
	{
		this.listener = listener;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
	{
		//Log.d(TAG, "list item clicked, pos : " + i + ", id : " + plAdapter.getItem(i).getId());
		listener.onDetailRequest("list", plAdapter.getItem(i-1).getId());
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.fab_list_write_post:
				Intent i = new Intent(getActivity(), PostWriteActivity.class);
				i.putExtra("board", board_id);
				startActivity(i);
				break;
			case R.id.iv_list_clear:
				etKeyword.setText("");
				break;
			case R.id.iv_list_search:
				search();
				break;
		}
	}


	private class LoadPostList extends AsyncTask<String, Void, PostsResult>
	{

		@Override
		protected PostsResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder().baseUrl(API_Internal.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();


				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<PostsResult> call = service.getPosts(param[0],
						String.valueOf(PW486.BOARD_KNOWHOW),
						param[1], Long.valueOf(param[2]),
						Integer.valueOf(param[3]),
						param[4],
						"true");
				Response<PostsResult> res = call.execute();

				if (res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				} else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				Log.d(TAG, e.getMessage());
				e.printStackTrace();
			}
			return null;
		}// #doInBackground

		@Override
		protected void onPostExecute(PostsResult result)
		{

			if (result == null)
			{
				Toast.makeText(getActivity(), "게시물을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
				return;
			}

			String strResult = result.getResult();

			if (strResult.equals(Result.RESULT_ERROR))
			{
				Toast.makeText(getActivity(), "게시물을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
				return;
			}

			List<PostDto> posts = result.getPosts();
			if (posts == null)
			{
				Toast.makeText(getActivity(), "게시물 목록을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT).show();
				return;
			}
			if(clearFlag){plAdapter.setList(result.getPosts());}
			else{plAdapter.addList(result.getPosts());clearFlag = true;}
			lvPosts.onRefreshComplete();
		}
	}

	private class SearchPosts extends AsyncTask<String, Void, PostsResult>
	{
		/**
		 *
		 * @param param [0] : keyword, [1] : board id, [2] : skin type id, [3] : sort
		 * @return
		 */
		@Override
		protected PostsResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<PostsResult> call = service.searchPost(param[0], param[1], param[2], param[3]);

				Response<PostsResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		} // #doInBackground

		@Override
		protected void onPostExecute(PostsResult result)
		{
			if(result == null)
			{
				Utilities.showToast(mContext, "검색 결과를 가져오는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}

			if(result.getResult().equals(Result.RESULT_ERROR))
			{
				Utilities.showToast(mContext, "검색 결과를 가져오는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}
			else if(result.getResult().equals(Result.RESULT_SUCCESS))
			{
				keyword = null;
				plAdapter.setList(result.getPosts());
			}
		}
	}


	public interface OnDetailRequestListener
	{
		public void onDetailRequest(String state, String id);
	}
}
