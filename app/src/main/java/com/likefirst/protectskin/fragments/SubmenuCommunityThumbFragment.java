package com.likefirst.protectskin.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.adapters.PostCategorySpinnerAdapter;
import com.likefirst.protectskin.adapters.ThumbPostListAdapter;
import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.results.PostsResult;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.PW486;
import com.likefirst.protectskin.utilities.Utilities;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class SubmenuCommunityThumbFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener
{
	private static final String TAG = "KnowhowP1Frag";

	private View mView;


	private OnMoreEventListener listener;

	private TextView tvMoreLatest;
	private TextView tvMorePopular;
	private TextView tvHeaderLatest;
	private TextView tvHeaderPopular;
	private ImageView ivClear;
	private ImageView ivSearch;
	private EditText etKeyword;

	private PullToRefreshScrollView svBackground;
	private Spinner spCategory;
	private ListView lvLatest;
	private ListView lvPopular;

	private String currentSkinType = null;
	private String board_id;
	
	private Context mContext;
	private ThumbPostListAdapter plaLatest;
	private ThumbPostListAdapter plaPopular;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
	{
		if(mView == null)
		{
			mView = inflater.inflate(R.layout.fragment_submenu_community_knowhow_thumb, parent, false);

			mContext = getActivity();

			tvMoreLatest = (TextView) mView.findViewById(R.id.tv_community_thumb_more_latest);
			tvMorePopular = (TextView) mView.findViewById(R.id.tv_community_thumb_more_popular);
			tvHeaderLatest = (TextView) mView.findViewById(R.id.tv_community_thumb_latest_header);
			tvHeaderPopular = (TextView) mView.findViewById(R.id.tv_community_thumb_popular_header);
			ivClear = (ImageView) mView.findViewById(R.id.iv_community_thumb_search_clear);
			ivSearch = (ImageView) mView.findViewById(R.id.iv_thumb_search);
			etKeyword = (EditText) mView.findViewById(R.id.et_community_thumb_search_keyword);

			svBackground = (PullToRefreshScrollView) mView.findViewById(R.id.sv_community_thumb_background);
			spCategory = (Spinner) mView.findViewById(R.id.sp_community_thumb_category);
			lvLatest = (ListView) mView.findViewById(R.id.lv_community_thumb_posts_latest);
			lvPopular = (ListView) mView.findViewById(R.id.lv_community_thumb_posts_popular);

			Typeface tf = (Typeface.createFromAsset(getActivity().getAssets(), "NanumSquareB.ttf"));
			tvHeaderLatest.setTypeface(tf);
			tvHeaderPopular.setTypeface(tf);
			tf = (Typeface.createFromAsset(getActivity().getAssets(), "NanumBarunGothic.ttf"));
			etKeyword.setTypeface(tf);
			tvMoreLatest.setTypeface(tf);
			tvMorePopular.setTypeface(tf);

			tvMoreLatest.setOnClickListener(this);
			tvMorePopular.setOnClickListener(this);

			ivClear.setOnClickListener(this);
			ivSearch.setOnClickListener(this);



			etKeyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_SEARCH) {
						search();
						return true;
					}
					return false;
				}
			});


			svBackground.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>()
			{
				@Override
				public void onRefresh(PullToRefreshBase<ScrollView> refreshView)
				{
					refresh();
				}
			});

			spCategory.setAdapter(new PostCategorySpinnerAdapter(mContext));

			spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
			{
				@Override
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
				{
					switch (i)
					{
						case 0:
							currentSkinType = null;
							break;
						case 1:
							currentSkinType = PW486.SKINTYPE_DRY;
							break;
						case 2:
							currentSkinType = PW486.SKINTYPE_GENERAL;
							break;
						case 3:
							currentSkinType = PW486.SKINTYPE_OILY;
							break;
						case 4:
							currentSkinType = PW486.SKINTYPE_COMPLEX;
							break;
						case 5:
							currentSkinType = PW486.SKINTYPE_SENSITIVE;
							break;
					}

					refresh();
				}

				@Override
				public void onNothingSelected(AdapterView<?> adapterView)
				{

				}
			});

			lvLatest.setAdapter(plaLatest = new ThumbPostListAdapter(mContext, null));
			lvPopular.setAdapter(plaPopular = new ThumbPostListAdapter(mContext, null));

			lvLatest.setOnItemClickListener(this);
			lvPopular.setOnItemClickListener(this);

			String name = getArguments().getString("name");
			if(name == null){name = "노하우";}

			tvHeaderPopular.setText(name + " 인기글" );
			tvHeaderLatest.setText(name + " 최신글");

			if(name.equals("노하우")){board_id = PW486.BOARD_KNOWHOW;}
			else if(name.equals("후기")){board_id = PW486.BOARD_REVIEW;}
			else if(name.equals("Q&A")){board_id = PW486.BOARD_QNA;}

			refresh();
		}





		return mView;
	}



	private void search()
	{
		if(etKeyword.getText().length() < 3)
		{
			Utilities.showToast(mContext, "검색어를 3자 이상 입력해주세요", Toast.LENGTH_SHORT);
			return;
		}
		if(listener != null)
		{
			listener.onMore("date", board_id, currentSkinType, etKeyword.getText().toString());
			etKeyword.setText("");
		}
	}

	private void refresh()
	{
		new LoadPostList().execute(((CustomApplication) getActivity().getApplication()).getUser().getId(), currentSkinType, String.valueOf(new Date().getTime()), "3", "date");
		new LoadPostList().execute(((CustomApplication) getActivity().getApplication()).getUser().getId(), currentSkinType, String.valueOf(new Date().getTime()), "3", "recommend");
	}

	@Override
	public void onClick(View view)
	{
		String sort = null;
		switch (view.getId())
		{
			case R.id.lv_community_thumb_posts_latest:
				sort = "date";
				break;
			case R.id.tv_community_thumb_more_popular:
				sort = "recommend";
				break;
			case R.id.iv_community_thumb_search_clear:
				etKeyword.setText("");
				break;
			case R.id.iv_thumb_search:
				search();
				break;
		}

		if (listener != null)
		{
			listener.onMore(sort, board_id, currentSkinType, null);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
	{
		switch (adapterView.getId())
		{
			case R.id.lv_community_thumb_posts_latest:
				listener.onDetailRequest("thumb", plaLatest.getItem(i).getId());
				break;
			case R.id.lv_community_thumb_posts_popular:
				listener.onDetailRequest("thumb", plaPopular.getItem(i).getId());
				break;
		}
	}

	public void setOnMoreEventListener(OnMoreEventListener listener)
	{
		this.listener = listener;
	}

	private class LoadPostList extends AsyncTask<String, Void, PostsResult>
	{
		private String sort;

		@Override
		protected PostsResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder().baseUrl(API_Internal.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();


				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<PostsResult> call = service.getPosts(param[0], board_id, param[1], Long.valueOf(param[2]), Integer.valueOf(param[3]), sort = param[4], "false");
				Response<PostsResult> res = call.execute();

				if (res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				} else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (Exception e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				Log.d(TAG, e.getMessage());
				e.printStackTrace();
			}
			return null;
		}// #doInBackground

		@Override
		protected void onPostExecute(PostsResult result)
		{

			if (result == null)
			{
				Toast.makeText(mContext, "게시물을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
				return;
			}

			String strResult = result.getResult();

			if (strResult.equals(Result.RESULT_ERROR))
			{
				Toast.makeText(mContext, "게시물을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
				return;
			}

			List<PostDto> posts = result.getPosts();
			if (posts == null)
			{
				Toast.makeText(mContext, "게시물 목록을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT).show();
				return;
			}
			if (sort.equals("date"))
			{
				plaLatest.setList(result.getPosts());
				Utilities.setListViewHeightBasedOnChildren(lvLatest);
				svBackground.onRefreshComplete();
			} else if (sort.equals("recommend"))
			{
				plaPopular.setList(result.getPosts());
				lvPopular.setAdapter(new ThumbPostListAdapter(mContext, posts));
				Utilities.setListViewHeightBasedOnChildren(lvPopular);
				svBackground.onRefreshComplete();
			}
		}
	}




	public interface OnMoreEventListener
	{
		public void onMore(String sort, String board, String skin, String keyword);
		public void onDetailRequest(String state, String id);
	}

}
