package com.likefirst.protectskin.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.likefirst.protectskin.API_Internal;
import com.likefirst.protectskin.R;
import com.likefirst.protectskin.adapters.PostContentAdapter;
import com.likefirst.protectskin.adapters.ReplyListAdapter;
import com.likefirst.protectskin.api_dto.FootStep;
import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.PostElement;
import com.likefirst.protectskin.api_dto.ReplyDto;
import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.results.PostResult;
import com.likefirst.protectskin.api_dto.results.RecommendResult;
import com.likefirst.protectskin.api_dto.results.ReplyResult;
import com.likefirst.protectskin.utilities.CustomApplication;
import com.likefirst.protectskin.utilities.Utilities;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jinwoo on 2016-08-08.
 *
 * 게시물 내용 프래그먼트
 */
public class SubmenuCommunityDetailFragment extends Fragment implements View.OnClickListener
{
	private static final String TAG = "CommDetailFrag";

	private Context mContext;

	private TextView tvTitle;
	private TextView tvAuthor;
	private TextView tvRegDate;
	private TextView tvCountView;
	private TextView tvCountRecommend;
	private TextView tvCountReplies;
	private TextView ivRecommend;
	private TextView ivBookmark;
	private EditText etReply;
	private Button btnAddReply;
	//private ListView lvContent;
	private ListView lvReplies;
	private PullToRefreshScrollView svBackground;
	private LinearLayout llContent;

	private ProgressDialog pd;

	private String post_id;
	private String user_id;


	private FootStep currentFootStep;

	private ReplyListAdapter rlaReply;
//	private PostContentAdapter pcAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_submenu_community_detail, parent, false);

		View header = inflater.inflate(R.layout.fragment_submenu_community_detail_header, null);
//		View header = inflater.inflate(R.layout.fragment_submenu_community_detail_header, null);
//		View header = inflater.inflate(R.layout.fragment_submenu_community_detail_header, null);
		View footer = inflater.inflate(R.layout.fragment_submenu_community_detail_footer, null);


		mContext = getActivity();

//		tvTitle = (TextView) rootView.findViewById(R.id.tv_community_detail_title);
//		tvAuthor = (TextView) rootView.findViewById(R.id.tv_community_detail_author);
//		tvRegDate = (TextView) rootView.findViewById(R.id.tv_community_detail_regdate);
//		tvCountView = (TextView) rootView.findViewById(R.id.tv_community_detail_count_view);
//		tvCountRecommend = (TextView) rootView.findViewById(R.id.tv_community_detail_count_recommend);
//		tvContent = (TextView) rootView.findViewById(R.id.tv_community_detail_content);
//		tvCountReplies = (TextView) rootView.findViewById(R.id.tv_community_detail_count_replies);
//		ivRecommend = (TextView) rootView.findViewById(R.id.tv_community_detail_recommend);
//		ivBookmark = (TextView) rootView.findViewById(R.id.tv_community_detail_add_bookmark);
//		etReply = (EditText) rootView.findViewById(R.id.et_community_detail_reply);
//		btnAddReply = (Button) rootView.findViewById(R.id.btn_detail_add_reply);
//		lvReplies = (ListView) rootView.findViewById(R.id.lv_community_detail_replies);
//		svBackground = (PullToRefreshScrollView) rootView.findViewById(R.id.sv_detail_scroll);

		//
		tvTitle = (TextView) header.findViewById(R.id.tv_community_detail_title);
		tvAuthor = (TextView) header.findViewById(R.id.tv_community_detail_author);
		tvRegDate = (TextView) header.findViewById(R.id.tv_community_detail_regdate);
		tvCountView = (TextView) header.findViewById(R.id.tv_community_detail_count_view);
		tvCountRecommend = (TextView) header.findViewById(R.id.tv_community_detail_count_recommend);
		//lvContent = (ListView) header.findViewById(R.id.lv_community_detail_content);
		tvCountReplies = (TextView) header.findViewById(R.id.tv_community_detail_count_replies);
		ivRecommend = (TextView) header.findViewById(R.id.tv_community_detail_recommend);
		ivBookmark = (TextView) header.findViewById(R.id.tv_community_detail_add_bookmark);
		etReply = (EditText) footer.findViewById(R.id.et_community_detail_reply);
		btnAddReply = (Button) footer.findViewById(R.id.btn_detail_add_reply);
		lvReplies = (ListView) rootView.findViewById(R.id.lv_community_detail_replies);
		//svBackground = (PullToRefreshScrollView) rootView.findViewById(R.id.sv_detail_scroll);
		llContent = (LinearLayout) header.findViewById(R.id.ll_detail_content);

//		lvContent.addHeaderView(header);
//		lvContent.addFooterView(header);
		lvReplies.addHeaderView(header);
		lvReplies.addFooterView(footer);
		//

		Typeface tf = (Typeface.createFromAsset(getActivity().getAssets(), "NanumBarunGothic-YetHangul.ttf"));
		etReply.setTypeface(tf);
		List<TextView> tvl = new ArrayList();
		tvl.add(tvAuthor);
		tvl.add(tvRegDate);
		tvl.add(tvCountView);
		tvl.add(tvCountRecommend);
		//tvl.add(tvContent);
		tvl.add(tvCountReplies);
		tvl.add(ivRecommend);
		tvl.add(ivBookmark);

		for(int i=0; i < tvl.size(); i++){
			tvl.get(i).setTypeface(tf);
		}

		tf = (Typeface.createFromAsset(getActivity().getAssets(), "NanumSquareB.ttf"));
		tvTitle.setTypeface(tf);
		tvCountReplies.setTypeface(tf);
		btnAddReply.setTypeface(tf);

		ivRecommend.setOnClickListener(this);
		ivBookmark.setOnClickListener(this);
		btnAddReply.setOnClickListener(this);

		rlaReply = new ReplyListAdapter(mContext, null);
		lvReplies.setAdapter(rlaReply);
//		pcAdapter = new PostContentAdapter(mContext, null);
//		lvContent.setAdapter(pcAdapter);
//		setListViewHeighBasedOnChildren(lvReplies);

//		svBackground.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>()
//		{
//			@Override
//			public void onRefresh(PullToRefreshBase<ScrollView> refreshView)
//			{
//				refresh();
//			}
//		});


		pd = new ProgressDialog(getActivity());
		pd.setMessage("게시물 가져오는 중");
		pd.setCancelable(false);

		post_id = getArguments().getString("id");
		user_id = ((CustomApplication) getActivity().getApplication()).getUser().getId();

		Log.d(TAG, "post id : " + post_id);

		refresh();


		return rootView;
	}

	private void setPost(PostDto post)
	{
		DateTime d = new DateTime(Long.valueOf(post.getRegDate()));
		tvTitle.setText(post.getTitle());
		tvAuthor.setText(post.getAuthor().getUsername());
		tvRegDate.setText(String.format(Locale.KOREA, "%d.%d.%02d", d.getYear(), d.getMonthOfYear(), d.getDayOfMonth()));
		tvCountView.setText(String.format(Locale.KOREA, "조회수 : %d", post.getCountView()));
		tvCountRecommend.setText(String.format(Locale.KOREA, "추천수 : %d", post.getCountRecommend()));
		//pcAdapter.setContent(post.getContent());
		renderContent(post);
		tvCountReplies.setText(String.format(Locale.KOREA, "댓글 %d", post.getCountReplies()));

		//setFootStepState(f);
	}

	private void renderContent(PostDto post)
	{
		for(PostElement e : post.getContent())
		{
			if(e.getType().equals(PostElement.TYPE_TEXT))
			{
				TextView tv = new TextView(mContext);
				tv.setText(e.getContent());
				tv.setTextColor(Color.BLACK);
				tv.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "NanumBarunGothic-YetHangul.ttf"));
				llContent.addView(tv);
			}
			else if(e.getType().equals(PostElement.TYPE_IMAGE))
			{
				ImageView iv = new ImageView(mContext);
				iv.setAdjustViewBounds(true);
				Picasso.with(mContext).load(API_Internal.BASE_URL + e.getContent()).into(iv);
				llContent.addView(iv);
			}
		}
	}


	private void refresh()
	{
		pd.show();
		new LoadPost().execute(post_id, user_id);
		new LoadReplies().execute(post_id);
	}

	private void setFootStepState(FootStep f, boolean showToast)
	{
		Log.d(TAG, "showToast : " + showToast + ", current : " + currentFootStep.isRecommended() + ", f : " + f.isRecommended());
		if (f.isRecommended())
		{
			if (showToast &&
					(currentFootStep.isRecommended() != f.isRecommended())) Utilities.showToast(mContext, "추천했습니다", Toast.LENGTH_SHORT);
			ivRecommend.setTextColor(Color.parseColor("#FF9999")); // 추천한 상태일때
		} else
		{
			if (showToast &&
					(currentFootStep.isRecommended() != f.isRecommended())) Utilities.showToast(mContext, "추천을 취소했습니다", Toast.LENGTH_SHORT);
			ivRecommend.setTextColor(Color.parseColor("#BFBFBF")); // 추천한 상태가 아닐 때
		}


		if (f.isBookmarked())
		{
			if (showToast &&
					(currentFootStep.isBookmarked() != f.isBookmarked())) Utilities.showToast(mContext, "북마크에 추가했습니다", Toast.LENGTH_SHORT);
			ivBookmark.setTextColor(Color.parseColor("#FFD966")); // 북마크에 추가했을 때
		} else
		{
			if (showToast &&
					(currentFootStep.isBookmarked() != f.isBookmarked())) Utilities.showToast(mContext, "북마크에서 삭제했습니다", Toast.LENGTH_SHORT);
			ivBookmark.setTextColor(Color.parseColor("#BFBFBF")); // 북마크에 추가하지 않았을 때
		}

		currentFootStep = f;
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.tv_community_detail_recommend:
				new RecommendPost().execute(post_id, user_id);
				break;
			case R.id.tv_community_detail_add_bookmark:
				new BookmarkPost().execute(post_id, user_id);
				break;
			case R.id.btn_detail_add_reply:
				new AddReply().execute(new ReplyDto(user_id, etReply.getText().toString(), false, post_id));
				etReply.setText("");
				break;
		}
	}


	private class LoadPost extends AsyncTask<String, Void, PostResult>
	{

		/**
		 * @param param [0] : 게시물 id, [1] : 사용자 id
		 * @return
		 */
		@Override
		protected PostResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder().baseUrl(API_Internal.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<PostResult> call = service.getPost(param[0], param[1]);

				Response<PostResult> res = call.execute();

				if (res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				} else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		}// #doInBackground

		@Override
		protected void onPostExecute(PostResult result)
		{
			if (result == null)
			{
				pd.dismiss();
				Utilities.showToast(mContext, "게시물을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}

			if (result.getResult().equals(Result.RESULT_ERROR))
			{
				pd.dismiss();
				Utilities.showToast(mContext, "게시물을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT);
				return;
			}

			currentFootStep = result.getFootStep();
			setPost(result.getPost());
			setFootStepState(result.getFootStep(), false);
			//svBackground.onRefreshComplete();
			pd.dismiss();

		}
	}

	private class RecommendPost extends AsyncTask<String, Void, RecommendResult>
	{
		@Override
		protected RecommendResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder().baseUrl(API_Internal.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<RecommendResult> call = service.recommendPost(param[0], param[1]);

				Response<RecommendResult> res = call.execute();

				if (res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				} else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		} // #doInBackground

		@Override
		protected void onPostExecute(RecommendResult result)
		{
			if (result == null)
			{
				Utilities.showToast(mContext, "잠시 후 다시 시도해주세요", Toast.LENGTH_SHORT);
				return;
			}

			if (result.getResult().equals(Result.RESULT_ERROR))
			{
				Utilities.showToast(mContext, "잠시 후 다시 시도해주세요", Toast.LENGTH_SHORT);
				return;
			} else if (result.getResult().equals(Result.RESULT_SUCCESS))
			{
				setFootStepState(result.getFootStep(), true);
			}

		}
	}


	private class BookmarkPost extends AsyncTask<String, Void, RecommendResult>
	{
		@Override
		protected RecommendResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder().baseUrl(API_Internal.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<RecommendResult> call = service.bookmarkPost(param[0], param[1]);

				Response<RecommendResult> res = call.execute();

				if (res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				} else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		} // #doInBackground

		@Override
		protected void onPostExecute(RecommendResult result)
		{
			if (result == null)
			{
				Utilities.showToast(mContext, "잠시 후 다시 시도해주세요", Toast.LENGTH_SHORT);
				return;
			}

			if (result.getResult().equals(Result.RESULT_ERROR))
			{
				Utilities.showToast(mContext, "잠시 후 다시 시도해주세요", Toast.LENGTH_SHORT);
				return;
			} else if (result.getResult().equals(Result.RESULT_SUCCESS))
			{
				setFootStepState(result.getFootStep(), true);
			}
		}
	}


	private class LoadReplies extends AsyncTask<String, Void, ReplyResult>
	{

		/**
		 *
		 * @param param [0] : 게시물 아이디
		 * @return
		 */
		@Override
		protected ReplyResult doInBackground(String... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<ReplyResult> call = service.getReplies(param[0]);

				Response<ReplyResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		} // #doInBackground

		@Override
		protected void onPostExecute(ReplyResult result)
		{
			if(result == null)
			{
				Utilities.showToast(mContext, "댓글 목록을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT);
				return;
			}

			if(result.getResult().equals(Result.RESULT_ERROR))
			{
				Utilities.showToast(mContext, "댓글 목록을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT);
				return;
			}
			else if(result.getResult().equals(Result.RESULT_SUCCESS))
			{
				rlaReply.setList(result.getReplies());
				//Utilities.setListViewHeightBasedOnChildren(lvReplies);
			}

		}
	}

	private class AddReply extends AsyncTask<ReplyDto, Void, ReplyResult>
	{

		@Override
		protected ReplyResult doInBackground(ReplyDto... param)
		{
			try
			{
				Retrofit retrofit = new Retrofit.Builder()
						.baseUrl(API_Internal.BASE_URL)
						.addConverterFactory(GsonConverterFactory.create())
						.build();

				API_Internal.Services.PostService service = retrofit.create(API_Internal.Services.PostService.class);

				Call<ReplyResult> call = service.addReply(post_id, param[0]);

				Response<ReplyResult> res = call.execute();

				if(res.isSuccessful())
				{
					Log.d(TAG, "Execution success from " + this.getClass().getSimpleName());
					return res.body();
				}
				else
				{
					Log.d(TAG, "Execution fail from " + this.getClass().getSimpleName());
					Log.d(TAG, res.code() + " : " + res.message());
				}
			}
			catch (IOException e)
			{
				Log.d(TAG, "Exception occurred from " + this.getClass().getSimpleName());
				e.printStackTrace();
			}
			return null;
		}// #doInBackground

		@Override
		protected void onPostExecute(ReplyResult result)
		{
			if(result == null)
			{
				Utilities.showToast(mContext, "댓글 목록을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT);
				return;
			}

			if(result.getResult().equals(Result.RESULT_ERROR))
			{
				Utilities.showToast(mContext, "댓글 목록을 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT);
				return;
			}
			else if(result.getResult().equals(Result.RESULT_SUCCESS))
			{
				Utilities.showToast(mContext, "댓글을 등록했습니다", Toast.LENGTH_SHORT);
				rlaReply.setList(result.getReplies());
//				Utilities.setListViewHeightBasedOnChildren(lvReplies);
			}
		}
	}
}
