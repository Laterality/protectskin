package com.likefirst.protectskin.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.likefirst.protectskin.R;
import com.likefirst.protectskin.adapters.MyInfoNotificationListAdapter;
import com.likefirst.protectskin.objects.Notification;
import com.likefirst.protectskin.utilities.IFavorable;
import com.likefirst.protectskin.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class SubmenuHomeMyInfoNotificationFragment extends Fragment
{
	private static final String TAG = "NotiFragment";

	private MyInfoNotificationListAdapter adapter;
	private IFavorable favor;

	private static View mView;

	private ListView lvNoti;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.d(TAG, "onCreateView()...entry");
		if(mView == null)
		{
			Log.d(TAG, "inflate view");
			mView = inflater.inflate(R.layout.fragment_submenu_home_myinfo_notification, null);

			lvNoti = (ListView) mView.findViewById(R.id.lv_myinfo_notification_list);

			lvNoti.setAdapter(adapter = new MyInfoNotificationListAdapter(getActivity(), null));

		}

		if(lvNoti == null){lvNoti = (ListView) mView.findViewById(R.id.lv_myinfo_notification_list);}

		updateNoti();

		return mView;
	}

	private void updateNoti()
	{
		Log.d(TAG, "update notification list");
		if(adapter != null) {adapter.setList(getDummyNotis());}
		Log.d(TAG, "lv h : " + Utilities.setListViewHeightBasedOnChildren(lvNoti));
		mView.measure(mView.getWidth(), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
		Log.d(TAG, "mView measured h : " + mView.getMeasuredHeight());
		if(favor != null){favor.favor();}
	}


	private List<Notification> getDummyNotis()
	{
		List<Notification> list = new ArrayList<>();

		list.add(new Notification("댓글 알림", "[게시글 제목] 글에 덧글이 달렸습니다", "2016. 7. 01"));
		list.add(new Notification("추천 알림", "[게시글 제목] 글이 추천되었습니다", "2016. 7. 01"));
		list.add(new Notification("추천 알림", "[게시글 제목] 글이 추천되었습니다", "2016. 7. 02"));

		return list;
	}

	@Override
	public View getView(){return mView;}

	public void setFavor(IFavorable favor)
	{
		this.favor = favor;
	}

}
