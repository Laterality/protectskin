package com.likefirst.protectskin.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.likefirst.protectskin.R;

/**
 * Created by jinwoo on 2016-08-18.
 */
public class SkinTypeDescriptionDryFragment extends Fragment
{

	private TextView tvDry1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_skin_type_description_dry, null);

		tvDry1 = (TextView) rootView.findViewById(R.id.tv_description_dry_1);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "NanumBarunGothic-YetHangul.ttf");
		tvDry1.setTypeface(tf);


		return rootView;
	}
}
