package com.likefirst.protectskin.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class NonSwipeViewPager extends ViewPager
{
	public NonSwipeViewPager(Context context)
	{
		super(context);
	}

	public NonSwipeViewPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event)
	{
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		return false;
	}
}
