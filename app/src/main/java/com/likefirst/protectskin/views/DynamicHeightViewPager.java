package com.likefirst.protectskin.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class DynamicHeightViewPager extends ViewPager
{
	private static final String TAG = "DynamicHeightVP";

	private Boolean mAnimStarted = false;

	public DynamicHeightViewPager(Context context)
	{
		super(context);
	}

	public DynamicHeightViewPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}


	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	protected void onMeasure(int widthMesasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMesasureSpec, heightMeasureSpec);

		if (!mAnimStarted && getAdapter() != null)
		{
			int height = 0;
			View child = ((FragmentPagerAdapter) getAdapter()).getItem(getCurrentItem()).getView();
			if (child != null)
			{
				child.measure(widthMesasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
				height = child.getMeasuredHeight();
				Log.d(TAG, "height : " + height);
			}

			// Not the best place to put this animation, but it works pretty good.
			int newHeight = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
/*			if (getLayoutParams().height != 0 && heightMeasureSpec != newHeight)
			{
				final int targetHeight = height;
				final int currentHeight = getLayoutParams().height;
				final int heightChange = targetHeight - currentHeight;


				Animation a = new Animation()
				{
					@Override
					protected void applyTransformation(float interpolatedTime, Transformation t)
					{
						if (interpolatedTime >= 1)
						{
							getLayoutParams().height = targetHeight;
						} else
						{
							int stepHeight = (int) (heightChange * interpolatedTime);
							getLayoutParams().height = currentHeight + stepHeight;
						}
						requestLayout();
					}

					@Override
					public boolean willChangeBounds()
					{
						return true;
					}
				};

				a.setAnimationListener(new Animation.AnimationListener()
				{
					@Override
					public void onAnimationStart(Animation animation)
					{
						mAnimStarted = true;
					}

					@Override
					public void onAnimationEnd(Animation animation)
					{
						mAnimStarted = false;
					}

					@Override
					public void onAnimationRepeat(Animation animation)
					{
					}
				});

				a.setDuration(1000);
				startAnimation(a);
				mAnimStarted = true;
			} else
			{
				heightMeasureSpec = newHeight;
			}*/

			heightMeasureSpec = newHeight;
			Log.d(TAG, "heightMeasureSpec : " + heightMeasureSpec + ", height : " + height);
			requestLayout();
		}
		super.onMeasure(widthMesasureSpec, heightMeasureSpec);
	}
}
