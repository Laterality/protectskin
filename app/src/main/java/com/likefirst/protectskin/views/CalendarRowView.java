package com.likefirst.protectskin.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.likefirst.protectskin.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-13.
 */
public class CalendarRowView extends LinearLayout
{
	private List<CalendarCellView> days;

	public CalendarRowView(Context context)
	{
		super(context);
		init(context);
	}

	public CalendarRowView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public CalendarRowView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(final Context context)
	{
		days = new ArrayList<>();
		LayoutInflater.from(context).inflate(R.layout.view_calendar_cell_row, CalendarRowView.this, true);
		Resources res = getResources();
		int id;
		for (int i = 0; i < 7; i++)
		{
			id = res.getIdentifier("ccv_" + (i + 1), "id", getContext().getPackageName());
			days.add((CalendarCellView) findViewById(id));
		}
	}

	public List<CalendarCellView> getDays()
	{
		return days;
	}
}
