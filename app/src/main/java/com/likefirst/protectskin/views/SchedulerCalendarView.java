package com.likefirst.protectskin.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;

import org.joda.time.*;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-13.
 */
public class SchedulerCalendarView extends LinearLayout implements View.OnClickListener
{
	private static final String TAG = "CalendarView";
	private OnDateClickListener listener;
	private DateTime currentDate;
	private List<CalendarCellView> days;
	private List<CalendarRowView> weeks;
	private TextView tvIndicator;
	private ImageView ivPrev;
	private ImageView ivNext;

	private int start_idx = 0;

	public SchedulerCalendarView(Context context)
	{
		super(context);
		init(context);
	}

	public SchedulerCalendarView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public SchedulerCalendarView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(final Context context)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Looper.prepare();
				Handler h = new Handler()
				{
					public void handleMessage(Message msg)
					{
						_init(context);
					}
				};

				Message msg = h.obtainMessage();
				h.sendMessage(msg);


			}
		}).start();
	}


	private void _init(final Context context)
	{
		//currentDate = DateTime.now(DateTimeZone.UTC.forOffsetHours(9));
		currentDate = DateTime.now();
		Log.d(TAG, "today : " + currentDate.toString("yy-MM-dd HH:mm:ss") + " " + currentDate.getDayOfWeek());
		days = new ArrayList<>();
		weeks = new ArrayList<>();
		Log.d(TAG, "flag-0");
		LayoutInflater.from(context).inflate(R.layout.view_calendar, SchedulerCalendarView.this, true);

		Log.d(TAG, "flag-0.5");
		ivPrev = (ImageView) findViewById(R.id.iv_calendar_prev_month);
		ivNext = (ImageView) findViewById(R.id.iv_calendar_next_month);
		tvIndicator = (TextView) findViewById(R.id.tv_calendar_indicator);

		List<TextView> tvl = new ArrayList();
		Typeface tf = (Typeface.createFromAsset(context.getAssets(),"NanumSquareB.ttf"));
		tvl.add(tvIndicator);
		tvl.add((TextView) findViewById(R.id.tv_calendar_sunday));
		tvl.add((TextView) findViewById(R.id.tv_calendar_monday));
		tvl.add((TextView) findViewById(R.id.tv_calendar_tuesday));
		tvl.add((TextView) findViewById(R.id.tv_calendar_wednesday));
		tvl.add((TextView) findViewById(R.id.tv_calendar_thursday));
		tvl.add((TextView) findViewById(R.id.tv_calendar_friday));
		tvl.add((TextView) findViewById(R.id.tv_calendar_saturday));
		for(int i = 0 ; i < tvl.size();i++)
		{
			tvl.get(i).setTypeface(tf);
		}

		Resources res = getResources();
		int id;
		for(int i = 1 ; i <= 6; i ++)
		{
			id = res.getIdentifier("ccr_calendar_week_" + i, "id", getContext().getPackageName());
			weeks.add((CalendarRowView)findViewById(id));
			days.addAll(weeks.get(i-1).getDays());
		}
		ivPrev.setOnClickListener(this);
		ivNext.setOnClickListener(this);

		for(int i = 0 ; i < days.size(); i++)
		{
			days.get(i).setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					if(listener != null)
					{
						listener.onClick(((CalendarCellView)view).getDate());
					}
				}
			});
		}

		setCalendar();
	}

	private void setCalendar()
	{
		int month = currentDate.getMonthOfYear();
		int year = currentDate.getYear();
		tvIndicator.setText(String.format("%d년 %d월", year, month));

		int start = 0;
		switch (new DateTime(currentDate.getYear(), currentDate.getMonthOfYear(), 1, 0 , 0 ).getDayOfWeek())
		{
			case DateTimeConstants.SUNDAY:
				start = 0;
				break;
			case DateTimeConstants.MONDAY:
				start = 1;
				break;
			case DateTimeConstants.TUESDAY:
				start = 2;
				break;
			case DateTimeConstants.WEDNESDAY:
				start = 3;
				break;
			case DateTimeConstants.THURSDAY:
				start = 4;
				break;
			case DateTimeConstants.FRIDAY:
				start = 5;
				break;
			case DateTimeConstants.SATURDAY:
				start = 6;
				break;
			default:
				break;
		}
		start_idx = start;

		DateTime dt = new DateTime(currentDate.getYear(), currentDate.getMonthOfYear(), 1, 0, 0);

		for(int i = 0 ; i < days.size(); i++)
		{
			if(i<start) {days.get(i).setDate(dt.minusDays(Math.abs(i - start)));}
			else{days.get(i).setDate(dt.plusDays(i - start));}

			if(days.get(i).getDate().getMonthOfYear() != currentDate.getMonthOfYear())
			{
				days.get(i).setTextColor("#BFBFBF");
			}
			else
			{
				days.get(i).setTextColor("#000000");
			}
		}

		/*for(int i = start - 1 ; i >= 0; i --)
		{
			days.get(i).setText(String.valueOf(currentDate.minusMonths(1).dayOfMonth().getMaximumValue() - i));
			days.get(i).setTextColor("#BFBFBF");
		}
		for(int i = start, d = 1; d <= currentDate.dayOfMonth().getMaximumValue(); i++, d++)
		{
			days.get(i).setText(String.valueOf(d));
			days.get(i).setTextColor("#000000");
		}
		for(int i = start + currentDate.dayOfMonth().getMaximumValue(), d = 1; i < days.size(); i++, d++)
		{
			days.get(i).setText(String.valueOf(d));
			days.get(i).setTextColor("#BFBFBF");
		}*/
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.iv_calendar_prev_month:
				currentDate = currentDate.minusMonths(1);
				setCalendar();
				break;
			case R.id.iv_calendar_next_month:
				currentDate = currentDate.plusMonths(1);
				setCalendar();
				break;
		}
	}

	public void setOnDateClickListener(OnDateClickListener listener)
	{
		this.listener = listener;
	}

	public interface OnDateClickListener
	{
		public void onClick(DateTime t);
	}
}
