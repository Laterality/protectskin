package com.likefirst.protectskin.views;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-07-16.
 */
public class CalendarViewIncl extends LinearLayout implements View.OnClickListener
{

	private static final String TAG = "CalendarView";


	private OnDateClickListener listener;
	private DateTime currentDate;
	private TextView tvIndicator;
	private ImageView ivPrev;
	private ImageView ivNext;
	private List<CalendarCell> cells;
	
	private int start_idx = 0;

	public CalendarViewIncl(Context context)
	{
		super(context);
		init(context);
	}

	public CalendarViewIncl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public CalendarViewIncl(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init(context);
	}

	@TargetApi(21)
	public CalendarViewIncl(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init(context);
	}
	
	
	private void init(final Context context)
	{
		final ProgressDialog pd = new ProgressDialog(context);
		pd.setMessage("불러오는 중");
		pd.setCancelable(false);
		pd.show();

		currentDate = DateTime.now();
		cells = new ArrayList<>();
		
		LayoutInflater.from(context).inflate(R.layout.view_calendar_incl, this, true);


		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				ivPrev = (ImageView) findViewById(R.id.iv_calendar_prev_month);
				ivNext = (ImageView) findViewById(R.id.iv_calendar_next_month);
				tvIndicator = (TextView) findViewById(R.id.tv_calendar_indicator);

				List<TextView> tvl = new ArrayList();
				Typeface tf = (Typeface.createFromAsset(context.getAssets(),"NanumSquareB.ttf"));
				tvl.add(tvIndicator);
				tvl.add((TextView) findViewById(R.id.tv_calendar_sunday));
				tvl.add((TextView) findViewById(R.id.tv_calendar_monday));
				tvl.add((TextView) findViewById(R.id.tv_calendar_tuesday));
				tvl.add((TextView) findViewById(R.id.tv_calendar_wednesday));
				tvl.add((TextView) findViewById(R.id.tv_calendar_thursday));
				tvl.add((TextView) findViewById(R.id.tv_calendar_friday));
				tvl.add((TextView) findViewById(R.id.tv_calendar_saturday));


				for(int i = 0 ; i < tvl.size();i++)
				{
					tvl.get(i).setTypeface(tf);
				}

				Resources res = getResources();
				TextView t;
				for(int i = 0 ; i < 42; i++)
				{
					View v = findViewById(res.getIdentifier("cv_calendar_cell_" + i, "id", context.getPackageName()));
					t = (TextView) v.findViewById(R.id.tv_calendar_cell_day);
					t.setTypeface(tf);
					cells.add(
							new CalendarCell(v,
									t,
									(ImageView) v.findViewById(R.id.iv_calendar_cell_img)));
				}

				ivPrev.setOnClickListener(CalendarViewIncl.this);
				ivNext.setOnClickListener(CalendarViewIncl.this);



				setCalendar();

				pd.dismiss();

			}
		}).start();

	}

	private void setCalendar()
	{
		int month = currentDate.getMonthOfYear();
		int year = currentDate.getYear();
		tvIndicator.setText(String.format("%d년 %d월", year, month));

		int start = 0;
		switch (new DateTime(currentDate.getYear(), currentDate.getMonthOfYear(), 1, 0 , 0 ).getDayOfWeek())
		{
			case DateTimeConstants.SUNDAY:
				start = 0;
				break;
			case DateTimeConstants.MONDAY:
				start = 1;
				break;
			case DateTimeConstants.TUESDAY:
				start = 2;
				break;
			case DateTimeConstants.WEDNESDAY:
				start = 3;
				break;
			case DateTimeConstants.THURSDAY:
				start = 4;
				break;
			case DateTimeConstants.FRIDAY:
				start = 5;
				break;
			case DateTimeConstants.SATURDAY:
				start = 6;
				break;
			default:
				break;
		}
		start_idx = start;
		DateTime t;
		DateTime dt = new DateTime(currentDate.getYear(), currentDate.getMonthOfYear(), 1, 0, 0);

		for(int i = 0 ; i < cells.size(); i++)
		{
			cells.get(i).setState(CalendarCell.STATE_NONE);
			if(i<start) {cells.get(i).setDate(dt.minusDays(Math.abs(i - start)), false);} // prev. month in the calendar
			else{cells.get(i).setDate(t = dt.plusDays(i - start), currentDate.getMonthOfYear() == t.getMonthOfYear());} // next month in the calendar
		}
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.iv_calendar_prev_month:
				currentDate = currentDate.minusMonths(1);
				setCalendar();
				break;
			case R.id.iv_calendar_next_month:
				currentDate = currentDate.plusMonths(1);
				setCalendar();
				break;
		}
	}

	public void setCellState(int index, int state)
	{
		cells.get(index).setState(state);
	}

	public int getIndexOfFirstDay(){return start_idx;}
	
	
	public class CalendarCell
	{
		public static final int STATE_NONE = 0;
		public static final int STATE_FULFILLED = 1;
		public static final int STATE_UNFULFILLED = 2;

		private TextView date;
		private ImageView img;
		private DateTime dt;
		
		public CalendarCell(@NonNull View body, @NonNull TextView tv, @NonNull ImageView iv)
		{
			date = tv;
			img = iv;
			if(dt != null){date.setText(dt.getDayOfMonth());}
			body.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					Log.d(TAG, "clicked date : " + (dt == null ? "date null" : dt.toString("yy-MM-dd hh:mm:ss")));
					if(listener != null)
					{
						listener.onClick(dt);
					}
				}
			});
		}

		
		private void setDate(DateTime date, boolean thisMonth)
		{
			this.dt = date;
			this.date.setText(String.valueOf(date.getDayOfMonth()));
			if(thisMonth){this.date.setTextColor(Color.parseColor("#000000"));}
			else{this.date.setTextColor(Color.parseColor("#BFBFBF"));}
		}
		
		public void setState(int state)
		{
			if(state == STATE_FULFILLED) {img.setImageDrawable(img.getResources().getDrawable(R.drawable.check_o_icon));}
			else if(state == STATE_UNFULFILLED) {img.setImageDrawable(img.getResources().getDrawable(R.drawable.check_x_icon));}
			else if(state == STATE_NONE){img.setImageDrawable(null);}
		}
		
		public DateTime getDate()
		{
			return dt;
		}
		
	}

	public void setOnDateClickListener(OnDateClickListener listener)
	{
		this.listener = listener;
	}

	public interface OnDateClickListener
	{
		void onClick(DateTime t);
	}
}
