package com.likefirst.protectskin.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class HeaderView extends LinearLayout
{


	public HeaderView(Context context)
	{
		super(context);
		init(context);
	}

	public HeaderView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public HeaderView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init(context);
	}

	public HeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init(context);
	}

	private void init(Context context)
	{
		LayoutInflater.from(context).inflate(R.layout.header_bar, this, true);

		Typeface tf = (Typeface.createFromAsset(context.getAssets(),"NanumSquareB.ttf"));
		TextView tv = ((TextView) findViewById(R.id.tv_headerBar_header));
		tv.setTypeface(tf);

	}
}
