package com.likefirst.protectskin.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;

import org.joda.time.DateTime;

/**
 * Created by jinwoo on 2016-07-13.
 */
public class CalendarCellView extends LinearLayout
{
	private static final String TAG = "CalendarCellView";
	private DateTime dt;
	private TextView tvDay;
	private ImageView ivImg;

	public CalendarCellView(Context context)
	{
		super(context);
		init(context);
	}

	public CalendarCellView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public CalendarCellView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context)
	{
		LayoutInflater.from(context).inflate(R.layout.view_calendar_cell, this, true);
		tvDay = (TextView) findViewById(R.id.tv_calendar_cell_day);
		ivImg = (ImageView) findViewById(R.id.iv_calendar_cell_img);

		Typeface tf = (Typeface.createFromAsset(context.getAssets(),"NanumSquareB.ttf"));
		tvDay.setTypeface(tf);
	}

	public void setText(CharSequence text)
	{
		tvDay.setText(text);
	}
	public void setTextColor(String hex){tvDay.setTextColor(Color.parseColor(hex));}
	public void setDrawable(Drawable drawable)
	{
		ivImg.setImageDrawable(drawable);
	}
	public void setDate(DateTime d){dt = d; tvDay.setText(String.valueOf(dt.getDayOfMonth()));}

	public DateTime getDate(){return dt;}
}
