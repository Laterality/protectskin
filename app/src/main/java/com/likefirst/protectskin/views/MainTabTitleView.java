package com.likefirst.protectskin.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.likefirst.protectskin.R;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class MainTabTitleView extends LinearLayout
{
	private TextView tvTitle;

	public MainTabTitleView(Context context)
	{
		super(context);
		init(context);
	}

	public MainTabTitleView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public MainTabTitleView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init(context);
	}

	public MainTabTitleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init(context);
	}



	private void init(Context context)
	{
		View rootView = LayoutInflater.from(context).inflate(R.layout.view_main_tab, this, true);
		tvTitle = (TextView)rootView.findViewById(R.id.tv_main_tab_title);

		TextView tv = (TextView) findViewById(R.id.tv_main_tab_title);
		Typeface tf = (Typeface.createFromAsset(context.getAssets(),"NanumSquareB.ttf"));
		tv.setTypeface(tf);
	}

	public View setText(CharSequence text)
	{
		if(tvTitle != null){tvTitle.setText(text);}
		return this;
	}
}
