package com.likefirst.protectskin.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.likefirst.protectskin.R;

/**
 * Created by jinwoo on 2016-07-17.
 */
public class MyInfoTabTitleView extends LinearLayout
{
	private TextView tvTitle;

	public MyInfoTabTitleView(Context context)
	{
		super(context);
		init(context);
	}

	public MyInfoTabTitleView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public MyInfoTabTitleView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init(context);
	}

	@TargetApi(21)
	public MyInfoTabTitleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init(context);
	}

	private void init(Context context)
	{
		LayoutInflater.from(context).inflate(R.layout.view_myinfo_tab, this, true);

		tvTitle = (TextView) findViewById(R.id.tv_myinfo_tab_title);
		Typeface tf = (Typeface.createFromAsset(context.getAssets(), "NanumSquareB.ttf"));
		tvTitle.setTypeface(tf);
	}

	public void setText(CharSequence text)
	{
		tvTitle.setText(text);
	}
}
