package com.likefirst.protectskin.api_dto;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by jinwoo on 2016-08-14.
 */
public class PostElement
{
	public static final String TYPE_TEXT = "text";
	public static final String TYPE_IMAGE = "image";
	public static final String TYPE_IMAGE_LOCAL = "image_local";
	private int _index;
	private String _type;
	private String content;
	private Bitmap img_local; // for local img
	private Uri img_uri; // for local img

	public PostElement(int index, String type, String content)
	{
		_index = index;
		_type = type;
		this.content = content;
	}

	public PostElement(int index, String type, Uri uri, Bitmap img)
	{
		_index = index;
		_type = type;
		img_uri = uri;
		img_local = img;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public int getIndex()
	{
		return _index;
	}

	public String getType()
	{
		return _type;
	}

	public String getContent()
	{
		return content;
	}

	public Bitmap getLocalImage()
	{
		return img_local;
	}

	public Uri getUri(){return img_uri;}

	public PostElement sanitize()
	{
		PostElement e = new PostElement(_index, _type, content);
		e.img_uri = null;
		e.img_local = null;
		if(e._type.equals(TYPE_IMAGE_LOCAL)){e._type = TYPE_IMAGE;}
		return e;
	}

	public static void swapIndex(PostElement e1, PostElement e2)
	{
		int t = e1._index;
		e1._index = e2._index;
		e2._index = t;
	}

}
