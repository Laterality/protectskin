package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.Result;
import com.likefirst.protectskin.api_dto.UserDto;

/**
 * Created by jinwoo on 2016-08-06.
 */
public class UserResult
{
	private String result;
	private UserDto user;

	public String getResult()
	{
		return result;
	}

	public UserDto getUser()
	{
		return user;
	}
}
