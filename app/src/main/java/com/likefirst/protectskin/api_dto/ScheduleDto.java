package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-08-18.
 */
public class ScheduleDto
{
	public static final String PERIOD_MORNING = "MORNING";
	public static final String PERIOD_NOON = "NOON";
	public static final String PERIOD_EVENING = "EVENING";

	private String _id;
	private String period;
	private String user_id;
	private String text;
	private long date;
	private boolean fulfilled;

	public String getId(){return _id;}
	public String getUserId(){return user_id;}
	public String getText(){return text;}
	public String getPeriod(){return period;}
	public long getDate(){return date;}
	public boolean isFulfilled(){return fulfilled;}
}
