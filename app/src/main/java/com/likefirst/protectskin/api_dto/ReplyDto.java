package com.likefirst.protectskin.api_dto;

import java.util.List;

/**
 * Created by jinwoo on 2016-08-11.
 *
 * 게시물에 대한 덧글
 */
public class ReplyDto
{
	private String post_id;
	private UserDto user_id;
	private String text;
	private boolean rereply;
	private List<ReplyDto> _ref;
	private long regDate;

	private String user; // 작성자 id for POST
	private String content;
	private String to;

	public String getPostId(){return post_id;}
	public UserDto getAuthor(){return user_id;}
	public String getContent(){return text;}
	public boolean isRereply(){return rereply;}
	public List<ReplyDto> getRefs(){return _ref;}
	public long getRegDate(){return regDate;}

	public ReplyDto(String user_id, String content, boolean rereply, String to)
	{
		user = user_id;
		this.content = content;
		this.rereply = rereply;
		this.to = to;
	}
}

