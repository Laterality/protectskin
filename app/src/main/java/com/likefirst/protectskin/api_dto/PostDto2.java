package com.likefirst.protectskin.api_dto;


import java.util.List;

/**
 * Created by jinwoo on 2016-08-15.
 *
 * 게시물 POST를 위한 Data Object
 */
public class PostDto2
{
	private String _id;
	private String author;
	private String board_id;
	private String skinType_id;
	private String title;
	private long regDate;
	private List<PostElement> content;

	public PostDto2(String author, String board, String skinType, String title, List<PostElement> content)
	{
		this.author = author;
		this.board_id = board;
		skinType_id = skinType;
		this.title = title;
		this.content = content;
	}

	public List<PostElement> getContent(){return content;}

	public String getId(){return _id;}
	public String getAuthor(){return author;}
	public String getBoardId(){return board_id;}
	public String getSkinTypeId(){return skinType_id;}
	public String getTitle() {return title;}
}
