package com.likefirst.protectskin.api_dto;

import com.likefirst.protectskin.objects.SkinType;

/**
 * Created by jinwoo on 2016-07-23.
 */
public class UserDto
{
	private String _id;
	private String __v;
	private String login_type;
	private String email;
	private String username;
	private String external_id;
	private String gender;
	private String profile_img;
	private SkinTypeDto skin_type;

	public String getId(){return _id;}
	public String getVersion() {return __v;}
	public String getLoginType(){return login_type;}
	public String getEmail(){return email;}
	public String getUsername(){return username;}
	public String getExternalId(){return external_id;}
	public String getGender(){return gender;}
	public String getProfileImg(){return profile_img;}
	public SkinTypeDto getSkinType(){return skin_type;}

	public UserDto(String id)
	{
		_id = id;
	}
	public UserDto(String email, String username, SkinTypeDto skin_type)
	{
		this.email = email;
		this.username = username;
		this.skin_type = skin_type;
	}

	private UserDto(String _id, String email, String username, String login_type, String external_id, String gender, String profile_img)
	{
		this._id = _id;
		this.email = email;
		this.username = username;
		this.login_type = login_type;
		this.external_id = external_id;
		this.gender = gender;
		this.profile_img = profile_img;
	}

	public static UserDto cloneFrom2(UserDto2 u)
	{
		return new UserDto(u.getId(), u.getEmail(), u.getUsername(), u.getLoginType(), u.getExternalId(), u.getGender(), u.getProfileImg());
	}


}
