package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.FootStep;
import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.Result;

import java.util.List;

/**
 * Created by jinwoo on 2016-08-06.
 */
public class PostsResult
{
	private String result;
	private List<PostDto> posts;

	public String getResult(){return result;}
	public List<PostDto> getPosts() {return posts;}
}
