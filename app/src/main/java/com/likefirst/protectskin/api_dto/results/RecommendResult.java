package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.FootStep;

/**
 * Created by jinwoo on 2016-08-10.
 */
public class RecommendResult
{
	private String result;
	private FootStep footstep;

	public String getResult()
	{
		return result;
	}

	public FootStep getFootStep(){return footstep;}
}
