package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-08-07.
 */
public class SkinTypeDto
{
	private String name;
	private String _id;

	public String getName(){return name;}
	public String getId(){return _id;}

	public SkinTypeDto(String id)
	{
		this._id = id;
	}
}
