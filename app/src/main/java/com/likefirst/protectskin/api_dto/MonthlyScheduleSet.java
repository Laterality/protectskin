package com.likefirst.protectskin.api_dto;

import java.util.List;

/**
 * Created by jinwoo on 2016-08-18.
 */
public class MonthlyScheduleSet
{

	private String _id;
	private String user_id;
	private int day_fulfilled;
	private int day_entire;
	private Date date;
	private List<DailyScheduleSet> schedules;

	public String getId(){return _id;}
	public String user_id(){return user_id;}
	public int getDayFulfilled(){return day_fulfilled;}
	public int getDayEntire(){return day_entire;}
	public Date getDate() {return date;}
	public List<DailyScheduleSet> getSchedules(){return schedules;}

	public class Date
	{
		private int year;
		private int month;

		public int getYear(){return year;}
		public int getMonth(){return month;}
	}
}
