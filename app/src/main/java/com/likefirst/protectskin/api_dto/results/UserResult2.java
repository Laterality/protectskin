package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.UserDto;
import com.likefirst.protectskin.api_dto.UserDto2;

/**
 * Created by jinwoo on 2016-08-08.
 */
public class UserResult2
{
	private String result;
	private UserDto2 user;

	public String getResult()
	{
		return result;
	}

	public UserDto2 getUser()
	{
		return user;
	}
}
