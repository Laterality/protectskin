package com.likefirst.protectskin.api_dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jinwoo on 2016-08-06.
 */
public class PostDto
{
	private String _id;
	private String title;
	private List<PostElement> content;
	private UserDto author;
	private long regDate;
	private int count_recommend;
	private int count_view;
	private int count_replies;

	public String getId() {return _id;}
	public String getTitle() {return title;}
	public List<PostElement> getContent() {return content;}
	public UserDto getAuthor() {return author;}
	public long getRegDate() {return regDate;}
	public int getCountRecommend() {return count_recommend;}
	public int getCountView() {return count_view;}
	public int getCountReplies() {return count_replies;}
	public List<String> getTextContent()
	{
		List<String> texts = new ArrayList<>();
		for(PostElement e : content)
		{
			if(e.getType().equals(PostElement.TYPE_TEXT))
			{
				texts.add(e.getContent());
			}
		}
		return texts;
	}

}
