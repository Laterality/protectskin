package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-08-08.
 */
public class UserDto2
{

	private String _id;
	private String __v;
	private String login_type;
	private String email;
	private String username;
	private String external_id;
	private String gender;
	private String profile_img;
	private String skin_type;

	public String getId(){return _id;}
	public String getVersion() {return __v;}
	public String getLoginType(){return login_type;}
	public String getEmail(){return email;}
	public String getUsername(){return username;}
	public String getExternalId(){return external_id;}
	public String getGender(){return gender;}
	public String getProfileImg(){return profile_img;}
	public String getSkinType(){return skin_type;}

	public UserDto2(String id)
	{
		_id = id;
	}
	public UserDto2(String email, String username, String skin_type)
	{
		this.email = email;
		this.username = username;
		this.skin_type = skin_type;
	}


}
