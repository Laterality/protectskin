package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.FootStep;
import com.likefirst.protectskin.api_dto.PostDto;

/**
 * Created by jinwoo on 2016-08-10.
 */
public class PostResult
{
	private String result;
	private PostDto post;
	private FootStep footstep;

	public String getResult(){return result;}
	public PostDto getPost(){return post;}
	public FootStep getFootStep(){return footstep;}
}
