package com.likefirst.protectskin.api_dto;

import java.util.List;

/**
 * Created by jinwoo on 2016-08-18.
 */
public class DailyScheduleSet
{
	private String _id;
	private String user_id;
	private long date;
	private List<ScheduleDto> schedules;

	public String getId(){return _id;}
	public String getUserId(){return user_id;}
	public long getDate(){return date;}
	public List<ScheduleDto> getSchedules(){return schedules;}
	public boolean isFulfilled()
	{
		int cnt = 0;
		for(ScheduleDto s : schedules)
		{
			if(s.isFulfilled()){cnt++;}
		}
		return cnt == schedules.size();
	}
}
