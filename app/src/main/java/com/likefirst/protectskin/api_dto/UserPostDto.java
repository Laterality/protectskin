package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-07-23.
 */
public class UserPostDto
{
	private String login_type;
	private String email;
	private String password;
	private String username;
	private String external_id;
	private String gender;

	public UserPostDto(String login_type, String email, String password, String username, String external_id, String gender)
	{
		this.login_type = login_type;
		this.email = email;
		this.password = password;
		this.username = username;
		this.external_id = external_id;
		this.gender = gender;
	}
}
