package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-08-15.
 */
public class UploadDto
{
	private String id;
	private int no;

	public UploadDto(String id, int no)
	{
		this.id = id;
		this.no = no;
	}
}
