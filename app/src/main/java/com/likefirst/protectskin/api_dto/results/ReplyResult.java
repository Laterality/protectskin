package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.ReplyDto;

import java.util.List;

/**
 * Created by jinwoo on 2016-08-11.
 *
 * 덧글 관련 API 결과
 */
public class ReplyResult
{
	private String result;
	private ReplyDto reply;
	private List<ReplyDto> replies;
	private int count_replies;

	public String getResult(){return result;}
	public ReplyDto getReply(){return reply;}
	public List<ReplyDto> getReplies(){return replies;}
	public int getCountReplies(){return count_replies;}
}
