package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.MonthlyScheduleSet;

/**
 * Created by jinwoo on 2016-08-18.
 */
public class MonthlyScheduleSetResult
{
	private String result;
	private MonthlyScheduleSet monthlyscheduleset;

	public String getResult(){return result;}
	public MonthlyScheduleSet getMonthlyScheduleSet(){return monthlyscheduleset;}
}
