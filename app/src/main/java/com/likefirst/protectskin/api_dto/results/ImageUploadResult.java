package com.likefirst.protectskin.api_dto.results;

/**
 * Created by jinwoo on 2016-08-15.
 */
public class ImageUploadResult
{
	private String result;
	private String path;

	public String getResult(){return result;}
	public String getPath(){return path;}
}
