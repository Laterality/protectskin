package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-07-23.
 */
public class UserLoginDto
{
	public static final String LOGIN_TYPE_NATIVE = "NATIVE";
	public static final String LOGIN_TYPE_GOOGLE = "GOOGLE";

	private String login_type;
	private String email;
	private String password;
	private String external_id;

	public UserLoginDto(String login_type, String email, String password, String external_id)
	{
		this.login_type = login_type;
		this.email = email;
		this.password = password;
		this.external_id = external_id;
	}

	public String getLoginType(){return login_type;}
	public String getEmail(){return email;}
	public String getExternal_id(){return external_id;}

}
