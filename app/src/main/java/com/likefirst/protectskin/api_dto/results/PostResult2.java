package com.likefirst.protectskin.api_dto.results;

import com.likefirst.protectskin.api_dto.PostDto;
import com.likefirst.protectskin.api_dto.PostDto2;
import com.likefirst.protectskin.api_dto.UserDto2;

/**
 * Created by jinwoo on 2016-08-15.
 */
public class PostResult2
{
	private String result;
	private PostDto2 post;

	public String getResult(){return result;}
	public PostDto2 getPost(){return post;}
}
