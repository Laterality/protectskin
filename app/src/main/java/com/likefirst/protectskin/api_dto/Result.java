package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-07-23.
 */
public class Result
{
	public static final String RESULT_ERROR = "error";
	public static final String RESULT_FAIL = "fail";
	public static final String RESULT_SUCCESS = "success";
	public static final String RESULT_REGISTER = "register";

	private String result;

	public String getResult(){return result;}
}
