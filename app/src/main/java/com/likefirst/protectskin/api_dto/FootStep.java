package com.likefirst.protectskin.api_dto;

/**
 * Created by jinwoo on 2016-08-10.
 */
public class FootStep
{
	private String post_id;
	private String user_id;
	private boolean read;
	private boolean recommend;
	private boolean bookmark;
	private long readDate;
	private long recommendDate;
	private long bookmarkDate;

	public String getPostId(){return post_id;}
	public String getUserId(){return user_id;}
	public boolean isRead(){return read;}
	public boolean isRecommended(){return recommend;}
	public boolean isBookmarked(){return bookmark;}

}
