package com.likefirst.protectskin.skplanet_api_dto;

import java.util.List;

/**
 * Created by blues on 2016-07-09.
 */
public class WeatherPlanetUVApiDto
{
	private Result result;
	private Common common;
	private Weather weather;
	private String timeRelease;

	public Result getResult(){return result;}
	public Common getCommon(){return common;}
	public Weather getWeather(){return weather;}
	public String getTimeRelease(){return timeRelease;}

	public class Weather
	{
		private WIndex wIndex;

		public WIndex getwIndex(){return wIndex;}

		public class WIndex
		{
			private List<UVIndex> uvindex;

			public List<UVIndex> getUvindex(){return uvindex;}
			public class UVIndex
			{
				private Grid grid;

				private Day day00;
				private Day day01;
				private Day day02;

				public Grid getGrid(){return grid;}
				public Day getDay(int index){switch(index){case 0: return day00;case 1: return day01;case 2: return day02;} return null;}

				public class Day
				{
					public String index;
					public String comment;
					public String imageUrl;

					public String getIndex(){return index;}
					public String getComment(){return comment;}
					public String getImageUrl(){return imageUrl;}
				}
			}


		}

	}

}
