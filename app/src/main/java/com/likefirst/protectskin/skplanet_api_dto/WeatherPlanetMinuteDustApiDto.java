package com.likefirst.protectskin.skplanet_api_dto;

import java.util.List;

/**
 * Created by blues on 2016-07-09.
 */
public class WeatherPlanetMinuteDustApiDto
{
	private Result result;
	private Common common;
	private Weather weather;

	public Result getResult()
	{
		return result;
	}

	public Common getCommon()
	{
		return common;
	}

	public Weather getWeather()
	{
		return weather;
	}


	public class Weather
	{
		private List<Dust> dust;

		public List<Dust> getDusts()
		{
			return dust;
		}

		public class Dust
		{
			private Station station;
			private String timeObservation;
			private ObservedValue pm10;

			public Station getStation()
			{
				return station;
			}

			public String getTimeObservation()
			{
				return timeObservation;
			}

			public ObservedValue getPm10()
			{
				return pm10;
			}
		}

		public class Station
		{
			private String name;
			private String id;
			private String latitude;
			private String longitude;

			public String getName()
			{
				return name;
			}

			public String getId()
			{
				return id;
			}

			public String getLatitude()
			{
				return latitude;
			}

			public String getLongitude()
			{
				return longitude;
			}
		}

		public class ObservedValue
		{
			String value;
			String grade;

			public String getValue()
			{
				return value;
			}

			public String getGrade()
			{
				return grade;
			}
		}
	}
}
