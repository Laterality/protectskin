package com.likefirst.protectskin.skplanet_api_dto;

/**
 * Created by blues on 2016-07-09.
 */
public class Common
{
	private String alertYn; // 한반도 전역 기준 특보 존재여부 Y , N
	private String stormYn; // 경계구역 기준 태풍 존재유무

	public String getAlertYn()
	{
		return alertYn;
	}

	public String getStormYn()
	{
		return stormYn;
	}
}
