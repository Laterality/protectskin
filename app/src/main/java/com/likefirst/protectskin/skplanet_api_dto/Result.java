package com.likefirst.protectskin.skplanet_api_dto;

/**
 * Created by blues on 2016-07-09.
 */
public class Result
{
	private String message;
	private String code;
	private String requestUrl;

	public String getMessage()
	{
		return message;
	}

	public String getCode()
	{
		return code;
	}

	public String getRequestUrl()
	{
		return requestUrl;
	}
}
