package com.likefirst.protectskin.skplanet_api_dto;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class Grid
{
	private String city;
	private String county;
	private String village;

	public String getCity()
	{
		return city;
	}

	public String getCounty()
	{
		return county;
	}

	public String getVillage()
	{
		return village;
	}
}
