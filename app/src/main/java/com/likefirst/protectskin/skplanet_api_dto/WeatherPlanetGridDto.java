package com.likefirst.protectskin.skplanet_api_dto;

/**
 * Created by jinwoo on 2016-07-15.
 */
public class WeatherPlanetGridDto
{
	private Result result;
	private Common common;
	private Weather weather;

	public Result getResult(){return result;}
	public Common getCommon(){return common;}
	public Weather getWeather(){return weather;}


	public class Weather
	{
		private Grid grid;

		public Grid getGrid(){return grid;}
	}
}
